﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monopolist.Core.Types.Players.Containers.Pairs;
using Monopolist.Player;

namespace Monopolist.Core.Types.Players.Containers
{
    public class AllPlayersContainer
    {
        public List<PlayerAccountPlayerFormPair> AccountAndFormPairs { get; private set; }
        public List<PlayerAccount> AllPlayers { get; }
        public Dictionary<int, PlayerForm> AllPlayersForms { get; }
        public PlayerAccount BankAccount { get; }

        public AllPlayersContainer(List<PlayerAccount> allPlayers, Dictionary<int, PlayerForm> allPlayersForms, PlayerAccount bankAccount)
        {
            AllPlayers = allPlayers;
            AllPlayersForms = allPlayersForms;
            PrepareAccountFormPairs(allPlayers, allPlayersForms);

            BankAccount = bankAccount;
        }
        
        private void PrepareAccountFormPairs(List<PlayerAccount> allPlayers, Dictionary<int, PlayerForm> allPlayersForms)
        {
            AccountAndFormPairs = new List<PlayerAccountPlayerFormPair>();
            
            for (int i=0; i<allPlayers.Count; i++)
            {
                AccountAndFormPairs.Add(new PlayerAccountPlayerFormPair(allPlayersForms.ElementAt(i).Value, allPlayers.ElementAt(i)));
            }
        }

    }
}

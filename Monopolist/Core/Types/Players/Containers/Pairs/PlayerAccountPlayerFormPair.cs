﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monopolist.Player;
using Monopolist.Core.Types.Players;

namespace Monopolist.Core.Types.Players.Containers.Pairs
{
    public class PlayerAccountPlayerFormPair
    {
        public PlayerForm PlayerForm { get; }
        public PlayerAccount PlayerAccount { get; }

        public PlayerAccountPlayerFormPair(PlayerForm playerForm, PlayerAccount playerAccount)
        {
            PlayerForm = playerForm;
            PlayerAccount = playerAccount;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopolist.Core.Types.Players
{
    public class PlayerCash
    {
        public int Cash { get; private set; }

        public PlayerCash(int cashOnStart)
        {
            Cash = cashOnStart;
        }

        public void AddCash(int amount)
        {
            Cash += amount;
        }

        public void RemoveCash(int amount)
        {
            Cash -= amount;
            
            /*
            // prevent Cash from being negative
            if (Cash < 0)
                Cash = 0;
            */
        }

    }
}

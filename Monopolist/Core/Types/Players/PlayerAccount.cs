﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monopolist.Board.Types.Fields;

namespace Monopolist.Core.Types.Players
{
    public class PlayerAccount
    {
        public enum PlayerType
        {
            Human,
            Computer,
            Bank
        }

        public string Name { get; }
        public char Symbol { get; }
        public bool IsBankrupt { get; private set; }
        public PlayerType Type { get; }

        public PlayerCash CashAccount { get; }

        public Field FieldOn { get; private set; }
        
        public PlayerPrisonStatus PrisonStatus { get; }
        
        public int LastDiceResult { get; private set; }

        public PlayerAccount(PlayerType playerType, string name, char symbol, int cashOnStart)
        {
            Name = name;
            Symbol = symbol;
            IsBankrupt = false;
            Type = playerType;

            CashAccount = new PlayerCash(cashOnStart);

            PrisonStatus = new PlayerPrisonStatus();

            LastDiceResult = 0;

            // use this only when you get startingField in constructor
            //startingField.AddVisitor(this);
        }

        /// <summary>
        /// Use this constructor to create BANK account
        /// </summary>
        public PlayerAccount()
        {
            Name = "BANK";
            Symbol = '$';
            Type = PlayerType.Bank;
            CashAccount = new PlayerCash(0);
        }
        
        public void ChangeFieldOn(Field field)
        {
            FieldOn = field;
        }
        
        public void ChangeLastDiceResult(int newResult)
        {
            LastDiceResult = newResult;
        }

        public void ChangeBankruptStatus(bool isBankrupt)
        {
            IsBankrupt = isBankrupt;
        }

        public override string ToString()
        {
            return String.Format("{0} {1}", Symbol, Name);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopolist.Core.Types.Players
{
    public class PlayerPrisonStatus
    {
        public bool IsInPrison { get; private set; }
        public int TurnsLeftInPrison { get; private set; }

        public PlayerPrisonStatus()
        {
            IsInPrison = false;
            TurnsLeftInPrison = 0;
        }

        public void FreeFromPrison()
        {
            IsInPrison = false;
            TurnsLeftInPrison = 0;
        }

        public void LockInPrison()
        {
            IsInPrison = true;
            TurnsLeftInPrison = 3;
        }

        public void RemoveOneDay()
        {
            TurnsLeftInPrison--;

            if (TurnsLeftInPrison == 0)
                IsInPrison = false;
        }

    }
}

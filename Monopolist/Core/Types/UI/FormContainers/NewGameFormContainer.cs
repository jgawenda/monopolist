﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.NewGame;
using Monopolist.NewGame.Result;

namespace Monopolist.Core.Types.UI.FormContainers
{
    public class NewGameFormContainer : FormContainer
    {
        private NewGameForm _newGameForm;

        public NewGameFormContainer(Form form) : base (form)
        {
            PrepareForm();
        }

        private void PrepareForm()
        {
            _newGameForm = (NewGameForm)_form;
        }
        
        public NewGameFormResult GetFormResult()
        {
            return _newGameForm.NewGameFormResult;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.NewGame;
using Monopolist.Core.Interfaces;

namespace Monopolist.Core.Types.UI.FormContainers
{
    public abstract class FormContainer
    {
        protected Form _form;

        public FormContainer(Form form)
        {
            _form = form;
        }

        public DialogResult SwitchToDialog(IWin32Window owner)
        {
            _form.ShowDialog(owner);
            _form.Dispose();

            return _form.DialogResult;
        }
        
    }
}

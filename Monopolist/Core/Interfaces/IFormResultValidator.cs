﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopolist.Core.Interfaces
{
    public interface IFormResultValidator
    {
        bool Validate();

    }
}

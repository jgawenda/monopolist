﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Actions.NewGame.PreparingBoardElements;
using Monopolist.Core.Actions.NewGame.PreparingUI;
using Monopolist.Core.Managers.NewGame;
using Monopolist.Core.Types.Players;
using Monopolist.Board.Types.Fields.Containers;
using Monopolist.NewGame.Result;

namespace Monopolist.Core.Actions.NewGame
{
    public class GamePreparingManager
    {
        public GameFormsManager PrepareAllForms(NewGameFormResult newGameFormResult)
        {
            return UIPreparingManager.PrepareAllForms(newGameFormResult);
        }
        
        public AllFieldsContainer PrepareBoardElements(List<PlayerAccount> allPlayers, PlayerAccount bankAccount, List<Button> allFieldsButtons)
        {
            AllFieldsContainer allFieldsContainer = BoardElementsPreparingManager.PrepareAllFields(bankAccount, allFieldsButtons);
            BoardElementsPreparingManager.PlacePlayersOnStart(allPlayers, allFieldsContainer);

            return allFieldsContainer;
        }
        
    }
}

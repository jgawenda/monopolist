﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using Monopolist.Board.Types.Fields;
using Monopolist.Core.Types.Players;
using Monopolist.Core.Actions.NewGame.PreparingBoardElements.CardsGenerating;
using Monopolist.Board.Types.Fields.Street;
using Monopolist.Board.Types.Fields.Chance;
using Monopolist.Board.Types.Fields.Actions.Chance.Types;

namespace Monopolist.Core.Actions.NewGame.PreparingBoardElements.FieldsGenerating
{
    public class FieldsGenerator
    {
        private PlayerAccount _bankAccount;
        private ChanceCardsGenerator _chanceCardsGenerator;
        private ChanceCardsGenerator _chestCardsGenerator;

        private List<Button> _allButtons;

        public FieldsGenerator(PlayerAccount bankAccount, List<Button> allButtons)
        {
            _bankAccount = bankAccount;
            _allButtons = allButtons;
        }
        
        public List<Field> GenerateFields()
        {
            //ChanceCardsPile pileOfChanceCards = _chanceCardsGenerator.GeneratePileOfCards();
            //ChanceCardsPile pileOfChestCards = _chestCardsGenerator.GeneratePileOfCards();
            ChanceCardsPile emptyPileOfCards = new ChanceCardsPile(new List<ChanceCard>());

            FieldPrison prison = new FieldPrison(_allButtons.ElementAt(10));

            // this list is made to be used inside every FieldStation, and no other places
            List<FieldStation> allStationsInternalList = new List<FieldStation>();
            // same for Utilities
            List<FieldUtility> allUtilitiesInternalList = new List<FieldUtility>();
            // ... and for Streets
            List<FieldStreet> allStreetsInternalList = new List<FieldStreet>();
            
            List<Field> allFields = new List<Field>()
            {
                new FieldStart(_allButtons.ElementAt(0)),
                new FieldStreet(_bankAccount, new StreetCardInfo(Color.FromArgb(128, 64, 0), "Konopacka", 60, 2, 10, 30, 90, 160, 250, 50), allStreetsInternalList, _allButtons.ElementAt(1)),
                new FieldChance(new CardInfo("Community Chest 1"), emptyPileOfCards, _allButtons.ElementAt(2)),
                new FieldStreet(_bankAccount, new StreetCardInfo(Color.FromArgb(128, 64, 0), "Stalowa", 60, 4, 20, 60, 180, 320, 450, 50), allStreetsInternalList, _allButtons.ElementAt(3)),
                new FieldTax("Tax 1", 200, _allButtons.ElementAt(4)),
                new FieldStation(_bankAccount, new BuyableCardInfo("Dworzec Zachodni", 200), allStationsInternalList, _allButtons.ElementAt(5)),
                new FieldStreet(_bankAccount, new StreetCardInfo(Color.DodgerBlue, "Radzymińska", 100, 6, 30, 90, 270, 400, 550, 50), allStreetsInternalList, _allButtons.ElementAt(6)),
                new FieldChance(new CardInfo("Chance 1"), emptyPileOfCards, _allButtons.ElementAt(7)),
                new FieldStreet(_bankAccount, new StreetCardInfo(Color.DodgerBlue, "Jagiellońska", 100, 6, 30, 90, 270, 400, 550, 50), allStreetsInternalList, _allButtons.ElementAt(8)),
                new FieldStreet(_bankAccount, new StreetCardInfo(Color.DodgerBlue, "Targowa", 120, 8, 40, 100, 300, 450, 600, 50), allStreetsInternalList, _allButtons.ElementAt(9)),
                prison,
                new FieldStreet(_bankAccount, new StreetCardInfo(Color.Violet, "Płowiecka", 140, 10, 50, 150, 450, 625, 750, 100), allStreetsInternalList, _allButtons.ElementAt(11)),
                new FieldUtility(_bankAccount, new BuyableCardInfo("Elektrownia", 150), allUtilitiesInternalList, _allButtons.ElementAt(12)),
                new FieldStreet(_bankAccount, new StreetCardInfo(Color.Violet, "Marsa", 140, 10, 50, 150, 450, 625, 750, 100), allStreetsInternalList, _allButtons.ElementAt(13)),
                new FieldStreet(_bankAccount, new StreetCardInfo(Color.Violet, "Grochowska", 160, 12, 60, 180, 500, 700, 900, 100), allStreetsInternalList, _allButtons.ElementAt(14)),
                new FieldStation(_bankAccount, new BuyableCardInfo("Dworzec Gdański", 200), allStationsInternalList, _allButtons.ElementAt(15)),
                new FieldStreet(_bankAccount, new StreetCardInfo(Color.DarkOrange, "Obozowa", 180, 14, 70, 200, 550, 750, 950, 100), allStreetsInternalList, _allButtons.ElementAt(16)),
                new FieldChance(new CardInfo("Community Chest 2"), emptyPileOfCards, _allButtons.ElementAt(17)),
                new FieldStreet(_bankAccount, new StreetCardInfo(Color.DarkOrange, "Górczewska", 180, 14, 70, 200, 550, 750, 950, 100), allStreetsInternalList, _allButtons.ElementAt(18)),
                new FieldStreet(_bankAccount, new StreetCardInfo(Color.DarkOrange, "Wolska", 200, 16, 80, 220, 600, 800, 1000, 100), allStreetsInternalList, _allButtons.ElementAt(19)),
                new FieldParking(_allButtons.ElementAt(20)),
                new FieldStreet(_bankAccount, new StreetCardInfo(Color.Red, "Mickiewicza", 220, 18, 90, 250, 700, 875, 1050, 150), allStreetsInternalList, _allButtons.ElementAt(21)),
                new FieldChance(new CardInfo("Chance 2"), emptyPileOfCards, _allButtons.ElementAt(22)),
                new FieldStreet(_bankAccount, new StreetCardInfo(Color.Red, "Słowackiego", 220, 18, 90, 250, 700, 875, 1050, 150), allStreetsInternalList, _allButtons.ElementAt(23)),
                new FieldStreet(_bankAccount, new StreetCardInfo(Color.Red, "Plac Wilsona", 240, 20, 100, 300, 750, 925, 1100, 150), allStreetsInternalList, _allButtons.ElementAt(24)),
                new FieldStation(_bankAccount, new BuyableCardInfo("Dworzec Wschodni", 200), allStationsInternalList, _allButtons.ElementAt(25)),
                new FieldStreet(_bankAccount, new StreetCardInfo(Color.Yellow, "Świętokrzyska", 260, 22, 110, 330, 800, 975, 1150, 150), allStreetsInternalList, _allButtons.ElementAt(26)),
                new FieldStreet(_bankAccount, new StreetCardInfo(Color.Yellow, "Krakowskie Przedmieście", 260, 22, 110, 330, 800, 975, 1150, 150), allStreetsInternalList, _allButtons.ElementAt(27)),
                new FieldUtility(_bankAccount, new BuyableCardInfo("Wodociągi", 150), allUtilitiesInternalList, _allButtons.ElementAt(28)),
                new FieldStreet(_bankAccount, new StreetCardInfo(Color.Yellow, "Nowy Świat", 280, 24, 120, 360, 850, 1025, 1200, 150), allStreetsInternalList, _allButtons.ElementAt(29)),
                new FieldGoToPrison(prison, _allButtons.ElementAt(30)),
                new FieldStreet(_bankAccount, new StreetCardInfo(Color.YellowGreen, "Plac Trzech Krzyży", 300, 26, 130, 390, 900, 1100, 1275, 200), allStreetsInternalList, _allButtons.ElementAt(31)),
                new FieldStreet(_bankAccount, new StreetCardInfo(Color.YellowGreen, "Marszałkowska", 300, 26, 130, 390, 900, 1100, 1275, 200), allStreetsInternalList, _allButtons.ElementAt(32)),
                new FieldChance(new CardInfo("Community Chest 3"), emptyPileOfCards, _allButtons.ElementAt(33)),
                new FieldStreet(_bankAccount, new StreetCardInfo(Color.YellowGreen, "Al. Jerozolimskie", 320, 28, 150, 450, 1000, 1200, 1400, 200), allStreetsInternalList, _allButtons.ElementAt(34)),
                new FieldStation(_bankAccount, new BuyableCardInfo("Dworzec centralny", 200), allStationsInternalList, _allButtons.ElementAt(35)),
                new FieldChance(new CardInfo("Chance 3"), emptyPileOfCards, _allButtons.ElementAt(36)),
                new FieldStreet(_bankAccount, new StreetCardInfo(Color.MidnightBlue, "Belwederska", 350, 35, 175, 500, 1100, 1300, 1500, 200), allStreetsInternalList, _allButtons.ElementAt(37)),
                new FieldTax("Tax 2", 100, _allButtons.ElementAt(38)),
                new FieldStreet(_bankAccount, new StreetCardInfo(Color.MidnightBlue, "Al. Ujazdowskie", 400, 50, 200, 600, 1400, 1700, 2000, 200), allStreetsInternalList, _allButtons.ElementAt(39))
            };
            
            AddStationsToInternalList(allStationsInternalList, allFields);
            AddUtilitiesToInternalList(allUtilitiesInternalList, allFields);
            AddStreetsToInternalList(allStreetsInternalList, allFields);
            
            InitiateChanceCardGenerators(allFields);
            InitiateChestCardGenerator(allFields);

            UpdateCardPilesInChanceFields(allFields, _chanceCardsGenerator.GeneratePileOfCards(), _chestCardsGenerator.GeneratePileOfCards());

            return allFields;
        }

        private void AddStationsToInternalList(List<FieldStation> allStationsInternalList, List<Field> allFields)
        {
            foreach (var field in allFields)
            {
                if (field is FieldStation)
                    allStationsInternalList.Add((FieldStation)field);
            }
        }

        private void AddUtilitiesToInternalList(List<FieldUtility> allUtilitiesInternalList, List<Field> allFields)
        {
            foreach (var field in allFields)
            {
                if (field is FieldUtility)
                    allUtilitiesInternalList.Add((FieldUtility)field);
            }
        }

        private void AddStreetsToInternalList(List<FieldStreet> allStreetsInternalList, List<Field> allFields)
        {
            foreach (var field in allFields)
            {
                if (field is FieldStreet)
                    allStreetsInternalList.Add((FieldStreet)field);
            }
        }

        private void InitiateChanceCardGenerators(List<Field> allFields)
        {
            List<Field> chanceGoToFields = new List<Field>()
            {
                allFields.ElementAt(39),
                allFields.ElementAt(30),
                allFields.ElementAt(24),
                allFields.ElementAt(15),
                allFields.ElementAt(11),
                allFields.ElementAt(0)
            };

            Dictionary<int, string> chanceCashAndDescription = new Dictionary<int, string>()
            {
                { 50, "Bank pays you dividend" },
                { 100, "You have won a crossword competition" },
                { 150, "You have received loan" },
                { -15, "Speeding ticket" },
                { -20, "Fine" },
                { -150, "Pay for school" }
            };

            _chanceCardsGenerator = new ChanceCardsGenerator(chanceGoToFields, chanceCashAndDescription);
        }

        private void InitiateChestCardGenerator(List<Field> allFields)
        {
            List<Field> chestGoToFields = new List<Field>()
            {
                allFields.ElementAt(30),
                allFields.ElementAt(0)
            };

            Dictionary<int, string> chestCashAndDescription = new Dictionary<int, string>()
            {
                { 10, "You have won beauty contest" },
                { 20, "Income tax refund" },
                { 25, "You found some cash laying on street" },
                { 50, "You have sold the shares on stock exchange" },
                { 100, "Life insurance matures" },
                { 150, "You inherit" },
                { 200, "Bank error in your favor" },
                { -50, "Doctor's fee" },
                { -100, "Pay life insurance" },
                { -150, "Hospital fees" }
            };

            _chestCardsGenerator = new ChanceCardsGenerator(chestGoToFields, chestCashAndDescription);
        }

        private void UpdateCardPilesInChanceFields(List<Field> allFields, ChanceCardsPile chanceCardsPile, ChanceCardsPile chestCardsPile)
        {
            int counter = 0;

            foreach (var field in allFields)
            {
                if (field is FieldChance fieldChance)
                {
                    counter++;

                    if (counter % 2 == 0)
                        fieldChance.UpdateCardPile(chanceCardsPile);
                    else
                        fieldChance.UpdateCardPile(chestCardsPile);
                }
            }
        }

    }
}

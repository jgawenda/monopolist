﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monopolist.Core.Helpers;
using Monopolist.Core.Actions.NewGame.PreparingBoardElements.CardsGenerating.Helpers;
using Monopolist.Board.Types.Fields.Actions.Chance.Types;
using Monopolist.Board.Types.Fields.Chance;
using Monopolist.Board.Types.Fields;

namespace Monopolist.Core.Actions.NewGame.PreparingBoardElements.CardsGenerating
{
    public class ChanceCardsGenerator
    {
        private List<ChanceCard> _allChanceCards;
        
        public ChanceCardsGenerator(List<Field> fieldsGoTo, Dictionary<int, string> cashAndDescription)
        {
            _allChanceCards = new List<ChanceCard>();

            _allChanceCards.AddRange(GenerateCardsGoTo(fieldsGoTo));
            _allChanceCards.AddRange(GenerateCardsCash(cashAndDescription));
        }

        private List<ChanceCard> GenerateCardsGoTo(List<Field> fieldsGoTo)
        {
            return CardsGenerator.GenerateGotoCards(fieldsGoTo);
        }

        private List<ChanceCard> GenerateCardsCash(Dictionary<int, string> cashAndDescription)
        {
            return CardsGenerator.GenerateCashCards(cashAndDescription);
        }

        private void RandomizeCardOrder()
        {
            ListShuffler.Shuffle(_allChanceCards);
        }

        public ChanceCardsPile GeneratePileOfCards()
        {
            RandomizeCardOrder();
            return new ChanceCardsPile(_allChanceCards);
        }
        
    }
}

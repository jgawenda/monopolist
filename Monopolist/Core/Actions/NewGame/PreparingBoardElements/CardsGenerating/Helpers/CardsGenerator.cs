﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monopolist.Board.Types.Fields.Actions.Chance.Types;
using Monopolist.Board.Types.Fields;

namespace Monopolist.Core.Actions.NewGame.PreparingBoardElements.CardsGenerating.Helpers
{
    public static class CardsGenerator
    {
        public static List<ChanceCard> GenerateGotoCards(List<Field> fieldsGoTo)
        {
            List<ChanceCard> outputList = new List<ChanceCard>();

            foreach (Field field in fieldsGoTo)
                outputList.Add(new ChanceGoTo(String.Format("Go to {0}", field.Name), field));

            return outputList;
        }
        
        public static List<ChanceCard> GenerateCashCards(Dictionary<int, string> cashAndDescription)
        {
            List<ChanceCard> outputList = new List<ChanceCard>();

            foreach (var pair in cashAndDescription)
                outputList.Add(new ChanceCash(String.Format("{0}, ${1}.", pair.Value, pair.Key), pair.Key));

            return outputList;
        }

    }
}

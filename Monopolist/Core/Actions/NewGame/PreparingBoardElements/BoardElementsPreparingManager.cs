﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Actions.NewGame.PreparingBoardElements.FieldsGenerating;
using Monopolist.Board.Types.Fields.Containers;
using Monopolist.Core.Types.Players;

namespace Monopolist.Core.Actions.NewGame.PreparingBoardElements
{
    /// <summary>
    /// Class used to prepare Board elements (Fields, Field-Button association)
    /// </summary>
    public static class BoardElementsPreparingManager
    {
        public static AllFieldsContainer PrepareAllFields(PlayerAccount bankAccount, List<Button> allButtons)
        {
            FieldsGenerator fieldsGenerator = new FieldsGenerator(bankAccount, allButtons);

            return new AllFieldsContainer(fieldsGenerator.GenerateFields());
        }
        
        public static void PlacePlayersOnStart(List<PlayerAccount> allPlayers, AllFieldsContainer allFieldsContainer)
        {
            foreach (PlayerAccount player in allPlayers)
                allFieldsContainer.Start.AddVisitor(player);
        }

    }
}

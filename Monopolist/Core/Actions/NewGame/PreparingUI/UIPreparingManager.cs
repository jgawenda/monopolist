﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monopolist.Core.Actions.NewGame.PreparingUI.Helpers;
using Monopolist.Core.Managers.NewGame;
using Monopolist.NewGame.Result;

namespace Monopolist.Core.Actions.NewGame.PreparingUI
{
    public static class UIPreparingManager
    {
        public static GameFormsManager PrepareAllForms(NewGameFormResult newGameFormResult)
        {
            FormsInitializingManager formsInitializingManager = new FormsInitializingManager();
            return formsInitializingManager.InitializeAllForms(newGameFormResult);
        }

    }
}

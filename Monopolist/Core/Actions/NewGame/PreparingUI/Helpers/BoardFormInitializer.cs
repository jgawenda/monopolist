﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monopolist.Board;

namespace Monopolist.Core.Actions.NewGame.PreparingUI.Helpers
{
    static class BoardFormInitializer
    {
        public static BoardForm InitializeBoardForm()
        {
            BoardForm boardForm = new BoardForm();
            boardForm.Show();

            return boardForm;
        }

    }
}

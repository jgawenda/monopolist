﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.NewGame.Result;
using Monopolist.Player;
using Monopolist.Board;
using Monopolist.Core.Managers.NewGame;

namespace Monopolist.Core.Actions.NewGame.PreparingUI.Helpers
{
    public class FormsInitializingManager
    {
        private PlayersFormsInitializer _playersFormsInitializer;
        
        private Dictionary<int, PlayerForm> InitializePlayersForms(NewGameFormResult newGameFormResult)
        {
            return _playersFormsInitializer.InitializePlayerForms(newGameFormResult.Players);
        }

        private BoardForm InitializeBoardForm()
        {
            return BoardFormInitializer.InitializeBoardForm();
        }
        
        public GameFormsManager InitializeAllForms(NewGameFormResult newGameFormResult)
        {
            var boardForm = InitializeBoardForm();

            _playersFormsInitializer = new PlayersFormsInitializer(boardForm);

            var playersForms = InitializePlayersForms(newGameFormResult);
            
            return new GameFormsManager(boardForm, playersForms);
        }

    }
}

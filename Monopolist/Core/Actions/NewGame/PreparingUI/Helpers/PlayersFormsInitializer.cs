﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using Monopolist.Player;
using Monopolist.Core.Types.Players;
using Monopolist.Core.Helpers.NewGame;

namespace Monopolist.Core.Actions.NewGame.PreparingUI.Helpers
{
    class PlayersFormsInitializer
    {
        private PlayersFormsCoordGenerator _coordsGenerator;

        public PlayersFormsInitializer(Form parentForm)
        {
            _coordsGenerator = new PlayersFormsCoordGenerator(parentForm);
        }
        
        public Dictionary<int, PlayerForm> InitializePlayerForms(List<PlayerAccount> players)
        {
            Dictionary<int, PlayerForm> playersForms = new Dictionary<int, PlayerForm>();
            
            for (int i=0; i<players.Count; i++)
            {
                playersForms.Add(i, InitializeSinglePlayer(players.ElementAt(i), _coordsGenerator.GetNextCoord()));
            }

            return playersForms;
        }

        private PlayerForm InitializeSinglePlayer(PlayerAccount playerAccount, Point pointCoords)
        {
            PlayerForm playerForm = new PlayerForm(playerAccount) { StartPosition = FormStartPosition.Manual, Location = pointCoords };
            playerForm.Show();

            return playerForm;
        }

    }
}

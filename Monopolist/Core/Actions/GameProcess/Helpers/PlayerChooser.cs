﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monopolist.Core.Types.Players;

namespace Monopolist.Core.Actions.GameProcess.Helpers
{
    /// <summary>
    /// This Class is made to decide which Player should now make turn.
    /// </summary>
    public class PlayerChooser
    {
        private List<PlayerAccount> _playersAccounts;
        private int _counter;

        public PlayerChooser(List<PlayerAccount> playersAccounts)
        {
            _playersAccounts = playersAccounts;
            _counter = -1;
        }

        private void IncreaseCounter()
        {
            if (_counter < _playersAccounts.Count - 1)
                _counter++;
            else
                _counter = 0;
        }

        public PlayerAccount GetNextPlayer()
        {
            do
            {
                IncreaseCounter();
            } while (_playersAccounts.ElementAt(_counter).IsBankrupt);

            return _playersAccounts.ElementAt(_counter);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monopolist.Core.Types.Players;

namespace Monopolist.Core.Actions.GameProcess.Helpers
{
    public class GameEndChecker
    {
        public PlayerAccount Winner
        {
            get
            {
                return _allPlayers.Where(p => p.IsBankrupt == false).FirstOrDefault();
            }
        }

        private List<PlayerAccount> _allPlayers;

        public GameEndChecker(List<PlayerAccount> allPlayers)
        {
            _allPlayers = allPlayers;
        }

        public bool CheckIfGameHasEnded()
        {
            int howManyNonBankrupts = 0;
            bool result = false;

            foreach (var player in _allPlayers)
            {
                if (!player.IsBankrupt)
                    howManyNonBankrupts++;
            }

            if (howManyNonBankrupts == 1)
                result = true;

            return result;
        }

    }
}

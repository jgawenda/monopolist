﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monopolist.Player;

namespace Monopolist.Core.Actions.GameProcess.Helpers
{
    public class PlayerFormsUpdater
    {
        private Dictionary<int, PlayerForm> _allPlayerForms;

        public PlayerFormsUpdater(Dictionary<int, PlayerForm> allPlayerForms)
        {
            _allPlayerForms = allPlayerForms;
        }

        public void UpdateInfo()
        {
            foreach (var pair in _allPlayerForms)
                pair.Value.UpdateInfo();
        }

    }
}

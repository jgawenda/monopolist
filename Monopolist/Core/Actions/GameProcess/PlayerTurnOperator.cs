﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players.Containers;
using Monopolist.Board.Types.Fields.Containers;
using Monopolist.Core.Managers.NewGame;
using Monopolist.Core.Types.Players;
using Monopolist.Board.Forms.PlayerTurn;
using Monopolist.Core.Actions.GameProcess.Helpers;

namespace Monopolist.Core.Actions.GameProcess
{
    public class PlayerTurnOperator
    {
        private PlayerTurnForm _playerTurnForm;
        
        private GameFormsManager _gameFormsManager;

        private PlayerChooser _playerChooser;

        private GameEndChecker _gameEndChecker;

        public PlayerTurnOperator(GameFormsManager gameFormsManager, AllPlayersContainer allPlayersContainer, AllFieldsContainer allFieldsContainer)
        {
            _gameFormsManager = gameFormsManager;

            _playerTurnForm = new PlayerTurnForm(_gameFormsManager.BoardForm, allFieldsContainer, allPlayersContainer, new PlayerFormsUpdater(allPlayersContainer.AllPlayersForms), this);

            _playerChooser = new PlayerChooser(allPlayersContainer.AllPlayers);

            _gameEndChecker = new GameEndChecker(allPlayersContainer.AllPlayers);
        }

        private void PrepareTurn(PlayerAccount playerAccount)
        {
            _gameFormsManager.FocusFormsOnPlayer(playerAccount);
            _playerTurnForm.UpdatePlayerInfo(playerAccount);
        }

        private void MakeTurn()
        {
            _gameFormsManager.UnregisterForm(_playerTurnForm);

            if (!_gameEndChecker.CheckIfGameHasEnded())
            {
                PlayerAccount activePlayer = _playerChooser.GetNextPlayer();
                PrepareTurn(activePlayer);
                MessageBox.Show(String.Format("{0}'s turn.", activePlayer.Name));

                _playerTurnForm.Show();
                _gameFormsManager.RegisterForm(_playerTurnForm);
            }
            else
            {
                _gameFormsManager.UpdatePlayersForms();
                _gameFormsManager.FocusOnAllPlayerForms();
                MessageBox.Show(String.Format("The WINNER is {0} with ${1}.\nCongratulations!", _gameEndChecker.Winner.Name, _gameEndChecker.Winner.CashAccount.Cash), "WE HAVE A WINNER!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        public void Start()
        {
            MakeTurn();
        }

    }
}

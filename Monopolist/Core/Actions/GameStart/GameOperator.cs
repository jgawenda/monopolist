﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Types.Fields.Containers;
using Monopolist.Core.Managers.NewGame;
using Monopolist.Core.Actions.NewGame;
using Monopolist.NewGame.Result;
using Monopolist.Core.Types.Players.Containers;
using Monopolist.Core.Actions.GameProcess;

namespace Monopolist.Core.Actions.GameStart
{
    /// <summary>
    /// Class used to manage all gaming process
    /// </summary>
    public class GameOperator
    {
        // for preparing the Game
        private GamePreparingManager _gamePreparingManager;

        // for processing the Game
        private PlayerTurnOperator _playerTurnOperator;

        // for storing Game elements
        private AllFieldsContainer _allFieldsContainer;
        private AllPlayersContainer _allPlayersContainer;
        
        private GameFormsManager _gameFormsManager;
        
        public void PrepareGame(NewGameFormResult newGameFormResult)
        {
            _gamePreparingManager = new GamePreparingManager();

            _gameFormsManager = _gamePreparingManager.PrepareAllForms(newGameFormResult);
            _allFieldsContainer = _gamePreparingManager.PrepareBoardElements(newGameFormResult.Players, newGameFormResult.BankAccount, _gameFormsManager.BoardForm.AllFieldsButtons);
            
            // sending AllFieldsContainer to BoardForm
            _gameFormsManager.BoardForm.SaveAllFields(_allFieldsContainer);

            // refreshing some info in PlayerForms
            _gameFormsManager.UpdatePlayersForms();

            // save PlayerForms, PlayerAccounts in Container
            _allPlayersContainer = new AllPlayersContainer(newGameFormResult.Players, _gameFormsManager.PlayersForms, newGameFormResult.BankAccount);
        }

        public void StartGame()
        {
            _playerTurnOperator = new PlayerTurnOperator(_gameFormsManager, _allPlayersContainer, _allFieldsContainer);
            
            _playerTurnOperator.Start();
        }

        public void HideAllForms()
        {
            _gameFormsManager.HideAllForms();
        }

        public void UnhideAllForms()
        {
            _gameFormsManager.UnhideAllForms();
        }
        
    }
}

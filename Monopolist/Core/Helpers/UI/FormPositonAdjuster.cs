﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monopolist.Core.Helpers.UI
{
    public class FormPositonAdjuster
    {
        protected Form _childForm;

        protected ParentCoordsExtractor _parentCoordsExtractor;

        public FormPositonAdjuster(Form parentForm, Form childForm)
        {
            _childForm = childForm;
            _parentCoordsExtractor = new ParentCoordsExtractor(parentForm, _childForm);
        }
        
        public virtual void AdjustPosition(ParentCoordsExtractor.Location location)
        {
            _childForm.Location = _parentCoordsExtractor.GetCoords(location);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Monopolist.Core.Helpers.UI
{
    public class ParentCoordsExtractor
    {
        public enum Location
        {
            Same,
            Above,
            OnLeft,
            OnRight,
            Under
        }

        private Form _parentForm;
        private Form _childForm;

        private Point _returnPoint;

        public ParentCoordsExtractor(Form parentForm, Form childForm)
        {
            _parentForm = parentForm;
            _childForm = childForm;
        }

        private void ChangeCoordsAbove()
        {
            _returnPoint.Y -= _childForm.Height;
        }

        private void ChangeCoordsUnder()
        {
            _returnPoint.Y += _parentForm.Height;
        }

        private void ChangeCoordsToLeft()
        {
            _returnPoint.X -= _childForm.Width;
        }

        private void ChangeCoordsToRight()
        {
            _returnPoint.X += _parentForm.Width;
        }
        
        public Point GetCoords(Location location)
        {
            _returnPoint = new Point(_parentForm.Location.X, _parentForm.Location.Y);

            switch (location)
            {
                case Location.Above:
                    ChangeCoordsAbove();
                    break;
                case Location.Under:
                    ChangeCoordsUnder();
                    break;
                case Location.OnLeft:
                    ChangeCoordsToLeft();
                    break;
                case Location.OnRight:
                    ChangeCoordsToRight();
                    break;
            }

            return _returnPoint;
        }

    }
}

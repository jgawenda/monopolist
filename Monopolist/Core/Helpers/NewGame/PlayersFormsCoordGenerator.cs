﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Monopolist.Core.Helpers.NewGame
{
    public class PlayersFormsCoordGenerator
    {
        private int _parentPosX;
        private int _parentPosY;

        private int _posX;
        private int _posY;

        private int _counter;

        public PlayersFormsCoordGenerator(Form parentForm)
        {
            SaveParentCoords(parentForm);
            SetStartingCoords();
            _counter = -1;
        }

        private void SaveParentCoords(Form parentForm)
        {
            _parentPosX = parentForm.Location.X;
            _parentPosY = parentForm.Location.Y;
        }

        private void SetStartingCoords()
        {
            _posX = _parentPosX - 220;
            _posY = _parentPosY;
        }

        private void MoveToRightColumn()
        {
            _posX = _parentPosX + 820;
            _posY = _parentPosY;
        }

        private void MoveDown()
        {
            _posY += 210;
        }

        public Point GetNextCoord()
        {
            _counter++;

            switch (_counter)
            {
                case 0:
                    break;
                case 3:
                    MoveToRightColumn();
                    break;
                default:
                    MoveDown();
                    break;
            }
            
            return new Point(_posX, _posY);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopolist.Core.Helpers
{
    public static class ListShuffler
    {
        public static void Shuffle<T>(List<T> inputList)
        {
            Random random = new Random();

            int randomIndex = 0;

            List<T> tempList = inputList.ToList();
            inputList.Clear();

            int howManyElements = tempList.Count;

            for (int i=0; i<howManyElements; i++)
            {
                randomIndex = random.Next(0, tempList.Count);
                inputList.Add(tempList.ElementAt(randomIndex));
                tempList.RemoveAt(randomIndex);
            }
            
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board;
using Monopolist.Player;
using Monopolist.Core.Types.Players;

namespace Monopolist.Core.Managers.NewGame
{
    /// <summary>
    /// Class for managing all forms: collecting, changing their properties
    /// </summary>
    public class GameFormsManager
    {
        public BoardForm BoardForm { get; }
        public Dictionary<int, PlayerForm> PlayersForms { get; }

        private List<Form> _allResponsiveForms;

        public GameFormsManager(BoardForm boardForm, Dictionary<int, PlayerForm> playersForms)
        {
            BoardForm = boardForm;
            PlayersForms = playersForms;

            _allResponsiveForms = new List<Form>();

            RegisterForm(BoardForm);
            RegisterAllPlayersForms();
        }

        /// <summary>
        /// Change opacity of one form.
        /// </summary>
        private void ChangeOpacity(PlayerForm playerForm, double value)
        {
            playerForm.Opacity = value;
        }

        private void RegisterAllPlayersForms()
        {
            foreach (var pair in PlayersForms)
                RegisterForm(pair.Value);
        }

        /// <summary>
        /// Use this method to register another Form. All Forms registered will be responsive to actions such as hiding all Forms etc.
        /// </summary>
        /// <param name="form"></param>
        public void RegisterForm(Form form)
        {
            if (!_allResponsiveForms.Contains(form))
                _allResponsiveForms.Add(form);
        }

        /// <summary>
        /// Unregistering means that the Form will be no longer responsive to actions such as hiding all Forms etc.
        /// </summary>
        /// <param name="form"></param>
        public void UnregisterForm(Form form)
        {
            _allResponsiveForms.Remove(form);
        }

        public void HideAllForms()
        {
            foreach (Form form in _allResponsiveForms)
                form.Hide();
        }

        public void UnhideAllForms()
        {
            foreach (Form form in _allResponsiveForms)
                form.Show();
        }
        
        /// <summary>
        /// Change opacity of all PlayerForms (value between 0 and 1).
        /// </summary>
        public void ChangeOpacity(double value)
        {
            foreach (var playerFormPair in PlayersForms)
            {
                ChangeOpacity(playerFormPair.Value, value);
            }
        }

        public void UpdatePlayersForms()
        {
            foreach (var playerFormPair in PlayersForms)
            {
                playerFormPair.Value.UpdateInfo();
            }
        }

        /// <summary>
        /// Leave the 100% opacity only on active Player's Form.
        /// </summary>
        public void FocusFormsOnPlayer(PlayerAccount playerAccount)
        {
            foreach (var playerFormPair in PlayersForms)
            {
                if (playerFormPair.Value.PlayerAccount == playerAccount)
                {
                    playerFormPair.Value.Enabled = true;
                    ChangeOpacity(playerFormPair.Value, 1);
                }
                else
                {
                    playerFormPair.Value.Enabled = false;
                    ChangeOpacity(playerFormPair.Value, 0.5);
                }
            }
        }

        public void FocusOnAllPlayerForms()
        {
            foreach (var playerFormPair in PlayersForms)
            {
                ChangeOpacity(playerFormPair.Value, 1);
                playerFormPair.Value.Enabled = true;
            }
        }
        
    }
}

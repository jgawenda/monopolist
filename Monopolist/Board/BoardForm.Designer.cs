﻿namespace Monopolist.Board
{
    partial class BoardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BoardForm));
            this.buttonStreetBrown1 = new System.Windows.Forms.Button();
            this.buttonStreetColorBrown1 = new System.Windows.Forms.Button();
            this.buttonFieldStart = new System.Windows.Forms.Button();
            this.buttonFieldCommunity1 = new System.Windows.Forms.Button();
            this.buttonTax1 = new System.Windows.Forms.Button();
            this.buttonStreetColorBrown2 = new System.Windows.Forms.Button();
            this.buttonStreetBrown2 = new System.Windows.Forms.Button();
            this.buttonStreetColorBlue2 = new System.Windows.Forms.Button();
            this.buttonStreetBlue2 = new System.Windows.Forms.Button();
            this.buttonFieldChance1 = new System.Windows.Forms.Button();
            this.buttonStreetBlue1 = new System.Windows.Forms.Button();
            this.buttonStation1 = new System.Windows.Forms.Button();
            this.buttonStreetColorBlue3 = new System.Windows.Forms.Button();
            this.buttonStreetBlue3 = new System.Windows.Forms.Button();
            this.buttonStreetPurple1 = new System.Windows.Forms.Button();
            this.buttonStreetColorPurple1 = new System.Windows.Forms.Button();
            this.buttonPowerStation = new System.Windows.Forms.Button();
            this.buttonStreetColorPurple3 = new System.Windows.Forms.Button();
            this.buttonStreetPurple3 = new System.Windows.Forms.Button();
            this.buttonStreetColorPurple2 = new System.Windows.Forms.Button();
            this.buttonStreetPurple2 = new System.Windows.Forms.Button();
            this.buttonStreetColorOrange2 = new System.Windows.Forms.Button();
            this.buttonStreetOrange2 = new System.Windows.Forms.Button();
            this.buttonFieldCommunity2 = new System.Windows.Forms.Button();
            this.buttonStreetColorOrange1 = new System.Windows.Forms.Button();
            this.buttonStreetOrange1 = new System.Windows.Forms.Button();
            this.buttonStation2 = new System.Windows.Forms.Button();
            this.buttonFieldFreeParking = new System.Windows.Forms.Button();
            this.buttonStreetColorOrange3 = new System.Windows.Forms.Button();
            this.buttonStreetOrange3 = new System.Windows.Forms.Button();
            this.buttonFieldGoToPrison = new System.Windows.Forms.Button();
            this.buttonStreetColorRed1 = new System.Windows.Forms.Button();
            this.buttonStreetRed1 = new System.Windows.Forms.Button();
            this.buttonFieldChance2 = new System.Windows.Forms.Button();
            this.buttonStreetColorRed2 = new System.Windows.Forms.Button();
            this.buttonStreetRed2 = new System.Windows.Forms.Button();
            this.buttonStreetColorRed3 = new System.Windows.Forms.Button();
            this.buttonStreetRed3 = new System.Windows.Forms.Button();
            this.buttonStation3 = new System.Windows.Forms.Button();
            this.buttonStreetColorYellow1 = new System.Windows.Forms.Button();
            this.buttonStreetYellow1 = new System.Windows.Forms.Button();
            this.buttonStreetColorYellow2 = new System.Windows.Forms.Button();
            this.buttonStreetYellow2 = new System.Windows.Forms.Button();
            this.buttonWaterSupply = new System.Windows.Forms.Button();
            this.buttonStreetColorYellow3 = new System.Windows.Forms.Button();
            this.buttonStreetYellow3 = new System.Windows.Forms.Button();
            this.buttonStreetColorGreen1 = new System.Windows.Forms.Button();
            this.buttonStreetColorGreen2 = new System.Windows.Forms.Button();
            this.buttonStreetColorGreen3 = new System.Windows.Forms.Button();
            this.buttonStreetColorDark1 = new System.Windows.Forms.Button();
            this.buttonStreetColorDark2 = new System.Windows.Forms.Button();
            this.buttonStreetGreen1 = new System.Windows.Forms.Button();
            this.buttonStreetGreen2 = new System.Windows.Forms.Button();
            this.buttonFieldCommunity3 = new System.Windows.Forms.Button();
            this.buttonStreetGreen3 = new System.Windows.Forms.Button();
            this.buttonStation4 = new System.Windows.Forms.Button();
            this.buttonFieldChance3 = new System.Windows.Forms.Button();
            this.buttonStreetDark1 = new System.Windows.Forms.Button();
            this.buttonTax2 = new System.Windows.Forms.Button();
            this.buttonStreetDark2 = new System.Windows.Forms.Button();
            this.buttonStreetColorBlue1 = new System.Windows.Forms.Button();
            this.buttonPrison = new System.Windows.Forms.Button();
            this.labelCopyright = new System.Windows.Forms.Label();
            this.labelGameTitle = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonStreetBrown1
            // 
            this.buttonStreetBrown1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetBrown1.Location = new System.Drawing.Point(-1, 632);
            this.buttonStreetBrown1.Name = "buttonStreetBrown1";
            this.buttonStreetBrown1.Size = new System.Drawing.Size(74, 68);
            this.buttonStreetBrown1.TabIndex = 0;
            this.buttonStreetBrown1.TabStop = false;
            this.buttonStreetBrown1.Text = "Konopacka\r\n\r\n$60";
            this.buttonStreetBrown1.UseVisualStyleBackColor = true;
            // 
            // buttonStreetColorBrown1
            // 
            this.buttonStreetColorBrown1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.buttonStreetColorBrown1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetColorBrown1.Location = new System.Drawing.Point(72, 632);
            this.buttonStreetColorBrown1.Name = "buttonStreetColorBrown1";
            this.buttonStreetColorBrown1.Size = new System.Drawing.Size(27, 68);
            this.buttonStreetColorBrown1.TabIndex = 1;
            this.buttonStreetColorBrown1.TabStop = false;
            this.buttonStreetColorBrown1.UseVisualStyleBackColor = false;
            // 
            // buttonFieldStart
            // 
            this.buttonFieldStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFieldStart.Location = new System.Drawing.Point(-1, 699);
            this.buttonFieldStart.Name = "buttonFieldStart";
            this.buttonFieldStart.Size = new System.Drawing.Size(100, 100);
            this.buttonFieldStart.TabIndex = 2;
            this.buttonFieldStart.TabStop = false;
            this.buttonFieldStart.Text = "Start\r\n\r\nPobierz $200";
            this.buttonFieldStart.UseVisualStyleBackColor = true;
            // 
            // buttonFieldCommunity1
            // 
            this.buttonFieldCommunity1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFieldCommunity1.Location = new System.Drawing.Point(-1, 565);
            this.buttonFieldCommunity1.Name = "buttonFieldCommunity1";
            this.buttonFieldCommunity1.Size = new System.Drawing.Size(100, 68);
            this.buttonFieldCommunity1.TabIndex = 4;
            this.buttonFieldCommunity1.TabStop = false;
            this.buttonFieldCommunity1.Text = "Kasa społeczna";
            this.buttonFieldCommunity1.UseVisualStyleBackColor = true;
            // 
            // buttonTax1
            // 
            this.buttonTax1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTax1.Location = new System.Drawing.Point(-1, 431);
            this.buttonTax1.Name = "buttonTax1";
            this.buttonTax1.Size = new System.Drawing.Size(100, 68);
            this.buttonTax1.TabIndex = 8;
            this.buttonTax1.TabStop = false;
            this.buttonTax1.Text = "Podatek\r\nDochodowy\r\n\r\n-$200";
            this.buttonTax1.UseVisualStyleBackColor = true;
            // 
            // buttonStreetColorBrown2
            // 
            this.buttonStreetColorBrown2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.buttonStreetColorBrown2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetColorBrown2.Location = new System.Drawing.Point(72, 498);
            this.buttonStreetColorBrown2.Name = "buttonStreetColorBrown2";
            this.buttonStreetColorBrown2.Size = new System.Drawing.Size(27, 68);
            this.buttonStreetColorBrown2.TabIndex = 7;
            this.buttonStreetColorBrown2.TabStop = false;
            this.buttonStreetColorBrown2.UseVisualStyleBackColor = false;
            // 
            // buttonStreetBrown2
            // 
            this.buttonStreetBrown2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetBrown2.Location = new System.Drawing.Point(-1, 498);
            this.buttonStreetBrown2.Name = "buttonStreetBrown2";
            this.buttonStreetBrown2.Size = new System.Drawing.Size(74, 68);
            this.buttonStreetBrown2.TabIndex = 6;
            this.buttonStreetBrown2.TabStop = false;
            this.buttonStreetBrown2.Text = "Stalowa\r\n\r\n$60";
            this.buttonStreetBrown2.UseVisualStyleBackColor = true;
            // 
            // buttonStreetColorBlue2
            // 
            this.buttonStreetColorBlue2.BackColor = System.Drawing.Color.DodgerBlue;
            this.buttonStreetColorBlue2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetColorBlue2.Location = new System.Drawing.Point(72, 163);
            this.buttonStreetColorBlue2.Name = "buttonStreetColorBlue2";
            this.buttonStreetColorBlue2.Size = new System.Drawing.Size(27, 68);
            this.buttonStreetColorBlue2.TabIndex = 17;
            this.buttonStreetColorBlue2.TabStop = false;
            this.buttonStreetColorBlue2.UseVisualStyleBackColor = false;
            // 
            // buttonStreetBlue2
            // 
            this.buttonStreetBlue2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetBlue2.Location = new System.Drawing.Point(-1, 163);
            this.buttonStreetBlue2.Name = "buttonStreetBlue2";
            this.buttonStreetBlue2.Size = new System.Drawing.Size(74, 68);
            this.buttonStreetBlue2.TabIndex = 16;
            this.buttonStreetBlue2.TabStop = false;
            this.buttonStreetBlue2.Text = "Jagiell-\r\nońska\r\n\r\n$100";
            this.buttonStreetBlue2.UseVisualStyleBackColor = true;
            // 
            // buttonFieldChance1
            // 
            this.buttonFieldChance1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFieldChance1.Location = new System.Drawing.Point(-1, 230);
            this.buttonFieldChance1.Name = "buttonFieldChance1";
            this.buttonFieldChance1.Size = new System.Drawing.Size(100, 68);
            this.buttonFieldChance1.TabIndex = 14;
            this.buttonFieldChance1.TabStop = false;
            this.buttonFieldChance1.Text = "Szansa";
            this.buttonFieldChance1.UseVisualStyleBackColor = true;
            // 
            // buttonStreetBlue1
            // 
            this.buttonStreetBlue1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetBlue1.Location = new System.Drawing.Point(-1, 297);
            this.buttonStreetBlue1.Name = "buttonStreetBlue1";
            this.buttonStreetBlue1.Size = new System.Drawing.Size(74, 68);
            this.buttonStreetBlue1.TabIndex = 12;
            this.buttonStreetBlue1.TabStop = false;
            this.buttonStreetBlue1.Text = "Radzy-\r\nmińska\r\n\r\n$100";
            this.buttonStreetBlue1.UseVisualStyleBackColor = true;
            // 
            // buttonStation1
            // 
            this.buttonStation1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStation1.Location = new System.Drawing.Point(-1, 364);
            this.buttonStation1.Name = "buttonStation1";
            this.buttonStation1.Size = new System.Drawing.Size(100, 68);
            this.buttonStation1.TabIndex = 10;
            this.buttonStation1.TabStop = false;
            this.buttonStation1.Text = "Dworzec\r\nZachodni\r\n\r\n$200";
            this.buttonStation1.UseVisualStyleBackColor = true;
            // 
            // buttonStreetColorBlue3
            // 
            this.buttonStreetColorBlue3.BackColor = System.Drawing.Color.DodgerBlue;
            this.buttonStreetColorBlue3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetColorBlue3.Location = new System.Drawing.Point(72, 96);
            this.buttonStreetColorBlue3.Name = "buttonStreetColorBlue3";
            this.buttonStreetColorBlue3.Size = new System.Drawing.Size(27, 68);
            this.buttonStreetColorBlue3.TabIndex = 19;
            this.buttonStreetColorBlue3.TabStop = false;
            this.buttonStreetColorBlue3.UseVisualStyleBackColor = false;
            // 
            // buttonStreetBlue3
            // 
            this.buttonStreetBlue3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetBlue3.Location = new System.Drawing.Point(-1, 96);
            this.buttonStreetBlue3.Name = "buttonStreetBlue3";
            this.buttonStreetBlue3.Size = new System.Drawing.Size(74, 68);
            this.buttonStreetBlue3.TabIndex = 18;
            this.buttonStreetBlue3.TabStop = false;
            this.buttonStreetBlue3.Text = "Targowa\r\n\r\n$120";
            this.buttonStreetBlue3.UseVisualStyleBackColor = true;
            // 
            // buttonStreetPurple1
            // 
            this.buttonStreetPurple1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetPurple1.Location = new System.Drawing.Point(97, -1);
            this.buttonStreetPurple1.Name = "buttonStreetPurple1";
            this.buttonStreetPurple1.Size = new System.Drawing.Size(68, 74);
            this.buttonStreetPurple1.TabIndex = 20;
            this.buttonStreetPurple1.TabStop = false;
            this.buttonStreetPurple1.Text = "Płowiecka\r\n\r\n$140";
            this.buttonStreetPurple1.UseVisualStyleBackColor = true;
            // 
            // buttonStreetColorPurple1
            // 
            this.buttonStreetColorPurple1.BackColor = System.Drawing.Color.Violet;
            this.buttonStreetColorPurple1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetColorPurple1.Location = new System.Drawing.Point(97, 72);
            this.buttonStreetColorPurple1.Name = "buttonStreetColorPurple1";
            this.buttonStreetColorPurple1.Size = new System.Drawing.Size(68, 27);
            this.buttonStreetColorPurple1.TabIndex = 21;
            this.buttonStreetColorPurple1.TabStop = false;
            this.buttonStreetColorPurple1.UseVisualStyleBackColor = false;
            // 
            // buttonPowerStation
            // 
            this.buttonPowerStation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPowerStation.Location = new System.Drawing.Point(164, -1);
            this.buttonPowerStation.Name = "buttonPowerStation";
            this.buttonPowerStation.Size = new System.Drawing.Size(68, 100);
            this.buttonPowerStation.TabIndex = 22;
            this.buttonPowerStation.TabStop = false;
            this.buttonPowerStation.Text = "Elektro-\r\nwnia\r\n\r\n$150";
            this.buttonPowerStation.UseVisualStyleBackColor = true;
            // 
            // buttonStreetColorPurple3
            // 
            this.buttonStreetColorPurple3.BackColor = System.Drawing.Color.Violet;
            this.buttonStreetColorPurple3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetColorPurple3.Location = new System.Drawing.Point(298, 72);
            this.buttonStreetColorPurple3.Name = "buttonStreetColorPurple3";
            this.buttonStreetColorPurple3.Size = new System.Drawing.Size(68, 27);
            this.buttonStreetColorPurple3.TabIndex = 27;
            this.buttonStreetColorPurple3.TabStop = false;
            this.buttonStreetColorPurple3.UseVisualStyleBackColor = false;
            // 
            // buttonStreetPurple3
            // 
            this.buttonStreetPurple3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetPurple3.Location = new System.Drawing.Point(298, -1);
            this.buttonStreetPurple3.Name = "buttonStreetPurple3";
            this.buttonStreetPurple3.Size = new System.Drawing.Size(68, 74);
            this.buttonStreetPurple3.TabIndex = 26;
            this.buttonStreetPurple3.TabStop = false;
            this.buttonStreetPurple3.Text = "Grocho-\r\nwska\r\n\r\n$160";
            this.buttonStreetPurple3.UseVisualStyleBackColor = true;
            // 
            // buttonStreetColorPurple2
            // 
            this.buttonStreetColorPurple2.BackColor = System.Drawing.Color.Violet;
            this.buttonStreetColorPurple2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetColorPurple2.Location = new System.Drawing.Point(231, 72);
            this.buttonStreetColorPurple2.Name = "buttonStreetColorPurple2";
            this.buttonStreetColorPurple2.Size = new System.Drawing.Size(68, 27);
            this.buttonStreetColorPurple2.TabIndex = 25;
            this.buttonStreetColorPurple2.TabStop = false;
            this.buttonStreetColorPurple2.UseVisualStyleBackColor = false;
            // 
            // buttonStreetPurple2
            // 
            this.buttonStreetPurple2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetPurple2.Location = new System.Drawing.Point(231, -1);
            this.buttonStreetPurple2.Name = "buttonStreetPurple2";
            this.buttonStreetPurple2.Size = new System.Drawing.Size(68, 74);
            this.buttonStreetPurple2.TabIndex = 24;
            this.buttonStreetPurple2.TabStop = false;
            this.buttonStreetPurple2.Text = "Marsa\r\n\r\n$140";
            this.buttonStreetPurple2.UseVisualStyleBackColor = true;
            // 
            // buttonStreetColorOrange2
            // 
            this.buttonStreetColorOrange2.BackColor = System.Drawing.Color.DarkOrange;
            this.buttonStreetColorOrange2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetColorOrange2.Location = new System.Drawing.Point(566, 72);
            this.buttonStreetColorOrange2.Name = "buttonStreetColorOrange2";
            this.buttonStreetColorOrange2.Size = new System.Drawing.Size(68, 27);
            this.buttonStreetColorOrange2.TabIndex = 35;
            this.buttonStreetColorOrange2.TabStop = false;
            this.buttonStreetColorOrange2.UseVisualStyleBackColor = false;
            // 
            // buttonStreetOrange2
            // 
            this.buttonStreetOrange2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetOrange2.Location = new System.Drawing.Point(566, -1);
            this.buttonStreetOrange2.Name = "buttonStreetOrange2";
            this.buttonStreetOrange2.Size = new System.Drawing.Size(68, 74);
            this.buttonStreetOrange2.TabIndex = 34;
            this.buttonStreetOrange2.TabStop = false;
            this.buttonStreetOrange2.Text = "Górczew-\r\nska\r\n\r\n$180";
            this.buttonStreetOrange2.UseVisualStyleBackColor = true;
            // 
            // buttonFieldCommunity2
            // 
            this.buttonFieldCommunity2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFieldCommunity2.Location = new System.Drawing.Point(499, -1);
            this.buttonFieldCommunity2.Name = "buttonFieldCommunity2";
            this.buttonFieldCommunity2.Size = new System.Drawing.Size(68, 100);
            this.buttonFieldCommunity2.TabIndex = 32;
            this.buttonFieldCommunity2.TabStop = false;
            this.buttonFieldCommunity2.Text = "Kasa\r\nspołeczna";
            this.buttonFieldCommunity2.UseVisualStyleBackColor = true;
            // 
            // buttonStreetColorOrange1
            // 
            this.buttonStreetColorOrange1.BackColor = System.Drawing.Color.DarkOrange;
            this.buttonStreetColorOrange1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetColorOrange1.Location = new System.Drawing.Point(432, 72);
            this.buttonStreetColorOrange1.Name = "buttonStreetColorOrange1";
            this.buttonStreetColorOrange1.Size = new System.Drawing.Size(68, 27);
            this.buttonStreetColorOrange1.TabIndex = 31;
            this.buttonStreetColorOrange1.TabStop = false;
            this.buttonStreetColorOrange1.UseVisualStyleBackColor = false;
            // 
            // buttonStreetOrange1
            // 
            this.buttonStreetOrange1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetOrange1.Location = new System.Drawing.Point(432, -1);
            this.buttonStreetOrange1.Name = "buttonStreetOrange1";
            this.buttonStreetOrange1.Size = new System.Drawing.Size(68, 74);
            this.buttonStreetOrange1.TabIndex = 30;
            this.buttonStreetOrange1.TabStop = false;
            this.buttonStreetOrange1.Text = "Obozowa\r\n\r\n$180";
            this.buttonStreetOrange1.UseVisualStyleBackColor = true;
            // 
            // buttonStation2
            // 
            this.buttonStation2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStation2.Location = new System.Drawing.Point(365, -1);
            this.buttonStation2.Name = "buttonStation2";
            this.buttonStation2.Size = new System.Drawing.Size(68, 100);
            this.buttonStation2.TabIndex = 28;
            this.buttonStation2.TabStop = false;
            this.buttonStation2.Text = "Dworzec\r\nGdański\r\n\r\n$200";
            this.buttonStation2.UseVisualStyleBackColor = true;
            // 
            // buttonFieldFreeParking
            // 
            this.buttonFieldFreeParking.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFieldFreeParking.Location = new System.Drawing.Point(699, -1);
            this.buttonFieldFreeParking.Name = "buttonFieldFreeParking";
            this.buttonFieldFreeParking.Size = new System.Drawing.Size(100, 100);
            this.buttonFieldFreeParking.TabIndex = 36;
            this.buttonFieldFreeParking.TabStop = false;
            this.buttonFieldFreeParking.Text = "Bezpłatny\r\nparking";
            this.buttonFieldFreeParking.UseVisualStyleBackColor = true;
            // 
            // buttonStreetColorOrange3
            // 
            this.buttonStreetColorOrange3.BackColor = System.Drawing.Color.DarkOrange;
            this.buttonStreetColorOrange3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetColorOrange3.Location = new System.Drawing.Point(633, 72);
            this.buttonStreetColorOrange3.Name = "buttonStreetColorOrange3";
            this.buttonStreetColorOrange3.Size = new System.Drawing.Size(68, 27);
            this.buttonStreetColorOrange3.TabIndex = 38;
            this.buttonStreetColorOrange3.TabStop = false;
            this.buttonStreetColorOrange3.UseVisualStyleBackColor = false;
            // 
            // buttonStreetOrange3
            // 
            this.buttonStreetOrange3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetOrange3.Location = new System.Drawing.Point(633, -1);
            this.buttonStreetOrange3.Name = "buttonStreetOrange3";
            this.buttonStreetOrange3.Size = new System.Drawing.Size(68, 74);
            this.buttonStreetOrange3.TabIndex = 37;
            this.buttonStreetOrange3.TabStop = false;
            this.buttonStreetOrange3.Text = "Wolska\r\n\r\n$200";
            this.buttonStreetOrange3.UseVisualStyleBackColor = true;
            // 
            // buttonFieldGoToPrison
            // 
            this.buttonFieldGoToPrison.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFieldGoToPrison.Location = new System.Drawing.Point(699, 699);
            this.buttonFieldGoToPrison.Name = "buttonFieldGoToPrison";
            this.buttonFieldGoToPrison.Size = new System.Drawing.Size(100, 100);
            this.buttonFieldGoToPrison.TabIndex = 39;
            this.buttonFieldGoToPrison.TabStop = false;
            this.buttonFieldGoToPrison.Text = "Idź do\r\n\r\nwięzienia";
            this.buttonFieldGoToPrison.UseVisualStyleBackColor = true;
            // 
            // buttonStreetColorRed1
            // 
            this.buttonStreetColorRed1.BackColor = System.Drawing.Color.Red;
            this.buttonStreetColorRed1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetColorRed1.Location = new System.Drawing.Point(699, 96);
            this.buttonStreetColorRed1.Name = "buttonStreetColorRed1";
            this.buttonStreetColorRed1.Size = new System.Drawing.Size(27, 68);
            this.buttonStreetColorRed1.TabIndex = 57;
            this.buttonStreetColorRed1.TabStop = false;
            this.buttonStreetColorRed1.UseVisualStyleBackColor = false;
            // 
            // buttonStreetRed1
            // 
            this.buttonStreetRed1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetRed1.Location = new System.Drawing.Point(725, 96);
            this.buttonStreetRed1.Name = "buttonStreetRed1";
            this.buttonStreetRed1.Size = new System.Drawing.Size(74, 68);
            this.buttonStreetRed1.TabIndex = 56;
            this.buttonStreetRed1.TabStop = false;
            this.buttonStreetRed1.Text = "Mickiewi-\r\ncza\r\n\r\n$220";
            this.buttonStreetRed1.UseVisualStyleBackColor = true;
            // 
            // buttonFieldChance2
            // 
            this.buttonFieldChance2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFieldChance2.Location = new System.Drawing.Point(699, 163);
            this.buttonFieldChance2.Name = "buttonFieldChance2";
            this.buttonFieldChance2.Size = new System.Drawing.Size(100, 68);
            this.buttonFieldChance2.TabIndex = 54;
            this.buttonFieldChance2.TabStop = false;
            this.buttonFieldChance2.Text = "Szansa";
            this.buttonFieldChance2.UseVisualStyleBackColor = true;
            // 
            // buttonStreetColorRed2
            // 
            this.buttonStreetColorRed2.BackColor = System.Drawing.Color.Red;
            this.buttonStreetColorRed2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetColorRed2.Location = new System.Drawing.Point(699, 230);
            this.buttonStreetColorRed2.Name = "buttonStreetColorRed2";
            this.buttonStreetColorRed2.Size = new System.Drawing.Size(27, 68);
            this.buttonStreetColorRed2.TabIndex = 53;
            this.buttonStreetColorRed2.TabStop = false;
            this.buttonStreetColorRed2.UseVisualStyleBackColor = false;
            // 
            // buttonStreetRed2
            // 
            this.buttonStreetRed2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetRed2.Location = new System.Drawing.Point(725, 230);
            this.buttonStreetRed2.Name = "buttonStreetRed2";
            this.buttonStreetRed2.Size = new System.Drawing.Size(74, 68);
            this.buttonStreetRed2.TabIndex = 52;
            this.buttonStreetRed2.TabStop = false;
            this.buttonStreetRed2.Text = "Słowackie-\r\ngo\r\n\r\n$220";
            this.buttonStreetRed2.UseVisualStyleBackColor = true;
            // 
            // buttonStreetColorRed3
            // 
            this.buttonStreetColorRed3.BackColor = System.Drawing.Color.Red;
            this.buttonStreetColorRed3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetColorRed3.Location = new System.Drawing.Point(699, 297);
            this.buttonStreetColorRed3.Name = "buttonStreetColorRed3";
            this.buttonStreetColorRed3.Size = new System.Drawing.Size(27, 68);
            this.buttonStreetColorRed3.TabIndex = 51;
            this.buttonStreetColorRed3.TabStop = false;
            this.buttonStreetColorRed3.UseVisualStyleBackColor = false;
            // 
            // buttonStreetRed3
            // 
            this.buttonStreetRed3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetRed3.Location = new System.Drawing.Point(725, 297);
            this.buttonStreetRed3.Name = "buttonStreetRed3";
            this.buttonStreetRed3.Size = new System.Drawing.Size(74, 68);
            this.buttonStreetRed3.TabIndex = 50;
            this.buttonStreetRed3.TabStop = false;
            this.buttonStreetRed3.Text = "Plac\r\nWilsona\r\n\r\n$240";
            this.buttonStreetRed3.UseVisualStyleBackColor = true;
            // 
            // buttonStation3
            // 
            this.buttonStation3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStation3.Location = new System.Drawing.Point(699, 364);
            this.buttonStation3.Name = "buttonStation3";
            this.buttonStation3.Size = new System.Drawing.Size(100, 68);
            this.buttonStation3.TabIndex = 48;
            this.buttonStation3.TabStop = false;
            this.buttonStation3.Text = "Dworzec\r\nWschodni\r\n\r\n$200";
            this.buttonStation3.UseVisualStyleBackColor = true;
            // 
            // buttonStreetColorYellow1
            // 
            this.buttonStreetColorYellow1.BackColor = System.Drawing.Color.Yellow;
            this.buttonStreetColorYellow1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetColorYellow1.Location = new System.Drawing.Point(699, 431);
            this.buttonStreetColorYellow1.Name = "buttonStreetColorYellow1";
            this.buttonStreetColorYellow1.Size = new System.Drawing.Size(27, 68);
            this.buttonStreetColorYellow1.TabIndex = 47;
            this.buttonStreetColorYellow1.TabStop = false;
            this.buttonStreetColorYellow1.UseVisualStyleBackColor = false;
            // 
            // buttonStreetYellow1
            // 
            this.buttonStreetYellow1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetYellow1.Location = new System.Drawing.Point(725, 431);
            this.buttonStreetYellow1.Name = "buttonStreetYellow1";
            this.buttonStreetYellow1.Size = new System.Drawing.Size(74, 68);
            this.buttonStreetYellow1.TabIndex = 46;
            this.buttonStreetYellow1.TabStop = false;
            this.buttonStreetYellow1.Text = "Świętokrzy-\r\nska\r\n\r\n$260";
            this.buttonStreetYellow1.UseVisualStyleBackColor = true;
            // 
            // buttonStreetColorYellow2
            // 
            this.buttonStreetColorYellow2.BackColor = System.Drawing.Color.Yellow;
            this.buttonStreetColorYellow2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetColorYellow2.Location = new System.Drawing.Point(699, 498);
            this.buttonStreetColorYellow2.Name = "buttonStreetColorYellow2";
            this.buttonStreetColorYellow2.Size = new System.Drawing.Size(27, 68);
            this.buttonStreetColorYellow2.TabIndex = 45;
            this.buttonStreetColorYellow2.TabStop = false;
            this.buttonStreetColorYellow2.UseVisualStyleBackColor = false;
            // 
            // buttonStreetYellow2
            // 
            this.buttonStreetYellow2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetYellow2.Location = new System.Drawing.Point(725, 498);
            this.buttonStreetYellow2.Name = "buttonStreetYellow2";
            this.buttonStreetYellow2.Size = new System.Drawing.Size(74, 68);
            this.buttonStreetYellow2.TabIndex = 44;
            this.buttonStreetYellow2.TabStop = false;
            this.buttonStreetYellow2.Text = "Krakowskie\r\nPrzedm.\r\n\r\n$260";
            this.buttonStreetYellow2.UseVisualStyleBackColor = true;
            // 
            // buttonWaterSupply
            // 
            this.buttonWaterSupply.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonWaterSupply.Location = new System.Drawing.Point(699, 565);
            this.buttonWaterSupply.Name = "buttonWaterSupply";
            this.buttonWaterSupply.Size = new System.Drawing.Size(100, 68);
            this.buttonWaterSupply.TabIndex = 42;
            this.buttonWaterSupply.TabStop = false;
            this.buttonWaterSupply.Text = "Wodociągi\r\n\r\n$150";
            this.buttonWaterSupply.UseVisualStyleBackColor = true;
            // 
            // buttonStreetColorYellow3
            // 
            this.buttonStreetColorYellow3.BackColor = System.Drawing.Color.Yellow;
            this.buttonStreetColorYellow3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetColorYellow3.Location = new System.Drawing.Point(699, 632);
            this.buttonStreetColorYellow3.Name = "buttonStreetColorYellow3";
            this.buttonStreetColorYellow3.Size = new System.Drawing.Size(27, 68);
            this.buttonStreetColorYellow3.TabIndex = 41;
            this.buttonStreetColorYellow3.TabStop = false;
            this.buttonStreetColorYellow3.UseVisualStyleBackColor = false;
            // 
            // buttonStreetYellow3
            // 
            this.buttonStreetYellow3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetYellow3.Location = new System.Drawing.Point(725, 632);
            this.buttonStreetYellow3.Name = "buttonStreetYellow3";
            this.buttonStreetYellow3.Size = new System.Drawing.Size(74, 68);
            this.buttonStreetYellow3.TabIndex = 40;
            this.buttonStreetYellow3.TabStop = false;
            this.buttonStreetYellow3.Text = "Nowy\r\nŚwiat\r\n\r\n$280";
            this.buttonStreetYellow3.UseVisualStyleBackColor = true;
            // 
            // buttonStreetColorGreen1
            // 
            this.buttonStreetColorGreen1.BackColor = System.Drawing.Color.YellowGreen;
            this.buttonStreetColorGreen1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetColorGreen1.Location = new System.Drawing.Point(633, 699);
            this.buttonStreetColorGreen1.Name = "buttonStreetColorGreen1";
            this.buttonStreetColorGreen1.Size = new System.Drawing.Size(68, 27);
            this.buttonStreetColorGreen1.TabIndex = 75;
            this.buttonStreetColorGreen1.TabStop = false;
            this.buttonStreetColorGreen1.UseVisualStyleBackColor = false;
            // 
            // buttonStreetColorGreen2
            // 
            this.buttonStreetColorGreen2.BackColor = System.Drawing.Color.YellowGreen;
            this.buttonStreetColorGreen2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetColorGreen2.Location = new System.Drawing.Point(566, 699);
            this.buttonStreetColorGreen2.Name = "buttonStreetColorGreen2";
            this.buttonStreetColorGreen2.Size = new System.Drawing.Size(68, 27);
            this.buttonStreetColorGreen2.TabIndex = 73;
            this.buttonStreetColorGreen2.TabStop = false;
            this.buttonStreetColorGreen2.UseVisualStyleBackColor = false;
            // 
            // buttonStreetColorGreen3
            // 
            this.buttonStreetColorGreen3.BackColor = System.Drawing.Color.YellowGreen;
            this.buttonStreetColorGreen3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetColorGreen3.Location = new System.Drawing.Point(432, 699);
            this.buttonStreetColorGreen3.Name = "buttonStreetColorGreen3";
            this.buttonStreetColorGreen3.Size = new System.Drawing.Size(68, 27);
            this.buttonStreetColorGreen3.TabIndex = 69;
            this.buttonStreetColorGreen3.TabStop = false;
            this.buttonStreetColorGreen3.UseVisualStyleBackColor = false;
            // 
            // buttonStreetColorDark1
            // 
            this.buttonStreetColorDark1.BackColor = System.Drawing.Color.MidnightBlue;
            this.buttonStreetColorDark1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetColorDark1.Location = new System.Drawing.Point(231, 699);
            this.buttonStreetColorDark1.Name = "buttonStreetColorDark1";
            this.buttonStreetColorDark1.Size = new System.Drawing.Size(68, 27);
            this.buttonStreetColorDark1.TabIndex = 63;
            this.buttonStreetColorDark1.TabStop = false;
            this.buttonStreetColorDark1.UseVisualStyleBackColor = false;
            // 
            // buttonStreetColorDark2
            // 
            this.buttonStreetColorDark2.BackColor = System.Drawing.Color.MidnightBlue;
            this.buttonStreetColorDark2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetColorDark2.Location = new System.Drawing.Point(97, 699);
            this.buttonStreetColorDark2.Name = "buttonStreetColorDark2";
            this.buttonStreetColorDark2.Size = new System.Drawing.Size(68, 27);
            this.buttonStreetColorDark2.TabIndex = 59;
            this.buttonStreetColorDark2.TabStop = false;
            this.buttonStreetColorDark2.UseVisualStyleBackColor = false;
            // 
            // buttonStreetGreen1
            // 
            this.buttonStreetGreen1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetGreen1.Location = new System.Drawing.Point(633, 725);
            this.buttonStreetGreen1.Name = "buttonStreetGreen1";
            this.buttonStreetGreen1.Size = new System.Drawing.Size(68, 74);
            this.buttonStreetGreen1.TabIndex = 74;
            this.buttonStreetGreen1.TabStop = false;
            this.buttonStreetGreen1.Text = "Pl. Trzech\r\nKrzyży\r\n\r\n$300";
            this.buttonStreetGreen1.UseVisualStyleBackColor = true;
            // 
            // buttonStreetGreen2
            // 
            this.buttonStreetGreen2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetGreen2.Location = new System.Drawing.Point(566, 725);
            this.buttonStreetGreen2.Name = "buttonStreetGreen2";
            this.buttonStreetGreen2.Size = new System.Drawing.Size(68, 74);
            this.buttonStreetGreen2.TabIndex = 72;
            this.buttonStreetGreen2.TabStop = false;
            this.buttonStreetGreen2.Text = "Marszał-\r\nkowska\r\n\r\n$300";
            this.buttonStreetGreen2.UseVisualStyleBackColor = true;
            // 
            // buttonFieldCommunity3
            // 
            this.buttonFieldCommunity3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFieldCommunity3.Location = new System.Drawing.Point(499, 699);
            this.buttonFieldCommunity3.Name = "buttonFieldCommunity3";
            this.buttonFieldCommunity3.Size = new System.Drawing.Size(68, 100);
            this.buttonFieldCommunity3.TabIndex = 70;
            this.buttonFieldCommunity3.TabStop = false;
            this.buttonFieldCommunity3.Text = "Kasa\r\nspołeczna";
            this.buttonFieldCommunity3.UseVisualStyleBackColor = true;
            // 
            // buttonStreetGreen3
            // 
            this.buttonStreetGreen3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetGreen3.Location = new System.Drawing.Point(432, 725);
            this.buttonStreetGreen3.Name = "buttonStreetGreen3";
            this.buttonStreetGreen3.Size = new System.Drawing.Size(68, 74);
            this.buttonStreetGreen3.TabIndex = 68;
            this.buttonStreetGreen3.TabStop = false;
            this.buttonStreetGreen3.Text = "Al. Jero-\r\nzolimskie\r\n\r\n$320";
            this.buttonStreetGreen3.UseVisualStyleBackColor = true;
            // 
            // buttonStation4
            // 
            this.buttonStation4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStation4.Location = new System.Drawing.Point(365, 699);
            this.buttonStation4.Name = "buttonStation4";
            this.buttonStation4.Size = new System.Drawing.Size(68, 100);
            this.buttonStation4.TabIndex = 66;
            this.buttonStation4.TabStop = false;
            this.buttonStation4.Text = "Dworzec Centralny\r\n\r\n$200";
            this.buttonStation4.UseVisualStyleBackColor = true;
            // 
            // buttonFieldChance3
            // 
            this.buttonFieldChance3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFieldChance3.Location = new System.Drawing.Point(298, 699);
            this.buttonFieldChance3.Name = "buttonFieldChance3";
            this.buttonFieldChance3.Size = new System.Drawing.Size(68, 100);
            this.buttonFieldChance3.TabIndex = 64;
            this.buttonFieldChance3.TabStop = false;
            this.buttonFieldChance3.Text = "Szansa";
            this.buttonFieldChance3.UseVisualStyleBackColor = true;
            // 
            // buttonStreetDark1
            // 
            this.buttonStreetDark1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetDark1.Location = new System.Drawing.Point(231, 725);
            this.buttonStreetDark1.Name = "buttonStreetDark1";
            this.buttonStreetDark1.Size = new System.Drawing.Size(68, 74);
            this.buttonStreetDark1.TabIndex = 62;
            this.buttonStreetDark1.TabStop = false;
            this.buttonStreetDark1.Text = "Belweder-\r\nska\r\n\r\n$350";
            this.buttonStreetDark1.UseVisualStyleBackColor = true;
            // 
            // buttonTax2
            // 
            this.buttonTax2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTax2.Location = new System.Drawing.Point(164, 699);
            this.buttonTax2.Name = "buttonTax2";
            this.buttonTax2.Size = new System.Drawing.Size(68, 100);
            this.buttonTax2.TabIndex = 60;
            this.buttonTax2.TabStop = false;
            this.buttonTax2.Text = "Podatek\r\n\r\n-$100";
            this.buttonTax2.UseVisualStyleBackColor = true;
            // 
            // buttonStreetDark2
            // 
            this.buttonStreetDark2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetDark2.Location = new System.Drawing.Point(97, 725);
            this.buttonStreetDark2.Name = "buttonStreetDark2";
            this.buttonStreetDark2.Size = new System.Drawing.Size(68, 74);
            this.buttonStreetDark2.TabIndex = 58;
            this.buttonStreetDark2.TabStop = false;
            this.buttonStreetDark2.Text = "Al. Ujaz-\r\ndowskie\r\n\r\n$400";
            this.buttonStreetDark2.UseVisualStyleBackColor = true;
            // 
            // buttonStreetColorBlue1
            // 
            this.buttonStreetColorBlue1.BackColor = System.Drawing.Color.DodgerBlue;
            this.buttonStreetColorBlue1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStreetColorBlue1.Location = new System.Drawing.Point(72, 297);
            this.buttonStreetColorBlue1.Name = "buttonStreetColorBlue1";
            this.buttonStreetColorBlue1.Size = new System.Drawing.Size(27, 68);
            this.buttonStreetColorBlue1.TabIndex = 13;
            this.buttonStreetColorBlue1.TabStop = false;
            this.buttonStreetColorBlue1.UseVisualStyleBackColor = false;
            // 
            // buttonPrison
            // 
            this.buttonPrison.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPrison.Location = new System.Drawing.Point(-1, -1);
            this.buttonPrison.Name = "buttonPrison";
            this.buttonPrison.Size = new System.Drawing.Size(100, 99);
            this.buttonPrison.TabIndex = 76;
            this.buttonPrison.TabStop = false;
            this.buttonPrison.Text = "Więzienie";
            this.buttonPrison.UseVisualStyleBackColor = true;
            // 
            // labelCopyright
            // 
            this.labelCopyright.AutoSize = true;
            this.labelCopyright.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCopyright.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelCopyright.Location = new System.Drawing.Point(325, 416);
            this.labelCopyright.Name = "labelCopyright";
            this.labelCopyright.Size = new System.Drawing.Size(148, 16);
            this.labelCopyright.TabIndex = 77;
            this.labelCopyright.Text = "© Jakub Gawenda 2019";
            // 
            // labelGameTitle
            // 
            this.labelGameTitle.AutoSize = true;
            this.labelGameTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelGameTitle.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelGameTitle.Location = new System.Drawing.Point(313, 381);
            this.labelGameTitle.Name = "labelGameTitle";
            this.labelGameTitle.Size = new System.Drawing.Size(175, 29);
            this.labelGameTitle.TabIndex = 78;
            this.labelGameTitle.Text = "MONOPOLIST";
            // 
            // BoardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(798, 798);
            this.ControlBox = false;
            this.Controls.Add(this.labelGameTitle);
            this.Controls.Add(this.labelCopyright);
            this.Controls.Add(this.buttonStreetColorGreen1);
            this.Controls.Add(this.buttonStreetColorGreen2);
            this.Controls.Add(this.buttonStreetColorGreen3);
            this.Controls.Add(this.buttonStreetColorDark1);
            this.Controls.Add(this.buttonStreetColorDark2);
            this.Controls.Add(this.buttonStreetGreen1);
            this.Controls.Add(this.buttonStreetGreen2);
            this.Controls.Add(this.buttonFieldCommunity3);
            this.Controls.Add(this.buttonStreetGreen3);
            this.Controls.Add(this.buttonStation4);
            this.Controls.Add(this.buttonFieldChance3);
            this.Controls.Add(this.buttonStreetDark1);
            this.Controls.Add(this.buttonTax2);
            this.Controls.Add(this.buttonStreetDark2);
            this.Controls.Add(this.buttonStreetColorOrange3);
            this.Controls.Add(this.buttonStreetColorOrange2);
            this.Controls.Add(this.buttonStreetColorOrange1);
            this.Controls.Add(this.buttonStreetColorPurple3);
            this.Controls.Add(this.buttonStreetColorPurple2);
            this.Controls.Add(this.buttonStreetColorPurple1);
            this.Controls.Add(this.buttonStreetColorRed1);
            this.Controls.Add(this.buttonStreetRed1);
            this.Controls.Add(this.buttonFieldChance2);
            this.Controls.Add(this.buttonStreetColorRed2);
            this.Controls.Add(this.buttonStreetRed2);
            this.Controls.Add(this.buttonStreetColorRed3);
            this.Controls.Add(this.buttonStreetRed3);
            this.Controls.Add(this.buttonStation3);
            this.Controls.Add(this.buttonStreetColorYellow1);
            this.Controls.Add(this.buttonStreetYellow1);
            this.Controls.Add(this.buttonStreetColorYellow2);
            this.Controls.Add(this.buttonStreetYellow2);
            this.Controls.Add(this.buttonWaterSupply);
            this.Controls.Add(this.buttonStreetColorYellow3);
            this.Controls.Add(this.buttonStreetYellow3);
            this.Controls.Add(this.buttonFieldGoToPrison);
            this.Controls.Add(this.buttonStreetOrange3);
            this.Controls.Add(this.buttonFieldFreeParking);
            this.Controls.Add(this.buttonStreetOrange2);
            this.Controls.Add(this.buttonFieldCommunity2);
            this.Controls.Add(this.buttonStreetOrange1);
            this.Controls.Add(this.buttonStation2);
            this.Controls.Add(this.buttonStreetPurple3);
            this.Controls.Add(this.buttonStreetPurple2);
            this.Controls.Add(this.buttonPowerStation);
            this.Controls.Add(this.buttonStreetPurple1);
            this.Controls.Add(this.buttonStreetColorBlue3);
            this.Controls.Add(this.buttonStreetBlue3);
            this.Controls.Add(this.buttonStreetColorBlue2);
            this.Controls.Add(this.buttonStreetBlue2);
            this.Controls.Add(this.buttonFieldChance1);
            this.Controls.Add(this.buttonStreetColorBlue1);
            this.Controls.Add(this.buttonStreetBlue1);
            this.Controls.Add(this.buttonStation1);
            this.Controls.Add(this.buttonTax1);
            this.Controls.Add(this.buttonStreetColorBrown2);
            this.Controls.Add(this.buttonStreetBrown2);
            this.Controls.Add(this.buttonFieldCommunity1);
            this.Controls.Add(this.buttonFieldStart);
            this.Controls.Add(this.buttonStreetColorBrown1);
            this.Controls.Add(this.buttonStreetBrown1);
            this.Controls.Add(this.buttonPrison);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BoardForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonStreetBrown1;
        private System.Windows.Forms.Button buttonStreetColorBrown1;
        private System.Windows.Forms.Button buttonFieldStart;
        private System.Windows.Forms.Button buttonFieldCommunity1;
        private System.Windows.Forms.Button buttonTax1;
        private System.Windows.Forms.Button buttonStreetColorBrown2;
        private System.Windows.Forms.Button buttonStreetBrown2;
        private System.Windows.Forms.Button buttonStreetColorBlue2;
        private System.Windows.Forms.Button buttonStreetBlue2;
        private System.Windows.Forms.Button buttonFieldChance1;
        private System.Windows.Forms.Button buttonStreetBlue1;
        private System.Windows.Forms.Button buttonStation1;
        private System.Windows.Forms.Button buttonStreetColorBlue3;
        private System.Windows.Forms.Button buttonStreetBlue3;
        private System.Windows.Forms.Button buttonStreetPurple1;
        private System.Windows.Forms.Button buttonPowerStation;
        private System.Windows.Forms.Button buttonStreetPurple3;
        private System.Windows.Forms.Button buttonStreetColorPurple2;
        private System.Windows.Forms.Button buttonStreetPurple2;
        private System.Windows.Forms.Button buttonStreetOrange2;
        private System.Windows.Forms.Button buttonFieldCommunity2;
        private System.Windows.Forms.Button buttonStreetColorOrange1;
        private System.Windows.Forms.Button buttonStreetOrange1;
        private System.Windows.Forms.Button buttonStation2;
        private System.Windows.Forms.Button buttonFieldFreeParking;
        private System.Windows.Forms.Button buttonStreetColorOrange3;
        private System.Windows.Forms.Button buttonStreetOrange3;
        private System.Windows.Forms.Button buttonFieldGoToPrison;
        private System.Windows.Forms.Button buttonStreetColorRed1;
        private System.Windows.Forms.Button buttonStreetRed1;
        private System.Windows.Forms.Button buttonFieldChance2;
        private System.Windows.Forms.Button buttonStreetColorRed2;
        private System.Windows.Forms.Button buttonStreetRed2;
        private System.Windows.Forms.Button buttonStreetColorRed3;
        private System.Windows.Forms.Button buttonStreetRed3;
        private System.Windows.Forms.Button buttonStation3;
        private System.Windows.Forms.Button buttonStreetColorYellow1;
        private System.Windows.Forms.Button buttonStreetYellow1;
        private System.Windows.Forms.Button buttonStreetColorYellow2;
        private System.Windows.Forms.Button buttonStreetYellow2;
        private System.Windows.Forms.Button buttonWaterSupply;
        private System.Windows.Forms.Button buttonStreetColorYellow3;
        private System.Windows.Forms.Button buttonStreetYellow3;
        private System.Windows.Forms.Button buttonStreetColorGreen1;
        private System.Windows.Forms.Button buttonStreetColorGreen2;
        private System.Windows.Forms.Button buttonStreetColorGreen3;
        private System.Windows.Forms.Button buttonStreetColorDark1;
        private System.Windows.Forms.Button buttonStreetColorDark2;
        private System.Windows.Forms.Button buttonStreetGreen1;
        private System.Windows.Forms.Button buttonStreetGreen2;
        private System.Windows.Forms.Button buttonFieldCommunity3;
        private System.Windows.Forms.Button buttonStreetGreen3;
        private System.Windows.Forms.Button buttonStation4;
        private System.Windows.Forms.Button buttonFieldChance3;
        private System.Windows.Forms.Button buttonStreetDark1;
        private System.Windows.Forms.Button buttonTax2;
        private System.Windows.Forms.Button buttonStreetDark2;
        private System.Windows.Forms.Button buttonStreetColorBlue1;
        private System.Windows.Forms.Button buttonPrison;
        private System.Windows.Forms.Button buttonStreetColorPurple1;
        private System.Windows.Forms.Button buttonStreetColorPurple3;
        private System.Windows.Forms.Button buttonStreetColorOrange2;
        private System.Windows.Forms.Label labelCopyright;
        private System.Windows.Forms.Label labelGameTitle;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Forms.FieldDetail;
using Monopolist.Board.Types.Fields;
using Monopolist.Board.Types.Fields.Containers;
using Monopolist.Board.Forms.ExtendedFieldDetail;

namespace Monopolist.Board.Actions.Display.FieldDetail
{
    public class UtilityDetailsDisplayer : DetailsDisplayer
    {
        private UtilityDetailsForm _utilityDetailsForm;
        private ExtFieldDetailsForm _extendedDetailsForm;

        public UtilityDetailsDisplayer(UtilityDetailsForm utilityDetailsForm, ExtFieldDetailsForm extFieldDetailsForm, AllFieldsContainer allFieldsContainer, List<Button> utilityButtons) : base(utilityDetailsForm, extFieldDetailsForm, utilityButtons, allFieldsContainer.UtilityFields)
        {
            _utilityDetailsForm = utilityDetailsForm;
            _extendedDetailsForm = extFieldDetailsForm;
        }

        protected override void Button_MouseHover(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            var found = _relationDictionary.Where(p => p.Key == button).FirstOrDefault();
            FieldUtility fieldUtility = (FieldUtility)found.Value;

            _utilityDetailsForm.UpdateInfo(fieldUtility.CardInfo);
            _utilityDetailsForm.Show();

            _extendedDetailsForm.UpdateInfo(fieldUtility);
            _extendedDetailsForm.Show();
        }

    }
}

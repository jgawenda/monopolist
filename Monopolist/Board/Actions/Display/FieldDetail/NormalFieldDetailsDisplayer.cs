﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Forms.FieldDetail;
using Monopolist.Board.Types.Fields.Containers;
using Monopolist.Board.Types.Fields;
using Monopolist.Board.Forms.ExtendedFieldDetail;

namespace Monopolist.Board.Actions.Display.FieldDetail
{
    public class NormalFieldDetailsDisplayer : DetailsDisplayer
    {
        private FieldDetailForm _fieldDetailForm;
        private ExtFieldDetailsForm _extendedDetailsForm;

        public NormalFieldDetailsDisplayer(FieldDetailForm fieldDetailForm, ExtFieldDetailsForm extFieldDetailsForm, AllFieldsContainer allFieldsContainer, List<Button> normalFieldButtons) : base(fieldDetailForm, extFieldDetailsForm, normalFieldButtons, allFieldsContainer.NormalFields)
        {
            _fieldDetailForm = fieldDetailForm;
            _extendedDetailsForm = extFieldDetailsForm;
        }
        
        protected override void Button_MouseHover(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            var found = _relationDictionary.Where(p => p.Key == button).FirstOrDefault();
            NormalField normalField = (NormalField)found.Value;

            _fieldDetailForm.UpdateFieldInfo(normalField.CardInfo);
            _fieldDetailForm.Show();

            _extendedDetailsForm.UpdateInfo(normalField);
            _extendedDetailsForm.Show();
        }
        
    }
}

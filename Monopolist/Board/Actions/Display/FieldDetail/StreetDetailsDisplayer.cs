﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Types.Fields;
using Monopolist.Board.Types.Fields.Containers;
using Monopolist.Board.Forms.FieldDetail;
using Monopolist.Board.Forms.ExtendedFieldDetail;

namespace Monopolist.Board.Actions.Display.FieldDetail
{
    /// <summary>
    /// Class used to manage Buttons (streets) events.
    /// </summary>
    public class StreetDetailsDisplayer : DetailsDisplayer
    {
        // Form used to display Street info
        private StreetDetailsForm _streetDetailsForm;

        private ExtFieldDetailsForm _extendedDetailsForm;

        public StreetDetailsDisplayer(StreetDetailsForm streetDetailsForm, ExtFieldDetailsForm extFieldDetailsForm, AllFieldsContainer allFieldsContainer, List<Button> streetButtons) : base(streetDetailsForm, extFieldDetailsForm, streetButtons, allFieldsContainer.AllStreets)
        {
            _streetDetailsForm = streetDetailsForm;
            _extendedDetailsForm = extFieldDetailsForm;
        }
        
        protected override void Button_MouseHover(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            var found = _relationDictionary.Where(p => p.Key == button).FirstOrDefault();
            FieldStreet fieldStreet = (FieldStreet)found.Value;

            _streetDetailsForm.UpdateStreetInfo(fieldStreet.StreetCardInfo);
            _streetDetailsForm.Show();

            _extendedDetailsForm.UpdateInfo(fieldStreet);
            _extendedDetailsForm.Show();
        }

    }
}

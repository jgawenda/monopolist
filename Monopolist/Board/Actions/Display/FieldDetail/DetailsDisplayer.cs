﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Actions.Display.Helpers;
using Monopolist.Board.Types.Fields;

namespace Monopolist.Board.Actions.Display.FieldDetail
{
    public abstract class DetailsDisplayer
    {
        private Form _detailsForm;
        private Form _extendedDetailsForm;

        protected Dictionary<object, object> _relationDictionary;

        private DetailsDisplayer(Form detailsForm, Form extendedDetailsForm, List<Button> buttons)
        {
            _detailsForm = detailsForm;
            _extendedDetailsForm = extendedDetailsForm;
            PrepareActions(buttons);
        }

        public DetailsDisplayer(Form detailsForm, Form extendedDetailsForm, List<Button> chanceButtons, List<FieldChance> chanceFields) : this(detailsForm, extendedDetailsForm, chanceButtons)
        {
            PrepareRelation(chanceButtons, chanceFields);
        }

        public DetailsDisplayer(Form detailsForm, Form extendedDetailsForm, List<Button> normalFieldButtons, List<NormalField> normalFields) : this(detailsForm, extendedDetailsForm, normalFieldButtons)
        {
            PrepareRelation(normalFieldButtons, normalFields);
        }

        public DetailsDisplayer(Form detailsForm, Form extendedDetailsForm, List<Button> stationButtons, List<FieldStation> stationFields) : this(detailsForm, extendedDetailsForm, stationButtons)
        {
            PrepareRelation(stationButtons, stationFields);
        }

        public DetailsDisplayer(Form detailsForm, Form extendedDetailsForm, List<Button> streetButtons, List<FieldStreet> streetFields) : this(detailsForm, extendedDetailsForm, streetButtons)
        {
            PrepareRelation(streetButtons, streetFields);
        }

        public DetailsDisplayer(Form detailsForm, Form extendedDetailsForm, List<Button> taxButtons, List<FieldTax> taxFields) : this(detailsForm, extendedDetailsForm, taxButtons)
        {
            PrepareRelation(taxButtons, taxFields);
        }

        public DetailsDisplayer(Form detailsForm, Form extendedDetailsForm, List<Button> utilityButtons, List<FieldUtility> utilityFields) : this(detailsForm, extendedDetailsForm, utilityButtons)
        {
            PrepareRelation(utilityButtons, utilityFields);
        }

        protected abstract void Button_MouseHover(object sender, EventArgs e);
        
        protected virtual void Button_MouseLeave(object sender, EventArgs e)
        {
            _detailsForm.Hide();
            _extendedDetailsForm.Hide();
        }

        private void PrepareActions(List<Button> buttons)
        {
            foreach (var button in buttons)
            {
                button.MouseHover += new EventHandler(this.Button_MouseHover);
                button.MouseLeave += new EventHandler(this.Button_MouseLeave);
            }
        }
        
        private void PrepareRelation<S, T>(List<S> buttons, List<T> fields)
        {
            ButtonFieldRelationMaker relationMaker = new ButtonFieldRelationMaker();
            _relationDictionary = relationMaker.GetRelationDictionary(buttons, fields);
        }

    }
}

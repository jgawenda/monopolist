﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Forms.FieldDetail;
using Monopolist.Board.Types.Fields;
using Monopolist.Board.Types.Fields.Containers;
using Monopolist.Board.Forms.ExtendedFieldDetail;

namespace Monopolist.Board.Actions.Display.FieldDetail
{
    public class StationDetailsDisplayer : DetailsDisplayer
    {
        private StationDetailsForm _stationDetailsForm;
        private ExtFieldDetailsForm _extendedDetailsForm;

        public StationDetailsDisplayer(StationDetailsForm stationDetailsForm, ExtFieldDetailsForm extFieldDetailsForm, AllFieldsContainer allFieldsContainer, List<Button> stationButtons) : base(stationDetailsForm, extFieldDetailsForm, stationButtons, allFieldsContainer.StationFields)
        {
            _stationDetailsForm = stationDetailsForm;
            _extendedDetailsForm = extFieldDetailsForm;
        }

        protected override void Button_MouseHover(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            var found = _relationDictionary.Where(p => p.Key == button).FirstOrDefault();
            FieldStation fieldStation = (FieldStation)found.Value;

            _stationDetailsForm.UpdateInfo(fieldStation.CardInfo);
            _stationDetailsForm.Show();

            _extendedDetailsForm.UpdateInfo(fieldStation);
            _extendedDetailsForm.Show();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Forms.FieldDetail;
using Monopolist.Board.Types.Fields;
using Monopolist.Board.Types.Fields.Containers;
using Monopolist.Board.Forms.ExtendedFieldDetail;

namespace Monopolist.Board.Actions.Display.FieldDetail
{
    public class ChanceDetailsDisplayer : DetailsDisplayer
    {
        private ChanceDetailsForm _chanceDetailsForm;
        private ExtFieldDetailsForm _extendedDetailsForm;
        
        public ChanceDetailsDisplayer(ChanceDetailsForm chanceDetailsForm, ExtFieldDetailsForm extFieldDetailsForm, AllFieldsContainer allFieldsContainer, List<Button> chanceButtons) : base(chanceDetailsForm, extFieldDetailsForm, chanceButtons, allFieldsContainer.CardFields)
        {
            _chanceDetailsForm = chanceDetailsForm;
            _extendedDetailsForm = extFieldDetailsForm;
        }
        
        protected override void Button_MouseHover(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            var found = _relationDictionary.Where(p => p.Key == button).FirstOrDefault();
            FieldChance fieldChance = (FieldChance)found.Value;

            _chanceDetailsForm.UpdateInfo(fieldChance.CardInfo);
            _chanceDetailsForm.Show();

            _extendedDetailsForm.UpdateInfo(fieldChance);
            _extendedDetailsForm.Show();
        }
        
    }
}

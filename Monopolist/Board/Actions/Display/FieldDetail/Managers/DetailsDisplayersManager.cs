﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Forms.FieldDetail;
using Monopolist.Board.Forms.ExtendedFieldDetail;
using Monopolist.Board.Types.Fields.Containers;

namespace Monopolist.Board.Actions.Display.FieldDetail.Managers
{
    public class DetailsDisplayersManager
    {
        private StreetDetailsDisplayer _streetDetailsDisplayer;
        private NormalFieldDetailsDisplayer _normalFieldDetailsDisplayer;
        private StationDetailsDisplayer _stationDetailsDisplayer;
        private ChanceDetailsDisplayer _chanceDetailsDisplayer;
        private TaxDetailsDisplayer _taxDetailsDisplayer;
        private UtilityDetailsDisplayer _utilityDetailsDisplayer;
        
        // This constructor initiates all Displayers. You don't have to be concerned about initiating them by yourself in BoardForm.
        public DetailsDisplayersManager(Form parentForm, AllFieldsContainer allFieldsContainer, List<Button> streetButtons, List<Button> normalFieldButtons, List<Button> stationButtons, List<Button> chanceButtons, List<Button> taxButtons, List<Button> utilityButtons)
        {
            ExtFieldDetailsForm extendedDetailsForm = new ExtFieldDetailsForm(parentForm);

            _streetDetailsDisplayer = new StreetDetailsDisplayer(new StreetDetailsForm(parentForm), extendedDetailsForm, allFieldsContainer, streetButtons);
            _normalFieldDetailsDisplayer = new NormalFieldDetailsDisplayer(new FieldDetailForm(parentForm), extendedDetailsForm, allFieldsContainer, normalFieldButtons);
            _stationDetailsDisplayer = new StationDetailsDisplayer(new StationDetailsForm(parentForm), extendedDetailsForm, allFieldsContainer, stationButtons);
            _chanceDetailsDisplayer = new ChanceDetailsDisplayer(new ChanceDetailsForm(parentForm), extendedDetailsForm, allFieldsContainer, chanceButtons);
            _taxDetailsDisplayer = new TaxDetailsDisplayer(new TaxDetailsForm(parentForm), extendedDetailsForm, allFieldsContainer, taxButtons);
            _utilityDetailsDisplayer = new UtilityDetailsDisplayer(new UtilityDetailsForm(parentForm), extendedDetailsForm, allFieldsContainer, utilityButtons);
        }

    }
}

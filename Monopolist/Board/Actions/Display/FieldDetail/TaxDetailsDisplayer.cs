﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Forms.FieldDetail;
using Monopolist.Board.Types.Fields;
using Monopolist.Board.Types.Fields.Containers;
using Monopolist.Board.Forms.ExtendedFieldDetail;

namespace Monopolist.Board.Actions.Display.FieldDetail
{
    public class TaxDetailsDisplayer : DetailsDisplayer
    {
        private TaxDetailsForm _taxDetailsForm;
        private ExtFieldDetailsForm _extendedDetailsForm;

        public TaxDetailsDisplayer(TaxDetailsForm taxDetailsForm, ExtFieldDetailsForm extFieldDetailsForm, AllFieldsContainer allFieldsContainer, List<Button> taxButtons) : base(taxDetailsForm, extFieldDetailsForm, taxButtons, allFieldsContainer.TaxFields)
        {
            _taxDetailsForm = taxDetailsForm;
            _extendedDetailsForm = extFieldDetailsForm;
        }

        protected override void Button_MouseHover(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            var found = _relationDictionary.Where(p => p.Key == button).FirstOrDefault();
            FieldTax fieldTax = (FieldTax)found.Value;

            _taxDetailsForm.UpdateInfo(fieldTax.CardInfo);
            _taxDetailsForm.Show();

            _extendedDetailsForm.UpdateInfo(fieldTax);
            _extendedDetailsForm.Show();
        }

    }
}

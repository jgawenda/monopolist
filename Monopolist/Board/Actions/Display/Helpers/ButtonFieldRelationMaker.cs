﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Types.Fields;
using Monopolist.Board.Types.Fields.Containers;

namespace Monopolist.Board.Actions.Display.Helpers
{
    public class ButtonFieldRelationMaker
    {
        private Dictionary<object, object> GenerateDictionary(List<object> buttons, List<object> fields)
        {
            Dictionary<object, object> convertedDictionary = new Dictionary<object, object>();

            for (int i=0; i<buttons.Count; i++)
                convertedDictionary.Add(buttons.ElementAt(i), fields.ElementAt(i));

            return convertedDictionary;
        }
        
        public Dictionary<object, object> GetRelationDictionary<S, T>(List<S> listButtons, List<T> listFields)
        {
            List<object> tempListButtons = new List<object>();
            List<object> tempListFields = new List<object>();

            foreach (var element in listButtons)
                tempListButtons.Add(element);

            foreach (var element in listFields)
                tempListFields.Add(element);

            return GenerateDictionary(tempListButtons, tempListFields);
        }
        
    }
}

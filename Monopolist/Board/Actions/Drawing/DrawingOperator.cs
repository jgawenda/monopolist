﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using Monopolist.Board.Helpers.Drawing;
using Monopolist.Board.Types.Drawing;

namespace Monopolist.Board.Actions.Drawing
{
    public class DrawingOperator
    {
        private Pen _pen;

        private LineCoordinatesContainer _coordinatesContainer;

        public DrawingOperator()
        {
            _pen = new Pen(Color.FromArgb(255, 0, 0, 0));

            LoadCoordinates();
        }

        private void LoadCoordinates()
        {
            DrawCoordinatesGenerator coordinatesGenerator = new DrawCoordinatesGenerator();

            _coordinatesContainer = coordinatesGenerator.GenerateLinesCoordinates();
        }

        private void DrawRectangle(PaintEventArgs eventArgs)
        {
            eventArgs.Graphics.DrawRectangle(_pen, new Rectangle(100, 100, 600, 600));
        }

        private void DrawLinesTop(PaintEventArgs eventArgs)
        {
            foreach (var coordinates in _coordinatesContainer.TopLinesCoordinates)
            {
                eventArgs.Graphics.DrawLine(_pen, coordinates.X1, coordinates.Y1, coordinates.X2, coordinates.Y2);
            }
        }

        private void DrawLinesBottom(PaintEventArgs eventArgs)
        {
            foreach (var coordinates in _coordinatesContainer.BottomLinesCoordinates)
            {
                eventArgs.Graphics.DrawLine(_pen, coordinates.X1, coordinates.Y1, coordinates.X2, coordinates.Y2);
            }
        }

        private void DrawLinesLeft(PaintEventArgs eventArgs)
        {
            foreach (var coordinates in _coordinatesContainer.LeftLinesCoordinates)
            {
                eventArgs.Graphics.DrawLine(_pen, coordinates.X1, coordinates.Y1, coordinates.X2, coordinates.Y2);
            }
        }

        private void DrawLinesRight(PaintEventArgs eventArgs)
        {
            foreach (var coordinates in _coordinatesContainer.RightLinesCoordinates)
            {
                eventArgs.Graphics.DrawLine(_pen, coordinates.X1, coordinates.Y1, coordinates.X2, coordinates.Y2);
            }
        }

        public void DrawBoardElements(PaintEventArgs eventArgs)
        {
            eventArgs.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            DrawRectangle(eventArgs);
            DrawLinesTop(eventArgs);
            DrawLinesBottom(eventArgs);
            DrawLinesLeft(eventArgs);
            DrawLinesRight(eventArgs);
        }

    }
}

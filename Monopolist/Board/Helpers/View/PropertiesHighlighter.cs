﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Monopolist.Board.Types.Fields;
using Monopolist.Board.Types.Fields.Containers;
using Monopolist.Core.Types.Players;
using System.Windows.Forms;

namespace Monopolist.Board.Helpers.View
{
    public class PropertiesHighlighter
    {
        private List<BuyableField> _allBuyableFields;
        private List<Color> _buyableFieldsColors;

        public PropertiesHighlighter(AllFieldsContainer allFieldsContainer)
        {
            _allBuyableFields = allFieldsContainer.AllBuyableFields;
            SavePreviousFieldsColors();
        }
        
        private void SavePreviousFieldsColors()
        {
            _buyableFieldsColors = new List<Color>();

            foreach (BuyableField field in _allBuyableFields)
            {
                _buyableFieldsColors.Add(field.AssignedButton.BackColor);
            }
        }

        public void UndoHighlight()
        {
            for (int i = 0; i < _allBuyableFields.Count; i++)
                _allBuyableFields.ElementAt(i).AssignedButton.BackColor = _buyableFieldsColors.ElementAt(i);
        }

        public void HighlightProperties(PlayerAccount playerAccount)
        {
            SavePreviousFieldsColors();

            foreach (var field in _allBuyableFields)
            {
                if (field.Owner == playerAccount)
                    field.AssignedButton.BackColor = Color.FromArgb(84, 255, 135);
                else
                    field.AssignedButton.BackColor = Color.FromArgb(255, 129, 84);
                
                field.AssignedButton.Refresh();
            }
            
        }

    }
}

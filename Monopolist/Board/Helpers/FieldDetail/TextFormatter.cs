﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopolist.Board.Helpers.FieldDetail
{
    public static class TextFormatter
    {
        public static string GetPrice(int price)
        {
            return String.Format("${0}", price);
        }

    }
}

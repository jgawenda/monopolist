﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monopolist.Board.Types.Drawing;

namespace Monopolist.Board.Helpers.Drawing
{
    public class DrawCoordinatesGenerator
    {
        private List<int> _basicCoordinates = new List<int>() { 100, 167, 233, 300, 366, 433, 500, 567, 633, 700 };

        /* VER 1
        private List<LineCoordinates> _topLines;
        private List<LineCoordinates> _bottomLines;
        private List<LineCoordinates> _leftLines;
        private List<LineCoordinates> _rightLines;

        private void ZeroLists()
        {
            _topLines = new List<LineCoordinates>();
            _bottomLines = new List<LineCoordinates>();
            _leftLines = new List<LineCoordinates>();
            _rightLines = new List<LineCoordinates>();
        }

        private void FillCoordinateLists()
        {
            ZeroLists();

            foreach (int coordinate in _basicCoordinates)
            {
                _topLines.Add(new LineCoordinates(coordinate, 0, coordinate, 100));
                _bottomLines.Add(new LineCoordinates(coordinate, 700, coordinate, 800));
                _leftLines.Add(new LineCoordinates(0, coordinate, 100, coordinate));
                _rightLines.Add(new LineCoordinates(700, coordinate, 800, coordinate));
            }
        }
        
        public LineCoordinatesContainer GenerateLinesCoordinates()
        {
            FillCoordinateLists();

            return new LineCoordinatesContainer(_topLines, _bottomLines, _leftLines, _rightLines);
        }
        */

        // VER 2
        private void FillCoordinateLists(List<LineCoordinates> topLines, List<LineCoordinates> bottomLines, List<LineCoordinates> leftLines, List<LineCoordinates> rightLines)
        {
            foreach (var coordinate in _basicCoordinates)
            {
                topLines.Add(new LineCoordinates(coordinate, 0, coordinate, 100));
                bottomLines.Add(new LineCoordinates(coordinate, 700, coordinate, 800));
                leftLines.Add(new LineCoordinates(0, coordinate, 100, coordinate));
                rightLines.Add(new LineCoordinates(700, coordinate, 800, coordinate));
            }
        }

        public LineCoordinatesContainer GenerateLinesCoordinates()
        {
            List<LineCoordinates> topLines = new List<LineCoordinates>();
            List<LineCoordinates> bottomLines = new List<LineCoordinates>();
            List<LineCoordinates> leftLines = new List<LineCoordinates>();
            List<LineCoordinates> rightLines = new List<LineCoordinates>();

            FillCoordinateLists(topLines, bottomLines, leftLines, rightLines);

            return new LineCoordinatesContainer(topLines, bottomLines, leftLines, rightLines);
        }

    }
}

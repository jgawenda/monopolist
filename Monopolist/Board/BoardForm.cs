﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Types.Fields.Containers;
using Monopolist.Board.Actions.Display.FieldDetail.Managers;

namespace Monopolist.Board
{
    public partial class BoardForm : Form
    {
        private AllFieldsContainer _allFields;
        private DetailsDisplayersManager _displayersManager;

        // Lists used to pass them to certain Classes
        public List<Button> AllFieldsButtons { get; private set; }
        private List<Button> _streetsButtons;
        private List<Button> _normalFieldsButtons;
        private List<Button> _stationsButtons;
        private List<Button> _chancesButtons;
        private List<Button> _taxesButtons;
        private List<Button> _utilitiesButtons;

        public BoardForm()
        {
            InitializeComponent();
            PrepareButtonLists();
        }
        
        private void PrepareButtonLists()
        {
            AllFieldsButtons = new List<Button>()
            {
                buttonFieldStart,
                buttonStreetBrown1,
                buttonFieldCommunity1,
                buttonStreetBrown2,
                buttonTax1,
                buttonStation1,
                buttonStreetBlue1,
                buttonFieldChance1,
                buttonStreetBlue2,
                buttonStreetBlue3,
                buttonPrison,
                buttonStreetPurple1,
                buttonPowerStation,
                buttonStreetPurple2,
                buttonStreetPurple3,
                buttonStation2,
                buttonStreetOrange1,
                buttonFieldCommunity2,
                buttonStreetOrange2,
                buttonStreetOrange3,
                buttonFieldFreeParking,
                buttonStreetRed1,
                buttonFieldChance2,
                buttonStreetRed2,
                buttonStreetRed3,
                buttonStation3,
                buttonStreetYellow1,
                buttonStreetYellow2,
                buttonWaterSupply,
                buttonStreetYellow3,
                buttonFieldGoToPrison,
                buttonStreetGreen1,
                buttonStreetGreen2,
                buttonFieldCommunity3,
                buttonStreetGreen3,
                buttonStation4,
                buttonFieldChance3,
                buttonStreetDark1,
                buttonTax2,
                buttonStreetDark2
            };

            _streetsButtons = new List<Button>()
            {
                buttonStreetBrown1,
                buttonStreetBrown2,
                buttonStreetBlue1,
                buttonStreetBlue2,
                buttonStreetBlue3,
                buttonStreetPurple1,
                buttonStreetPurple2,
                buttonStreetPurple3,
                buttonStreetOrange1,
                buttonStreetOrange2,
                buttonStreetOrange3,
                buttonStreetRed1,
                buttonStreetRed2,
                buttonStreetRed3,
                buttonStreetYellow1,
                buttonStreetYellow2,
                buttonStreetYellow3,
                buttonStreetGreen1,
                buttonStreetGreen2,
                buttonStreetGreen3,
                buttonStreetDark1,
                buttonStreetDark2
            };

            _normalFieldsButtons = new List<Button>()
            {
                buttonFieldStart,
                buttonPrison,
                buttonFieldFreeParking,
                buttonFieldGoToPrison
            };

            _stationsButtons = new List<Button>()
            {
                buttonStation1,
                buttonStation2,
                buttonStation3,
                buttonStation4
            };

            _chancesButtons = new List<Button>()
            {
                buttonFieldCommunity1,
                buttonFieldChance1,
                buttonFieldCommunity2,
                buttonFieldChance2,
                buttonFieldCommunity3,
                buttonFieldChance3
            };

            _taxesButtons = new List<Button>()
            {
                buttonTax1,
                buttonTax2
            };

            _utilitiesButtons = new List<Button>()
            {
                buttonPowerStation,
                buttonWaterSupply
            };

        }
        
        public void SaveAllFields(AllFieldsContainer allFieldsContainer)
        {
            _allFields = allFieldsContainer;

            // By initiating the Manager, all necessary Displayers are initiated also.
            _displayersManager = new DetailsDisplayersManager(this, _allFields, _streetsButtons, _normalFieldsButtons, _stationsButtons, _chancesButtons, _taxesButtons, _utilitiesButtons);
        }

    }
}

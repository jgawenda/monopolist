﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;
using Monopolist.Board.Forms.PlayerTurn.Trading.Helpers;
using Monopolist.Board.Forms.PlayerTurn.Trading.Helpers.Types;
using Monopolist.Board.Forms.PlayerTurn.Trading.Helpers.Tools;
using Monopolist.Board.Forms.PlayerTurn.Trading.Trade;
using Monopolist.Board.Forms.PlayerTurn.Trading.Trade.Helpers;
using Monopolist.Board.Types.Fields.Containers;
using Monopolist.Core.Actions.GameProcess.Helpers;

namespace Monopolist.Board.Forms.PlayerTurn.Trading
{
    public partial class TradingForm : Form
    {
        private PlayerAccount _playerAccount;
        private PlayerFormsUpdater _playerFormsUpdater;

        private LoadingDataOperator _loadingDataOperator;

        private TradingOperator _tradingOperator;

        public TradingForm(PlayerAccount playerAccount, List<PlayerAccount> allPlayers, AllFieldsContainer allFieldsContainer, PlayerFormsUpdater playerFormsUpdater)
        {
            InitializeComponent();

            _playerAccount = playerAccount;

            _playerFormsUpdater = playerFormsUpdater;

            TradingFormControls tradingFormControls = new TradingFormControls(labelPlayerSymbol, labelPlayerName, comboBoxOtherPlayers, listBoxLeft, listBoxRight, numericUpDownLeft, numericUpDownRight);

            _loadingDataOperator = new LoadingDataOperator(tradingFormControls, _playerAccount, allPlayers, allFieldsContainer);

            _tradingOperator = new TradingOperator(_playerAccount);
        }

        private void ComboBoxOtherPlayers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxOtherPlayers.SelectedItem is PlayerAccount tradingPartner)
            {
                _loadingDataOperator.LoadOtherPlayerData(tradingPartner);
                buttonMakeDeal.Enabled = true;
            }
            else
                buttonMakeDeal.Enabled = false;
            
        }

        private void ButtonMakeDeal_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Do you want to accept this deal?", "Accept trade?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            
            if (result == DialogResult.Yes)
            {
                PlayerAccount tradingPartner = (PlayerAccount)comboBoxOtherPlayers.SelectedItem;
                _tradingOperator.MakeDeal(tradingPartner, ListBoxExtractor.GetBuyables(listBoxLeft.SelectedItems), ListBoxExtractor.GetBuyables(listBoxRight.SelectedItems), numericUpDownLeft.Value, numericUpDownRight.Value);
                _loadingDataOperator.RefreshBothPlayersData(tradingPartner);
                _playerFormsUpdater.UpdateInfo();
            }

        }

        private void NumericUpDownLeft_KeyUp(object sender, KeyEventArgs e)
        {
            NumericValueProtector.CorrectValue((NumericUpDown)sender);
        }

        private void NumericUpDownRight_KeyUp(object sender, KeyEventArgs e)
        {
            NumericValueProtector.CorrectValue((NumericUpDown)sender);
        }

        private void LinkLabelClose_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }
    }
}

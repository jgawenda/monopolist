﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monopolist.Core.Types.Players;
using Monopolist.Board.Types.Fields;

namespace Monopolist.Board.Forms.PlayerTurn.Trading.Trade
{
    public class TradingOperator
    {
        private PlayerAccount _activePlayer;

        public TradingOperator(PlayerAccount activePlayer)
        {
            _activePlayer = activePlayer;
        }

        public void MakeDeal(PlayerAccount tradingPartner, List<BuyableField> buyablesLeft, List<BuyableField> buyablesRight, decimal cashLeft, decimal cashRight)
        {
            ChangeOwner(buyablesRight, _activePlayer);
            ChangeOwner(buyablesLeft, tradingPartner);

            TradeCash(tradingPartner, cashLeft, cashRight);
        }

        private void ChangeOwner(List<BuyableField> buyablesForNewOwner, PlayerAccount newOwner)
        {
            foreach (BuyableField field in buyablesForNewOwner)
                field.ChangeOwner(newOwner);
        }

        private void TradeCash(PlayerAccount tradingPartner, decimal cashLeft, decimal cashRight)
        {
            int cashLeftConverted = Convert.ToInt32(cashLeft);
            int cashRightConverted = Convert.ToInt32(cashRight);

            _activePlayer.CashAccount.RemoveCash(cashLeftConverted);
            _activePlayer.CashAccount.AddCash(cashRightConverted);

            tradingPartner.CashAccount.RemoveCash(cashRightConverted);
            tradingPartner.CashAccount.AddCash(cashLeftConverted);
        }

    }
}

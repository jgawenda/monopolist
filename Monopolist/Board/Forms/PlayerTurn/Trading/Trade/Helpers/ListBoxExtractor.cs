﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Types.Fields;

namespace Monopolist.Board.Forms.PlayerTurn.Trading.Trade.Helpers
{
    public static class ListBoxExtractor
    {
        /// <summary>
        /// Converts collection to List of BuyableFields.
        /// </summary>
        /// <param name="selectedObjects"></param>
        /// <returns>Returned list is empty if there were no BuyableFields in collection provided.</returns>
        public static List<BuyableField> GetBuyables(ListBox.SelectedObjectCollection selectedObjects)
        {
            List<BuyableField> buyableFields = new List<BuyableField>();

            foreach (var element in selectedObjects)
            {
                if (element is BuyableField buyable)
                    buyableFields.Add(buyable);
            }

            return buyableFields;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;
using Monopolist.Board.Types.Fields.Containers;
using Monopolist.Board.Forms.PlayerTurn.Trading.Helpers.LoadingData;
using Monopolist.Board.Forms.PlayerTurn.Trading.Helpers.LoadingData.Helpers;
using Monopolist.Board.Forms.PlayerTurn.Trading.Helpers.Types;

namespace Monopolist.Board.Forms.PlayerTurn.Trading.Helpers
{
    public class LoadingDataOperator
    {
        private PlayerAccount _activePlayer;

        private PlayerDataLoader _activePlayerDataLoader;
        private PlayerDataLoader _otherPlayerDataLoader;

        public LoadingDataOperator(TradingFormControls tradingFormControls, PlayerAccount activePlayer, List<PlayerAccount> allPlayers, AllFieldsContainer allFieldsContainer)
        {
            _activePlayer = activePlayer;
            LoadConstantElements(tradingFormControls, _activePlayer, allPlayers);
            _activePlayerDataLoader = new PlayerDataLoader(tradingFormControls.ListBoxLeft, tradingFormControls.NumericUpDownLeft, allFieldsContainer);
            _otherPlayerDataLoader = new PlayerDataLoader(tradingFormControls.ListBoxRight, tradingFormControls.NumericUpDownRight, allFieldsContainer);

            _activePlayerDataLoader.LoadAllData(_activePlayer);
        }

        // Load Data to ComboBox, LabelPlayerSymbol, LabelPlayerName
        private void LoadConstantElements(TradingFormControls tradingFormControls, PlayerAccount playerAccount, List<PlayerAccount> allPlayers)
        {
            tradingFormControls.LabelPlayerSymbol.Text = playerAccount.Symbol.ToString();
            tradingFormControls.LabelPlayerName.Text = playerAccount.Name;

            ComboBoxFiller.Fill(tradingFormControls.ComboBox, allPlayers, playerAccount);
        }
        
        public void LoadOtherPlayerData(PlayerAccount playerAccount)
        {
            _otherPlayerDataLoader.LoadAllData(playerAccount);
        }

        public void RefreshBothPlayersData(PlayerAccount tradingPartner)
        {
            _activePlayerDataLoader.LoadAllData(_activePlayer);
            LoadOtherPlayerData(tradingPartner);
        }

    }
}

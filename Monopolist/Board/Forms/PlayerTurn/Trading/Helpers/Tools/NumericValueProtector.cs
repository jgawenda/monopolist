﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monopolist.Board.Forms.PlayerTurn.Trading.Helpers.Tools
{
    /// <summary>
    /// This class is used to protect the NumericUpDown value from being set over the maximum.
    /// </summary>
    public static class NumericValueProtector
    {
        public static void CorrectValue(NumericUpDown numeric)
        {
            if (numeric.Value > numeric.Maximum)
                numeric.Value = numeric.Maximum;
        }
    }
}

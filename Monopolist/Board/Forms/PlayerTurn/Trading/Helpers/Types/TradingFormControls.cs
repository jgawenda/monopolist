﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monopolist.Board.Forms.PlayerTurn.Trading.Helpers.Types
{
    public class TradingFormControls
    {
        public Label LabelPlayerSymbol { get; }
        public Label LabelPlayerName { get; }
        public ComboBox ComboBox { get; }
        public ListBox ListBoxLeft { get; }
        public ListBox ListBoxRight { get; }
        public NumericUpDown NumericUpDownLeft { get; }
        public NumericUpDown NumericUpDownRight { get; }

        public TradingFormControls(Label labelPlayerSymbol, Label labelPlayerName, ComboBox comboBox, ListBox listBoxLeft, ListBox listBoxRight, NumericUpDown numericUpDownLeft, NumericUpDown numericUpDownRight)
        {
            LabelPlayerSymbol = labelPlayerSymbol;
            LabelPlayerName = labelPlayerName;
            ComboBox = comboBox;
            ListBoxLeft = listBoxLeft;
            ListBoxRight = listBoxRight;
            NumericUpDownLeft = numericUpDownLeft;
            NumericUpDownRight = numericUpDownRight;
        }

    }
}

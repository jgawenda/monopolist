﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;
using Monopolist.Board.Types.Fields.Containers;
using Monopolist.Board.Forms.PlayerTurn.Trading.Helpers.LoadingData.Helpers;

namespace Monopolist.Board.Forms.PlayerTurn.Trading.Helpers.LoadingData
{
    public class PlayerDataLoader
    {
        private ListBoxFiller _listBoxFiller;
        private NumericUpDownAdjuster _numericAdjuster;

        public PlayerDataLoader(ListBox listBox, NumericUpDown numericUpDown, AllFieldsContainer allFieldsContainer)
        {
            _listBoxFiller = new ListBoxFiller(listBox, allFieldsContainer.AllBuyableFields);
            _numericAdjuster = new NumericUpDownAdjuster(numericUpDown);

        }

        public void LoadAllData(PlayerAccount playerAccount)
        {
            _listBoxFiller.FillBox(playerAccount);
            _numericAdjuster.AdjustNumeric(playerAccount);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;

namespace Monopolist.Board.Forms.PlayerTurn.Trading.Helpers.LoadingData.Helpers
{
    public static class ComboBoxFiller
    {
        public static void Fill(ComboBox comboBox, List<PlayerAccount> allPlayers, PlayerAccount activePlayer)
        {
            List<PlayerAccount> listWithoutActivePlayer = GetListWithoutActivePlayer(allPlayers, activePlayer);
            FillComboBox(comboBox, listWithoutActivePlayer);
        }
        
        private static List<PlayerAccount> GetListWithoutActivePlayer(List<PlayerAccount> allPlayers, PlayerAccount activePlayer)
        {
            List<PlayerAccount> outputList = allPlayers.ToList();
            outputList.Remove(activePlayer);

            return outputList;
        }

        private static void FillComboBox(ComboBox comboBox, List<PlayerAccount> listWithoutActivePlayer)
        {
            comboBox.Items.AddRange(listWithoutActivePlayer.ToArray());
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;
using Monopolist.Board.Types.Fields;

namespace Monopolist.Board.Forms.PlayerTurn.Trading.Helpers.LoadingData.Helpers
{
    public class ListBoxFiller
    {
        private ListBox _assignedListBox;
        private List<BuyableField> _allBuyableFields;

        public ListBoxFiller(ListBox listBox, List<BuyableField> allBuyableFields)
        {
            _assignedListBox = listBox;
            _allBuyableFields = allBuyableFields;
        }

        private List<BuyableField> GetBuyablesOwnedByPlayer(PlayerAccount playerAccount)
        {
            List<BuyableField> allbuyablesOwned = _allBuyableFields.Where(p => (p.Owner == playerAccount)).ToList();
            
            // Color-sets where there are Streets with some buildings.
            // We cannot sell the Street when another one has some buildings on.
            List<Color> forbiddenColors = new List<Color>();

            foreach (BuyableField field in allbuyablesOwned)
            {
                if (field is FieldStreet fieldStreet)
                {
                    if (fieldStreet.Buildings.Houses > 0 || fieldStreet.Buildings.Hotels > 0)
                        if (!forbiddenColors.Contains(fieldStreet.StreetCardInfo.StreetColor))
                            forbiddenColors.Add(fieldStreet.StreetCardInfo.StreetColor);
                }
            }
            
            foreach (var color in forbiddenColors)
                allbuyablesOwned.RemoveAll(p => (p is FieldStreet fieldStreet) && (fieldStreet.StreetCardInfo.StreetColor == color));

            return allbuyablesOwned;
        }
        
        public void FillBox(PlayerAccount playerAccount)
        {
            _assignedListBox.Items.Clear();

            List<BuyableField> ownedWithoutBuildings = GetBuyablesOwnedByPlayer(playerAccount);

            foreach (BuyableField field in ownedWithoutBuildings)
                _assignedListBox.Items.Add(field);

            if (_assignedListBox.Items.Count < 1)
            {
                _assignedListBox.Items.Add("- no properties to trade -");
                _assignedListBox.Enabled = false;
            }
            else
                _assignedListBox.Enabled = true;
        }

    }
}

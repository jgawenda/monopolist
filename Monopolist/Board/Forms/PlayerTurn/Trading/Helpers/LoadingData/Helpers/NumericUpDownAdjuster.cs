﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;

namespace Monopolist.Board.Forms.PlayerTurn.Trading.Helpers.LoadingData.Helpers
{
    public class NumericUpDownAdjuster
    {
        private NumericUpDown _assignedNumericUpDown;

        public NumericUpDownAdjuster(NumericUpDown numericUpDown)
        {
            _assignedNumericUpDown = numericUpDown;
        }

        private void AdjustNumericMax(PlayerAccount playerAccount)
        {
            int cashToSet = playerAccount.CashAccount.Cash;

            if (cashToSet < 0)
                cashToSet = 0;

            _assignedNumericUpDown.Maximum = cashToSet;
        }
        
        public void AdjustNumeric(PlayerAccount playerAccount)
        {
            _assignedNumericUpDown.Value = 0;
            AdjustNumericMax(playerAccount);
        }

    }
}

﻿namespace Monopolist.Board.Forms.PlayerTurn.Trading
{
    partial class TradingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TradingForm));
            this.listBoxLeft = new System.Windows.Forms.ListBox();
            this.listBoxRight = new System.Windows.Forms.ListBox();
            this.buttonMakeDeal = new System.Windows.Forms.Button();
            this.labelTitle = new System.Windows.Forms.Label();
            this.comboBoxOtherPlayers = new System.Windows.Forms.ComboBox();
            this.labelPlayerName = new System.Windows.Forms.Label();
            this.numericUpDownLeft = new System.Windows.Forms.NumericUpDown();
            this.labelCashLeft = new System.Windows.Forms.Label();
            this.labelCashRight = new System.Windows.Forms.Label();
            this.numericUpDownRight = new System.Windows.Forms.NumericUpDown();
            this.labelPlayerSymbol = new System.Windows.Forms.Label();
            this.linkLabelClose = new System.Windows.Forms.LinkLabel();
            this.labelYou = new System.Windows.Forms.Label();
            this.labelTradingPartner = new System.Windows.Forms.Label();
            this.labelFor = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRight)).BeginInit();
            this.SuspendLayout();
            // 
            // listBoxLeft
            // 
            this.listBoxLeft.FormattingEnabled = true;
            this.listBoxLeft.Location = new System.Drawing.Point(35, 79);
            this.listBoxLeft.Name = "listBoxLeft";
            this.listBoxLeft.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBoxLeft.Size = new System.Drawing.Size(179, 212);
            this.listBoxLeft.TabIndex = 0;
            // 
            // listBoxRight
            // 
            this.listBoxRight.FormattingEnabled = true;
            this.listBoxRight.Location = new System.Drawing.Point(281, 79);
            this.listBoxRight.Name = "listBoxRight";
            this.listBoxRight.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBoxRight.Size = new System.Drawing.Size(179, 212);
            this.listBoxRight.TabIndex = 1;
            // 
            // buttonMakeDeal
            // 
            this.buttonMakeDeal.Enabled = false;
            this.buttonMakeDeal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMakeDeal.Location = new System.Drawing.Point(199, 339);
            this.buttonMakeDeal.Name = "buttonMakeDeal";
            this.buttonMakeDeal.Size = new System.Drawing.Size(99, 43);
            this.buttonMakeDeal.TabIndex = 2;
            this.buttonMakeDeal.Text = "Make a deal";
            this.buttonMakeDeal.UseVisualStyleBackColor = true;
            this.buttonMakeDeal.Click += new System.EventHandler(this.ButtonMakeDeal_Click);
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTitle.Location = new System.Drawing.Point(215, 9);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(62, 20);
            this.labelTitle.TabIndex = 3;
            this.labelTitle.Text = "Trading";
            // 
            // comboBoxOtherPlayers
            // 
            this.comboBoxOtherPlayers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOtherPlayers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBoxOtherPlayers.FormattingEnabled = true;
            this.comboBoxOtherPlayers.Location = new System.Drawing.Point(366, 52);
            this.comboBoxOtherPlayers.Name = "comboBoxOtherPlayers";
            this.comboBoxOtherPlayers.Size = new System.Drawing.Size(94, 21);
            this.comboBoxOtherPlayers.TabIndex = 4;
            this.comboBoxOtherPlayers.SelectedIndexChanged += new System.EventHandler(this.ComboBoxOtherPlayers_SelectedIndexChanged);
            // 
            // labelPlayerName
            // 
            this.labelPlayerName.AutoSize = true;
            this.labelPlayerName.Location = new System.Drawing.Point(104, 60);
            this.labelPlayerName.Name = "labelPlayerName";
            this.labelPlayerName.Size = new System.Drawing.Size(64, 13);
            this.labelPlayerName.TabIndex = 5;
            this.labelPlayerName.Text = "PlayerName";
            // 
            // numericUpDownLeft
            // 
            this.numericUpDownLeft.Location = new System.Drawing.Point(99, 297);
            this.numericUpDownLeft.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDownLeft.Name = "numericUpDownLeft";
            this.numericUpDownLeft.Size = new System.Drawing.Size(77, 20);
            this.numericUpDownLeft.TabIndex = 6;
            this.numericUpDownLeft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownLeft.KeyUp += new System.Windows.Forms.KeyEventHandler(this.NumericUpDownLeft_KeyUp);
            // 
            // labelCashLeft
            // 
            this.labelCashLeft.AutoSize = true;
            this.labelCashLeft.Location = new System.Drawing.Point(62, 300);
            this.labelCashLeft.Name = "labelCashLeft";
            this.labelCashLeft.Size = new System.Drawing.Size(31, 13);
            this.labelCashLeft.TabIndex = 8;
            this.labelCashLeft.Text = "Cash";
            // 
            // labelCashRight
            // 
            this.labelCashRight.AutoSize = true;
            this.labelCashRight.Location = new System.Drawing.Point(307, 300);
            this.labelCashRight.Name = "labelCashRight";
            this.labelCashRight.Size = new System.Drawing.Size(31, 13);
            this.labelCashRight.TabIndex = 10;
            this.labelCashRight.Text = "Cash";
            // 
            // numericUpDownRight
            // 
            this.numericUpDownRight.Location = new System.Drawing.Point(344, 297);
            this.numericUpDownRight.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDownRight.Name = "numericUpDownRight";
            this.numericUpDownRight.Size = new System.Drawing.Size(77, 20);
            this.numericUpDownRight.TabIndex = 9;
            this.numericUpDownRight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownRight.KeyUp += new System.Windows.Forms.KeyEventHandler(this.NumericUpDownRight_KeyUp);
            // 
            // labelPlayerSymbol
            // 
            this.labelPlayerSymbol.AutoSize = true;
            this.labelPlayerSymbol.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPlayerSymbol.Location = new System.Drawing.Point(68, 51);
            this.labelPlayerSymbol.Name = "labelPlayerSymbol";
            this.labelPlayerSymbol.Size = new System.Drawing.Size(31, 25);
            this.labelPlayerSymbol.TabIndex = 11;
            this.labelPlayerSymbol.Text = "%";
            // 
            // linkLabelClose
            // 
            this.linkLabelClose.AutoSize = true;
            this.linkLabelClose.Location = new System.Drawing.Point(231, 397);
            this.linkLabelClose.Name = "linkLabelClose";
            this.linkLabelClose.Size = new System.Drawing.Size(32, 13);
            this.linkLabelClose.TabIndex = 12;
            this.linkLabelClose.TabStop = true;
            this.linkLabelClose.Text = "close";
            this.linkLabelClose.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabelClose_LinkClicked);
            // 
            // labelYou
            // 
            this.labelYou.AutoSize = true;
            this.labelYou.Location = new System.Drawing.Point(35, 60);
            this.labelYou.Name = "labelYou";
            this.labelYou.Size = new System.Drawing.Size(29, 13);
            this.labelYou.TabIndex = 13;
            this.labelYou.Text = "You:";
            // 
            // labelTradingPartner
            // 
            this.labelTradingPartner.AutoSize = true;
            this.labelTradingPartner.Location = new System.Drawing.Point(278, 60);
            this.labelTradingPartner.Name = "labelTradingPartner";
            this.labelTradingPartner.Size = new System.Drawing.Size(82, 13);
            this.labelTradingPartner.TabIndex = 14;
            this.labelTradingPartner.Text = "Trading partner:";
            // 
            // labelFor
            // 
            this.labelFor.AutoSize = true;
            this.labelFor.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelFor.Location = new System.Drawing.Point(229, 179);
            this.labelFor.Name = "labelFor";
            this.labelFor.Size = new System.Drawing.Size(37, 25);
            this.labelFor.TabIndex = 15;
            this.labelFor.Text = "for";
            // 
            // TradingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(496, 422);
            this.ControlBox = false;
            this.Controls.Add(this.labelFor);
            this.Controls.Add(this.labelTradingPartner);
            this.Controls.Add(this.labelYou);
            this.Controls.Add(this.linkLabelClose);
            this.Controls.Add(this.labelPlayerSymbol);
            this.Controls.Add(this.labelCashRight);
            this.Controls.Add(this.numericUpDownRight);
            this.Controls.Add(this.labelCashLeft);
            this.Controls.Add(this.numericUpDownLeft);
            this.Controls.Add(this.labelPlayerName);
            this.Controls.Add(this.comboBoxOtherPlayers);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.buttonMakeDeal);
            this.Controls.Add(this.listBoxRight);
            this.Controls.Add(this.listBoxLeft);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TradingForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRight)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxLeft;
        private System.Windows.Forms.ListBox listBoxRight;
        private System.Windows.Forms.Button buttonMakeDeal;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.ComboBox comboBoxOtherPlayers;
        private System.Windows.Forms.Label labelPlayerName;
        private System.Windows.Forms.NumericUpDown numericUpDownLeft;
        private System.Windows.Forms.Label labelCashLeft;
        private System.Windows.Forms.Label labelCashRight;
        private System.Windows.Forms.NumericUpDown numericUpDownRight;
        private System.Windows.Forms.Label labelPlayerSymbol;
        private System.Windows.Forms.LinkLabel linkLabelClose;
        private System.Windows.Forms.Label labelYou;
        private System.Windows.Forms.Label labelTradingPartner;
        private System.Windows.Forms.Label labelFor;
    }
}
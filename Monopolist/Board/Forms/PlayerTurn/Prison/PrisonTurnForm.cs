﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Forms.PlayerTurn.Prison.Helpers;
using Monopolist.Core.Types.Players;

namespace Monopolist.Board.Forms.PlayerTurn.Prison
{
    public partial class PrisonTurnForm : Form
    {
        private int _fine;
        private PlayerAccount _playerAccount;

        private PrisonTurnFormFiller _formFiller;
        private PrisonTurnOperator _prisonTurnOperator;

        public PrisonTurnForm(PlayerAccount playerAccount, int fine)
        {
            InitializeComponent();

            _fine = fine;
            _playerAccount = playerAccount;

            _prisonTurnOperator = new PrisonTurnOperator(fine);
            PrepareFormElements();
        }

        private void PrepareFormElements()
        {
            _formFiller = new PrisonTurnFormFiller(labelRemainingNumber, buttonPayFine, _fine);
            _formFiller.UpdateInfo(_playerAccount);
        }

        private void ButtonPayFine_Click(object sender, EventArgs e)
        {
            _prisonTurnOperator.PayFine(_playerAccount);
            CloseForm(DialogResult.Yes);
        }

        private void ButtonRolling_Click(object sender, EventArgs e)
        {
            if (_prisonTurnOperator.RollDice(_playerAccount))
                CloseForm(DialogResult.Yes);
            else
                CloseForm(DialogResult.No);
        }

        private void CloseForm(DialogResult dialogResult)
        {
            DialogResult = dialogResult;
            this.Close();
        }
    }
}

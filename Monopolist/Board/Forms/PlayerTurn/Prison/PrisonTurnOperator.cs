﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Forms.PlayerTurn.Prison.Helpers;
using Monopolist.Board.Types.Fields;
using Monopolist.Core.Types.Players;

namespace Monopolist.Board.Forms.PlayerTurn.Prison
{
    public class PrisonTurnOperator
    {
        private int _fine;

        public PrisonTurnOperator(int fine)
        {
            _fine = fine;
        }

        private void FreeFromPrison(PlayerAccount playerAccount)
        {
            FieldPrison prison = (FieldPrison)playerAccount.FieldOn;
            prison.FreeFromPrison(playerAccount);
        }
        
        public void PayFine(PlayerAccount playerAccount)
        {
            playerAccount.CashAccount.RemoveCash(_fine);
            FreeFromPrison(playerAccount);
        }

        public bool RollDice(PlayerAccount playerAccount)
        {
            bool rollResult = PrisonDiceRollOperator.RollDice(playerAccount);

            if (rollResult)
                FreeFromPrison(playerAccount);
            else
            {
                playerAccount.PrisonStatus.RemoveOneDay();

                if (!playerAccount.PrisonStatus.IsInPrison)
                    FreeFromPrison(playerAccount);
            }

            return rollResult;
        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;

namespace Monopolist.Board.Forms.PlayerTurn.Prison.Helpers
{
    public class PrisonTurnFormFiller
    {
        private Label _remainingTurnsLabel;
        private Button _payFineButton;
        private int _fine;
        
        public PrisonTurnFormFiller(Label remainingTurnsLabel, Button payFineButton, int fine)
        {
            _remainingTurnsLabel = remainingTurnsLabel;
            _payFineButton = payFineButton;
            _fine = fine;
        }

        private void AdjustButton(int playerCash)
        {
            _payFineButton.Text = String.Format("Pay fine ${0}", _fine);

            if (playerCash < _fine)
                _payFineButton.Enabled = false;
        }

        private void AdjustRemainingTurns(int remainingTurns)
        {
            _remainingTurnsLabel.Text = remainingTurns.ToString();
        }

        public void UpdateInfo(PlayerAccount playerAccount)
        {
            AdjustButton(playerAccount.CashAccount.Cash);
            AdjustRemainingTurns(playerAccount.PrisonStatus.TurnsLeftInPrison);
        }

    }
}

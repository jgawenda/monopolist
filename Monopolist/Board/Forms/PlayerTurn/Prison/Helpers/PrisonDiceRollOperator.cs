﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;
using Monopolist.Board.Forms.PlayerTurn.DiceRoll.Helpers;

namespace Monopolist.Board.Forms.PlayerTurn.Prison.Helpers
{
    public static class PrisonDiceRollOperator
    {
        public static bool RollDice(PlayerAccount playerAccount)
        {
            bool result = false;

            DiceRollCalculator diceRollCalculator = new DiceRollCalculator();
            var diceResult = diceRollCalculator.RollDices(1, 6);

            string message = String.Format("{0} & {1}", diceResult.Dice1, diceResult.Dice2);

            if (diceResult.Double)
            {
                message += "\nYou got out of prison!";
                result = true;
            }
            else
                message += "\nYou didn't get out of prison.";

            MessageBox.Show(message);
            return result;
        }

    }
}

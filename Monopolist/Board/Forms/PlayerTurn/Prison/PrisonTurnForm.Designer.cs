﻿namespace Monopolist.Board.Forms.PlayerTurn.Prison
{
    partial class PrisonTurnForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrisonTurnForm));
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelDescription = new System.Windows.Forms.Label();
            this.buttonPayFine = new System.Windows.Forms.Button();
            this.buttonRolling = new System.Windows.Forms.Button();
            this.labelOr = new System.Windows.Forms.Label();
            this.labelRemainingTitle = new System.Windows.Forms.Label();
            this.labelRemainingNumber = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTitle.Location = new System.Drawing.Point(138, 9);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(51, 18);
            this.labelTitle.TabIndex = 0;
            this.labelTitle.Text = "Prison";
            // 
            // labelDescription
            // 
            this.labelDescription.AutoSize = true;
            this.labelDescription.Location = new System.Drawing.Point(117, 40);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(89, 13);
            this.labelDescription.TabIndex = 1;
            this.labelDescription.Text = "You are in prison.";
            // 
            // buttonPayFine
            // 
            this.buttonPayFine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPayFine.Location = new System.Drawing.Point(29, 116);
            this.buttonPayFine.Name = "buttonPayFine";
            this.buttonPayFine.Size = new System.Drawing.Size(112, 23);
            this.buttonPayFine.TabIndex = 2;
            this.buttonPayFine.Text = "Pay fine $50";
            this.buttonPayFine.UseVisualStyleBackColor = true;
            this.buttonPayFine.Click += new System.EventHandler(this.ButtonPayFine_Click);
            // 
            // buttonRolling
            // 
            this.buttonRolling.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRolling.Location = new System.Drawing.Point(187, 116);
            this.buttonRolling.Name = "buttonRolling";
            this.buttonRolling.Size = new System.Drawing.Size(112, 23);
            this.buttonRolling.TabIndex = 3;
            this.buttonRolling.Text = "Try rolling double";
            this.buttonRolling.UseVisualStyleBackColor = true;
            this.buttonRolling.Click += new System.EventHandler(this.ButtonRolling_Click);
            // 
            // labelOr
            // 
            this.labelOr.AutoSize = true;
            this.labelOr.Location = new System.Drawing.Point(156, 121);
            this.labelOr.Name = "labelOr";
            this.labelOr.Size = new System.Drawing.Size(16, 13);
            this.labelOr.TabIndex = 4;
            this.labelOr.Text = "or";
            // 
            // labelRemainingTitle
            // 
            this.labelRemainingTitle.AutoSize = true;
            this.labelRemainingTitle.Location = new System.Drawing.Point(111, 82);
            this.labelRemainingTitle.Name = "labelRemainingTitle";
            this.labelRemainingTitle.Size = new System.Drawing.Size(86, 13);
            this.labelRemainingTitle.TabIndex = 5;
            this.labelRemainingTitle.Text = "Remaining turns:";
            // 
            // labelRemainingNumber
            // 
            this.labelRemainingNumber.AutoSize = true;
            this.labelRemainingNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelRemainingNumber.Location = new System.Drawing.Point(196, 82);
            this.labelRemainingNumber.Name = "labelRemainingNumber";
            this.labelRemainingNumber.Size = new System.Drawing.Size(14, 13);
            this.labelRemainingNumber.TabIndex = 6;
            this.labelRemainingNumber.Text = "3";
            // 
            // PrisonTurnForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(330, 161);
            this.ControlBox = false;
            this.Controls.Add(this.labelRemainingNumber);
            this.Controls.Add(this.labelRemainingTitle);
            this.Controls.Add(this.labelOr);
            this.Controls.Add(this.buttonRolling);
            this.Controls.Add(this.buttonPayFine);
            this.Controls.Add(this.labelDescription);
            this.Controls.Add(this.labelTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PrisonTurnForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.Button buttonPayFine;
        private System.Windows.Forms.Button buttonRolling;
        private System.Windows.Forms.Label labelOr;
        private System.Windows.Forms.Label labelRemainingTitle;
        private System.Windows.Forms.Label labelRemainingNumber;
    }
}
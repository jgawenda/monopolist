﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Types.Fields;
using Monopolist.Board.Types.Fields.Containers;
using Monopolist.Board.Forms.PlayerTurn.Helpers.Tools;

namespace Monopolist.Board.Forms.PlayerTurn.HousesOperations.SellHouses
{
    public class HousesSellingOperator
    {
        private AllFieldsContainer _allFieldsContainer;

        public HousesSellingOperator(AllFieldsContainer allFieldsContainer)
        {
            _allFieldsContainer = allFieldsContainer;
        }

        private bool CheckHouseDifferences(FieldStreet fieldStreet, List<FieldStreet> streetsOfSameColor)
        {
            bool result = true;
            
            int maximumBuildings = fieldStreet.Buildings.Houses + 2;

            if (fieldStreet.Buildings.Hotels > 0)
                maximumBuildings = 5;
            
            foreach (FieldStreet street in streetsOfSameColor)
            {
                if ((street.Buildings.Houses >= maximumBuildings) || ((fieldStreet.Buildings.Houses == 3) && (street.Buildings.Hotels > 0)))
                {
                    result = false;
                    break;
                }
            }

            return result;
        }

        public bool SellHouse(FieldStreet fieldStreet)
        {
            bool result = false;
            List<FieldStreet> sameColorStreets = SameColorStreetsExtractor.GetStreetsOfTheSameColor(_allFieldsContainer.AllStreets, fieldStreet);
            
            if (CheckHouseDifferences(fieldStreet, sameColorStreets))
            {
                fieldStreet.SellBuilding();
                result = true;
            }
            else
                MessageBox.Show("You can't sell a house, because of Even Build Rule.\nSell more houses from other Streets first.", "Can't sell a house", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            return result;
        }

    }
}

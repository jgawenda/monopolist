﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;
using Monopolist.Board.Types.Fields;
using Monopolist.Board.Types.Fields.Containers;
using Monopolist.Board.Forms.PlayerTurn.HousesOperations.BuyHouses;
using Monopolist.Board.Forms.PlayerTurn.HousesOperations.SellHouses;
using Monopolist.Board.Forms.PlayerTurn.HousesOperations.Helpers;
using Monopolist.Core.Actions.GameProcess.Helpers;

namespace Monopolist.Board.Forms.PlayerTurn.HousesOperations
{
    public partial class HousesForm : Form
    {
        private PlayerAccount _playerAccount;

        private PlayerFormsUpdater _playerFormsUpdater;
        
        private ButtonsAdjuster _buttonsAdjuster;
        private StreetsComboLoader _streetsComboLoader;

        private HousesBuyingOperator _housesBuyingOperator;
        private HousesSellingOperator _housesSellingOperator;

        public HousesForm(PlayerAccount playerAccount, AllFieldsContainer allFieldsContainer, PlayerFormsUpdater playerFormsUpdater, bool leftButtonVisible)
        {
            InitializeComponent();

            _playerAccount = playerAccount;
            _playerFormsUpdater = playerFormsUpdater;
            _buttonsAdjuster = new ButtonsAdjuster(buttonBuyHouse, buttonSellHouse, leftButtonVisible);
            _streetsComboLoader = new StreetsComboLoader(comboBoxStreets, allFieldsContainer.AllStreets, _playerAccount);
            _housesBuyingOperator = new HousesBuyingOperator(allFieldsContainer);
            _housesSellingOperator = new HousesSellingOperator(allFieldsContainer);
        }

        private void ComboBoxStreets_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxStreets.SelectedItem is FieldStreet)
            {
                _buttonsAdjuster.EnableButtons();
                _buttonsAdjuster.UpdateButtonsTexts((FieldStreet)comboBoxStreets.SelectedItem);
            }
            else
                _buttonsAdjuster.DisableButtons();
        }

        private void ButtonBuyHouse_Click(object sender, EventArgs e)
        {
            if (_housesBuyingOperator.BuyHouse((FieldStreet)comboBoxStreets.SelectedItem))
                RefreshFormsAndControls();
        }

        private void ButtonSellHouse_Click(object sender, EventArgs e)
        {
            if (_housesSellingOperator.SellHouse((FieldStreet)comboBoxStreets.SelectedItem))
                RefreshFormsAndControls();
        }

        private void RefreshFormsAndControls()
        {
            _streetsComboLoader.Refresh();
            _playerFormsUpdater.UpdateInfo();
        }

        private void LinkLabelCloseForm_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }
    }
}

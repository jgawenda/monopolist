﻿namespace Monopolist.Board.Forms.PlayerTurn.HousesOperations
{
    partial class HousesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HousesForm));
            this.comboBoxStreets = new System.Windows.Forms.ComboBox();
            this.labelTitle = new System.Windows.Forms.Label();
            this.buttonBuyHouse = new System.Windows.Forms.Button();
            this.linkLabelCloseForm = new System.Windows.Forms.LinkLabel();
            this.buttonSellHouse = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // comboBoxStreets
            // 
            this.comboBoxStreets.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxStreets.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBoxStreets.FormattingEnabled = true;
            this.comboBoxStreets.Location = new System.Drawing.Point(12, 39);
            this.comboBoxStreets.Name = "comboBoxStreets";
            this.comboBoxStreets.Size = new System.Drawing.Size(231, 21);
            this.comboBoxStreets.TabIndex = 0;
            this.comboBoxStreets.SelectedIndexChanged += new System.EventHandler(this.ComboBoxStreets_SelectedIndexChanged);
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Location = new System.Drawing.Point(67, 12);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(120, 13);
            this.labelTitle.TabIndex = 2;
            this.labelTitle.Text = "Buying / Selling Houses";
            // 
            // buttonBuyHouse
            // 
            this.buttonBuyHouse.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.buttonBuyHouse.Enabled = false;
            this.buttonBuyHouse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBuyHouse.Location = new System.Drawing.Point(28, 75);
            this.buttonBuyHouse.Name = "buttonBuyHouse";
            this.buttonBuyHouse.Size = new System.Drawing.Size(97, 23);
            this.buttonBuyHouse.TabIndex = 3;
            this.buttonBuyHouse.Text = "Buy 1";
            this.buttonBuyHouse.UseVisualStyleBackColor = false;
            this.buttonBuyHouse.Click += new System.EventHandler(this.ButtonBuyHouse_Click);
            // 
            // linkLabelCloseForm
            // 
            this.linkLabelCloseForm.AutoSize = true;
            this.linkLabelCloseForm.Location = new System.Drawing.Point(112, 136);
            this.linkLabelCloseForm.Name = "linkLabelCloseForm";
            this.linkLabelCloseForm.Size = new System.Drawing.Size(32, 13);
            this.linkLabelCloseForm.TabIndex = 4;
            this.linkLabelCloseForm.TabStop = true;
            this.linkLabelCloseForm.Text = "close";
            this.linkLabelCloseForm.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabelCloseForm_LinkClicked);
            // 
            // buttonSellHouse
            // 
            this.buttonSellHouse.BackColor = System.Drawing.Color.Coral;
            this.buttonSellHouse.Enabled = false;
            this.buttonSellHouse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSellHouse.Location = new System.Drawing.Point(131, 75);
            this.buttonSellHouse.Name = "buttonSellHouse";
            this.buttonSellHouse.Size = new System.Drawing.Size(97, 23);
            this.buttonSellHouse.TabIndex = 5;
            this.buttonSellHouse.Text = "Sell 1";
            this.buttonSellHouse.UseVisualStyleBackColor = false;
            this.buttonSellHouse.Click += new System.EventHandler(this.ButtonSellHouse_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label1.Location = new System.Drawing.Point(2, 111);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(251, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "You can sell buildings for only half their original cost!";
            // 
            // HousesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(255, 158);
            this.ControlBox = false;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonSellHouse);
            this.Controls.Add(this.linkLabelCloseForm);
            this.Controls.Add(this.buttonBuyHouse);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.comboBoxStreets);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "HousesForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxStreets;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Button buttonBuyHouse;
        private System.Windows.Forms.LinkLabel linkLabelCloseForm;
        private System.Windows.Forms.Button buttonSellHouse;
        private System.Windows.Forms.Label label1;
    }
}
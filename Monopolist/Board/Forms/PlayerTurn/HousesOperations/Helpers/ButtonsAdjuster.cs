﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Types.Fields;

namespace Monopolist.Board.Forms.PlayerTurn.HousesOperations.Helpers
{
    public class ButtonsAdjuster
    {
        private Button _buttonBuy;
        private Button _buttonSell;

        private string _textBuy;
        private string _textSell;

        public ButtonsAdjuster(Button buttonBuy, Button buttonSell, bool leftButtonVisible)
        {
            _buttonBuy = buttonBuy;
            _buttonSell = buttonSell;

            _textBuy = "Buy 1";
            _textSell = "Sell 1";

            PrepareButtonsVisibility(leftButtonVisible);
        }

        private void PrepareButtonsVisibility(bool leftButtonVisible)
        {
            if (!leftButtonVisible)
                _buttonBuy.Visible = false;
        }
        
        public void UpdateButtonsTexts(FieldStreet fieldStreet)
        {
            _buttonBuy.Text = String.Format("{0} (${1})", _textBuy, fieldStreet.StreetCardInfo.OneHousePrice);
            _buttonSell.Text = String.Format("{0} (${1})", _textSell, fieldStreet.StreetCardInfo.OneHousePrice / 2);
        }

        public void EnableButtons()
        {
            _buttonBuy.Enabled = true;
            _buttonSell.Enabled = true;
        }

        public void DisableButtons()
        {
            _buttonBuy.Enabled = false;
            _buttonBuy.Text = "---";
            _buttonSell.Enabled = false;
            _buttonSell.Text = "---";
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using Monopolist.Board.Types.Fields;
using Monopolist.Core.Types.Players;

namespace Monopolist.Board.Forms.PlayerTurn.HousesOperations.Helpers
{
    public class StreetsComboLoader
    {
        private ComboBox _comboBoxStreets;
        private List<FieldStreet> _allStreetsFromBoard;

        public StreetsComboLoader(ComboBox comboBoxStreets, List<FieldStreet> allStreetsFromBoard, PlayerAccount playerAccount)
        {
            _comboBoxStreets = comboBoxStreets;
            _allStreetsFromBoard = allStreetsFromBoard;

            LoadStreets(playerAccount);
            _comboBoxStreets.SelectedIndex = 0;
        }

        private void LoadStreets(PlayerAccount playerAccount)
        {
            Color lastOkColor = Color.Black;

            foreach (FieldStreet street in _allStreetsFromBoard)
            {
                if (street.StreetCardInfo.StreetColor == lastOkColor)
                    _comboBoxStreets.Items.Add(street);
                else if (street.CheckSameColor(playerAccount))
                {
                    _comboBoxStreets.Items.Add(street);
                    lastOkColor = street.StreetCardInfo.StreetColor;
                }
            }

            if (_comboBoxStreets.Items.Count == 0)
                _comboBoxStreets.Items.Add("no streets");
        }

        public void Refresh()
        {
            int selectedIndex = _comboBoxStreets.SelectedIndex;
            object[] tempArray = new object[_comboBoxStreets.Items.Count];
            _comboBoxStreets.Items.CopyTo(tempArray, 0);
            _comboBoxStreets.Items.Clear();
            _comboBoxStreets.Items.AddRange(tempArray);
            _comboBoxStreets.SelectedIndex = selectedIndex;
        }

    }
}

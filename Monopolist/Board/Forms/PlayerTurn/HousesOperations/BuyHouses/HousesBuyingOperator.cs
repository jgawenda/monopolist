﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Types.Fields;
using Monopolist.Board.Types.Fields.Containers;
using Monopolist.Board.Forms.PlayerTurn.Helpers.Tools;

namespace Monopolist.Board.Forms.PlayerTurn.HousesOperations.BuyHouses
{
    public class HousesBuyingOperator
    {
        private AllFieldsContainer _allFieldsContainer;

        public HousesBuyingOperator(AllFieldsContainer allFieldsContainer)
        {
            _allFieldsContainer = allFieldsContainer;
        }
        
        private bool CheckIfNotMortgaged(List<FieldStreet> streetsOfSameColor)
        {
            bool result = true;

            foreach (FieldStreet street in streetsOfSameColor)
            {
                if (street.Mortgage == Field.MortgageStatus.Mortgaged)
                {
                    result = false;
                    break;
                }
            }

            return result;
        }

        private bool CheckHouseDifferences(FieldStreet fieldStreet, List<FieldStreet> streetsOfSameColor)
        {
            bool result = true;
            int minimumBuildings = fieldStreet.Buildings.Houses - 2;
            
            foreach (FieldStreet street in streetsOfSameColor)
            {
                if (street.Buildings.Houses <= minimumBuildings && street.Buildings.Hotels < 1)
                {
                    result = false;
                    break;
                }
            }

            return result;
        }

        public bool BuyHouse(FieldStreet fieldStreet)
        {
            bool result = false;
            List<FieldStreet> sameColorStreets = SameColorStreetsExtractor.GetStreetsOfTheSameColor(_allFieldsContainer.AllStreets, fieldStreet);

            if (CheckIfNotMortgaged(sameColorStreets))
            {
                if (CheckHouseDifferences(fieldStreet, sameColorStreets))
                {
                    fieldStreet.BuyBuilding();
                    result = true;
                }
                else
                    MessageBox.Show("You can't buy a house, because of Even Build Rule.\nBuild more houses on other Streets first.", "Can't buy a house", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
            else
                MessageBox.Show("You can't buy a house, because there are Mortgaged Streets in this set of color.", "Can't buy a house", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            return result;
        }

    }
}

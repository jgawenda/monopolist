﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Types.Fields;
using Monopolist.Core.Types.Players;

namespace Monopolist.Board.Forms.PlayerTurn.Mortgage.Helpers
{
    public class MorgtgageComboLoader
    {
        private ComboBox _comboBox;
        private List<BuyableField> _allBuyablesFromBoard;

        public MorgtgageComboLoader(ComboBox comboBoxProperties, List<BuyableField> allBuyablesFromBoard, PlayerAccount playerAccount)
        {
            _comboBox = comboBoxProperties;
            _allBuyablesFromBoard = allBuyablesFromBoard;

            LoadStreets(playerAccount);
            _comboBox.SelectedIndex = 0;
        }

        private void LoadStreets(PlayerAccount playerAccount)
        {
            foreach (BuyableField property in _allBuyablesFromBoard)
            {
                if (property.Owner == playerAccount)
                    _comboBox.Items.Add(property);
            }

            if (_comboBox.Items.Count == 0)
                _comboBox.Items.Add("no properties owned");
        }
        
        public void Refresh()
        {
            int selectedIndex = _comboBox.SelectedIndex;
            object[] tempArray = new object[_comboBox.Items.Count];
            _comboBox.Items.CopyTo(tempArray, 0);
            _comboBox.Items.Clear();
            _comboBox.Items.AddRange(tempArray);
            _comboBox.SelectedIndex = selectedIndex;
        }

    }
}

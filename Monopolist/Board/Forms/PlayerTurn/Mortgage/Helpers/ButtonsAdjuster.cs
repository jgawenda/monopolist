﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Types.Fields;

namespace Monopolist.Board.Forms.PlayerTurn.Mortgage.Helpers
{
    public class ButtonsAdjuster
    {
        private Button _mortgageButton;
        private Button _liftMortgageButton;

        private string _textMortgage;
        private string _textLiftMortgage;

        public ButtonsAdjuster(Button mortgageButton, Button liftMortgageButton, bool rightButtonVisible)
        {
            _mortgageButton = mortgageButton;
            _liftMortgageButton = liftMortgageButton;

            _textMortgage = "Mortgage";
            _textLiftMortgage = "Lift mortgage";

            PrepareButtonsVisibility(rightButtonVisible);
        }

        private void PrepareButtonsVisibility(bool rightButtonVisible)
        {
            if (!rightButtonVisible)
                _liftMortgageButton.Visible = false;
        }

        public void UpdateButtonsTexts(BuyableField buyableField)
        {
            _mortgageButton.Text = String.Format("{0}\n(${1})", _textMortgage, buyableField.CardInfo.MortgageValue);
            _liftMortgageButton.Text = String.Format("{0}\n(${1})", _textLiftMortgage, buyableField.CardInfo.MortgageBuyoutFullPrice);
        }

        public void EnableButtons()
        {
            _mortgageButton.Enabled = true;
            _liftMortgageButton.Enabled = true;
        }

        public void DisableButtons()
        {
            _mortgageButton.Enabled = false;
            _mortgageButton.Text = "---";
            _liftMortgageButton.Enabled = false;
            _liftMortgageButton.Text = "---";
        }

    }
}

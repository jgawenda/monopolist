﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;
using Monopolist.Board.Types.Fields;
using Monopolist.Board.Types.Fields.Containers;
using Monopolist.Board.Forms.PlayerTurn.Mortgage.Helpers;
using Monopolist.Board.Forms.PlayerTurn.Mortgage.MakeMortgage;
using Monopolist.Core.Actions.GameProcess.Helpers;

namespace Monopolist.Board.Forms.PlayerTurn.Mortgage
{
    public partial class MortgageForm : Form
    {
        private PlayerAccount _playerAccount;

        private PlayerFormsUpdater _playerFormsUpdater;

        private ButtonsAdjuster _buttonsAdjuster;
        private MorgtgageComboLoader _comboLoader;

        private MortgageMakingOperator _mortgageMakingOperator;

        public MortgageForm(PlayerAccount playerAccount, AllFieldsContainer allFieldsContainer, PlayerFormsUpdater playerFormsUpdater, bool rightButtonVisible)
        {
            InitializeComponent();

            _playerAccount = playerAccount;
            _playerFormsUpdater = playerFormsUpdater;
            _buttonsAdjuster = new ButtonsAdjuster(buttonMortgage, buttonLiftMortgage, rightButtonVisible);
            _comboLoader = new MorgtgageComboLoader(comboBoxProperties, allFieldsContainer.AllBuyableFields, _playerAccount);

            _mortgageMakingOperator = new MortgageMakingOperator(allFieldsContainer);
        }

        private void ComboBoxProperties_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxProperties.SelectedItem is BuyableField)
            {
                _buttonsAdjuster.EnableButtons();
                _buttonsAdjuster.UpdateButtonsTexts((BuyableField)comboBoxProperties.SelectedItem);
            }
            else
                _buttonsAdjuster.DisableButtons();
        }

        private void ButtonMortgage_Click(object sender, EventArgs e)
        {
            if (_mortgageMakingOperator.MakeMortgage((BuyableField)comboBoxProperties.SelectedItem))
                RefreshFormsAndControls();
        }

        private void ButtonLiftMortgage_Click(object sender, EventArgs e)
        {
            BuyableField buyableField = (BuyableField)comboBoxProperties.SelectedItem;
            buyableField.BuyoutMortgage();

            RefreshFormsAndControls();
        }

        private void RefreshFormsAndControls()
        {
            _comboLoader.Refresh();
            _playerFormsUpdater.UpdateInfo();
        }

        private void LinkLabelClose_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Types.Fields;
using Monopolist.Board.Types.Fields.Containers;
using Monopolist.Board.Forms.PlayerTurn.Helpers.Tools;

namespace Monopolist.Board.Forms.PlayerTurn.Mortgage.MakeMortgage
{
    public class MortgageMakingOperator
    {
        private AllFieldsContainer _allFieldsContainer;

        public MortgageMakingOperator(AllFieldsContainer allFieldsContainer)
        {
            _allFieldsContainer = allFieldsContainer;
        }
        
        private bool CheckIfStreetsHaveNoBuildings(List<FieldStreet> streetsOfSameColor)
        {
            bool areStreetsClear = true;

            foreach (FieldStreet street in streetsOfSameColor)
            {
                if ((street.Buildings.Houses > 0) || (street.Buildings.Hotels > 0))
                {
                    areStreetsClear = false;
                    break;
                }
            }

            return areStreetsClear;
        }

        public bool MakeMortgage(BuyableField buyableField)
        {
            bool canWeMakeMortgage = true;

            if (buyableField is FieldStreet fieldStreet)
            {
                if (fieldStreet.CheckSameColor(buyableField.Owner))
                {
                    List<FieldStreet> sameColorStreets = SameColorStreetsExtractor.GetStreetsOfTheSameColor(_allFieldsContainer.AllStreets, fieldStreet);

                    if (!CheckIfStreetsHaveNoBuildings(sameColorStreets))
                    {
                        canWeMakeMortgage = false;
                        MessageBox.Show("You can't mortgage this street.\nAll the Houses and Hotels on all the properties of this Street's color-group must be sold first.", "Can't mortgage street", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }

            if (canWeMakeMortgage)
                buyableField.MakeMortgage();

            return canWeMakeMortgage;
        }

    }
}

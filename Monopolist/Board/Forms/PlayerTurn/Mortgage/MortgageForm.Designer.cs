﻿namespace Monopolist.Board.Forms.PlayerTurn.Mortgage
{
    partial class MortgageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MortgageForm));
            this.labelTitle = new System.Windows.Forms.Label();
            this.comboBoxProperties = new System.Windows.Forms.ComboBox();
            this.buttonMortgage = new System.Windows.Forms.Button();
            this.buttonLiftMortgage = new System.Windows.Forms.Button();
            this.linkLabelClose = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Location = new System.Drawing.Point(70, 17);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(132, 13);
            this.labelTitle.TabIndex = 0;
            this.labelTitle.Text = "Make / Remove mortgage";
            // 
            // comboBoxProperties
            // 
            this.comboBoxProperties.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxProperties.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBoxProperties.FormattingEnabled = true;
            this.comboBoxProperties.Location = new System.Drawing.Point(12, 48);
            this.comboBoxProperties.Name = "comboBoxProperties";
            this.comboBoxProperties.Size = new System.Drawing.Size(243, 21);
            this.comboBoxProperties.TabIndex = 1;
            this.comboBoxProperties.SelectedIndexChanged += new System.EventHandler(this.ComboBoxProperties_SelectedIndexChanged);
            // 
            // buttonMortgage
            // 
            this.buttonMortgage.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.buttonMortgage.Enabled = false;
            this.buttonMortgage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMortgage.Location = new System.Drawing.Point(29, 86);
            this.buttonMortgage.Name = "buttonMortgage";
            this.buttonMortgage.Size = new System.Drawing.Size(93, 46);
            this.buttonMortgage.TabIndex = 2;
            this.buttonMortgage.Text = "Mortgage";
            this.buttonMortgage.UseVisualStyleBackColor = false;
            this.buttonMortgage.Click += new System.EventHandler(this.ButtonMortgage_Click);
            // 
            // buttonLiftMortgage
            // 
            this.buttonLiftMortgage.BackColor = System.Drawing.Color.Coral;
            this.buttonLiftMortgage.Enabled = false;
            this.buttonLiftMortgage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLiftMortgage.Location = new System.Drawing.Point(145, 86);
            this.buttonLiftMortgage.Name = "buttonLiftMortgage";
            this.buttonLiftMortgage.Size = new System.Drawing.Size(93, 46);
            this.buttonLiftMortgage.TabIndex = 3;
            this.buttonLiftMortgage.Text = "Lift mortgage";
            this.buttonLiftMortgage.UseVisualStyleBackColor = false;
            this.buttonLiftMortgage.Click += new System.EventHandler(this.ButtonLiftMortgage_Click);
            // 
            // linkLabelClose
            // 
            this.linkLabelClose.AutoSize = true;
            this.linkLabelClose.Location = new System.Drawing.Point(119, 149);
            this.linkLabelClose.Name = "linkLabelClose";
            this.linkLabelClose.Size = new System.Drawing.Size(32, 13);
            this.linkLabelClose.TabIndex = 4;
            this.linkLabelClose.TabStop = true;
            this.linkLabelClose.Text = "close";
            this.linkLabelClose.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabelClose_LinkClicked);
            // 
            // MortgageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(268, 171);
            this.ControlBox = false;
            this.Controls.Add(this.linkLabelClose);
            this.Controls.Add(this.buttonLiftMortgage);
            this.Controls.Add(this.buttonMortgage);
            this.Controls.Add(this.comboBoxProperties);
            this.Controls.Add(this.labelTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MortgageForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.ComboBox comboBoxProperties;
        private System.Windows.Forms.Button buttonMortgage;
        private System.Windows.Forms.Button buttonLiftMortgage;
        private System.Windows.Forms.LinkLabel linkLabelClose;
    }
}
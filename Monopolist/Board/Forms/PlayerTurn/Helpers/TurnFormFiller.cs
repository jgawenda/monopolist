﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;

namespace Monopolist.Board.Forms.PlayerTurn.Helpers
{
    public static class TurnFormFiller
    {
        public static void FillLabels(PlayerAccount playerAccount, Label labelPlayerName, Label labelPlayerSymbol)
        {
            labelPlayerName.Text = playerAccount.Name;
            labelPlayerSymbol.Text = playerAccount.Symbol.ToString();
        }
        
    }
}

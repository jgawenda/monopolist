﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monopolist.Board.Types.Fields;
using Monopolist.Board.Types.Fields.Containers;

namespace Monopolist.Board.Forms.PlayerTurn.Helpers.Tools
{
    /// <summary>
    /// This Class is used to provide Method to get all Streets of the same color.
    /// </summary>
    public static class SameColorStreetsExtractor
    {
        public static List<FieldStreet> GetStreetsOfTheSameColor(List<FieldStreet> allStreets, FieldStreet fieldStreet)
        {
            return allStreets.Where(p => (p.StreetCardInfo.StreetColor == fieldStreet.StreetCardInfo.StreetColor)).ToList();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Helpers.UI;

namespace Monopolist.Board.Forms.PlayerTurn.Helpers
{
    public class PlayerTurnPosAdjuster : FormPositonAdjuster
    {
        public PlayerTurnPosAdjuster(Form parentForm, Form childForm) : base(parentForm, childForm)
        {

        }

        public void AdjustPosition()
        {
            var coords = _parentCoordsExtractor.GetCoords(ParentCoordsExtractor.Location.Same);

            coords.X += 100;
            coords.Y += 399;

            _childForm.Location = coords;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Forms.PlayerTurn.Helpers;
using Monopolist.Core.Types.Players;
using Monopolist.Board.Helpers.View;
using Monopolist.Core.Types.Players.Containers;
using Monopolist.Board.Types.Fields.Containers;
using Monopolist.Core.Actions.GameProcess;
using Monopolist.Board.Forms.PlayerTurn.DiceRoll;
using Monopolist.Core.Actions.GameProcess.Helpers;
using Monopolist.Board.Forms.PlayerTurn.Prison;
using Monopolist.Board.Forms.PlayerTurn.HousesOperations;
using Monopolist.Board.Forms.PlayerTurn.Mortgage;
using Monopolist.Board.Forms.PlayerTurn.Trading;
using Monopolist.Board.Forms.PlayerTurn.Rescue;

namespace Monopolist.Board.Forms.PlayerTurn
{
    public partial class PlayerTurnForm : Form
    {
        private PlayerTurnPosAdjuster _formAdjuster;

        private PlayerAccount _playerAccount;
        private AllFieldsContainer _allFieldsContainer;
        private AllPlayersContainer _allPlayersContainer;
        private PropertiesHighlighter _propHighlighter;
        private PlayerFormsUpdater _playerFormsUpdater;

        private PlayerTurnOperator _playerTurnOperator;

        // ACTIONS
        private DiceRollOperator _diceRollOperator;
        
        public PlayerTurnForm(Form parentForm, AllFieldsContainer allFieldsContainer, AllPlayersContainer allPlayersContainer, PlayerFormsUpdater playerFormsUpdater, PlayerTurnOperator playerTurnOperator)
        {
            InitializeComponent();
            
            _formAdjuster = new PlayerTurnPosAdjuster(parentForm, this);

            _allFieldsContainer = allFieldsContainer;
            _propHighlighter = new PropertiesHighlighter(_allFieldsContainer);

            _allPlayersContainer = allPlayersContainer;

            _playerFormsUpdater = playerFormsUpdater;

            _playerTurnOperator = playerTurnOperator;

            _diceRollOperator = new DiceRollOperator(_allFieldsContainer, _playerFormsUpdater);
        }
        
        private void PlayerTurnForm_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
            {
                _formAdjuster.AdjustPosition();

                if (_playerAccount.PrisonStatus.IsInPrison)
                {
                    PrisonTurnForm prisonTurnForm = new PrisonTurnForm(_playerAccount, 50);
                    var dialogResult = prisonTurnForm.ShowDialog(this);

                    if (dialogResult == DialogResult.No)
                        EndTurn();
                }
            }

            _playerFormsUpdater.UpdateInfo();
        }
        
        private void CheckBoxHighlightProperties_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxHighlightProperties.Checked)
                _propHighlighter.HighlightProperties(_playerAccount);
            else
                _propHighlighter.UndoHighlight();
        }

        public void UpdatePlayerInfo(PlayerAccount playerAccount)
        {
            _playerAccount = playerAccount;
            TurnFormFiller.FillLabels(_playerAccount, labelPlayerName, labelPlayerSymbol);
        }

        private void ButtonRollDice_Click(object sender, EventArgs e)
        {
            checkBoxHighlightProperties.Checked = false;
            _diceRollOperator.StartRolling(_playerAccount);
            _playerFormsUpdater.UpdateInfo();
            EndTurn();
        }

        private void ButtonBuySellHouse_Click(object sender, EventArgs e)
        {
            HousesForm housesBuyingForm = new HousesForm(_playerAccount, _allFieldsContainer, _playerFormsUpdater, true);
            this.Opacity = 0.5;
            housesBuyingForm.ShowDialog(this);
            this.Opacity = 1;
        }

        private void ButtonManageMortgages_Click(object sender, EventArgs e)
        {
            MortgageForm mortgageForm = new MortgageForm(_playerAccount, _allFieldsContainer, _playerFormsUpdater, true);
            this.Opacity = 0.5;
            mortgageForm.ShowDialog(this);
            this.Opacity = 1;
        }

        private void ButtonDeal_Click(object sender, EventArgs e)
        {
            TradingForm tradingForm = new TradingForm(_playerAccount, _allPlayersContainer.AllPlayers, _allFieldsContainer, _playerFormsUpdater);
            this.Opacity = 0.5;
            tradingForm.ShowDialog(this);
            this.Opacity = 1;
        }

        private void EndTurn()
        {
            Hide();

            if (_playerAccount.CashAccount.Cash < 0)
            {
                RescueForm rescueForm = new RescueForm(_playerAccount, _allFieldsContainer, _allPlayersContainer, _playerFormsUpdater);
                rescueForm.ShowDialog(this);
            }

            MessageBox.Show("End of turn.");
            _playerTurnOperator.Start();
        }
    }
}

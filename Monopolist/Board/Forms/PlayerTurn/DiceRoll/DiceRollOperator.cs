﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;
using Monopolist.Core.Actions.GameProcess.Helpers;
using Monopolist.Board.Types.Fields.Containers;
using Monopolist.Board.Forms.PlayerTurn.DiceRoll.Helpers;
using Monopolist.Board.Forms.PlayerTurn.DiceRoll.Helpers.Types;

namespace Monopolist.Board.Forms.PlayerTurn.DiceRoll
{
    public class DiceRollOperator
    {
        private DiceRollCalculator _diceRollCalculator;
        private MovingOperator _movingOperator;
        
        public DiceRollOperator(AllFieldsContainer allFieldsContainer, PlayerFormsUpdater playerFormsUpdater)
        {
            _diceRollCalculator = new DiceRollCalculator();
            _movingOperator = new MovingOperator(allFieldsContainer, playerFormsUpdater);
        }
        
        private int RollDices(SpeedingInspector speedingInspector)
        {
            int totalResult = 0;
            DiceResult diceResult;

            do
            {
                speedingInspector.AddOneRoll();

                if (speedingInspector.IsPlayerSpeeding())
                    break;

                diceResult = _diceRollCalculator.RollDices(1, 6);
                MessageBox.Show(String.Format("{0} & {1}\nDoubles? {2}", diceResult.Dice1, diceResult.Dice2, diceResult.Double));
                totalResult += diceResult.Sum;
            } while (diceResult.Double);

            return totalResult;
        }

        public void StartRolling(PlayerAccount playerAccount)
        {
            SpeedingInspector speedingInspector = new SpeedingInspector(3);
            int fieldsToMove = RollDices(speedingInspector);
            playerAccount.ChangeLastDiceResult(fieldsToMove);

            if (!speedingInspector.IsPlayerSpeeding())
                _movingOperator.MovePlayer(playerAccount, fieldsToMove);
            else
            {
                MessageBox.Show("You had doubles too many times in a row!\nYou go to prison for speeding.");
                _movingOperator.LockInPrison(playerAccount);
            }

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopolist.Board.Forms.PlayerTurn.DiceRoll.Helpers
{
    public class SpeedingInspector
    {
        private int _maxRolls;
        private int _rollsCounter;

        public SpeedingInspector(int maxRolls)
        {
            _maxRolls = maxRolls;
            _rollsCounter = 0;
        }

        public void AddOneRoll()
        {
            _rollsCounter++;
        }

        public bool IsPlayerSpeeding()
        {
            if (_rollsCounter > _maxRolls)
                return true;
            else
                return false;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopolist.Board.Forms.PlayerTurn.DiceRoll.Helpers.Types
{
    public class DiceResult
    {
        public int Dice1 { get; }
        public int Dice2 { get; }

        public int Sum { get; }

        public bool Double { get; }

        public DiceResult(int dice1, int dice2)
        {
            Dice1 = dice1;
            Dice2 = dice2;

            Sum = dice1 + dice2;

            Double = IsItDouble();
        }

        private bool IsItDouble()
        {
            if (Dice1 == Dice2)
                return true;
            else
                return false;
        }

    }
}

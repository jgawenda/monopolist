﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;
using Monopolist.Core.Actions.GameProcess.Helpers;
using Monopolist.Board.Types.Fields.Containers;
using Monopolist.Board.Forms.PlayerTurn.DiceRoll.Helpers.Moving;

namespace Monopolist.Board.Forms.PlayerTurn.DiceRoll.Helpers
{
    public class MovingOperator
    {
        private AllFieldsContainer _allFieldsContainer;
        private PlayerFormsUpdater _playerFormsUpdater;

        public MovingOperator(AllFieldsContainer allFieldsContainer, PlayerFormsUpdater playerFormsUpdater)
        {
            _allFieldsContainer = allFieldsContainer;
            _playerFormsUpdater = playerFormsUpdater;
        }

        private void PayoffSalaryAndRefresh(PlayerAccount playerAccount, int salaryAmount)
        {
            SalaryPayoffOperator.PayoffSalary(playerAccount, salaryAmount);
            _playerFormsUpdater.UpdateInfo();
            MessageBox.Show(String.Format("{0}{1} collected ${2} salary.", playerAccount.Symbol, playerAccount.Name, salaryAmount), "Salary");
        }
        
        public void MovePlayer(PlayerAccount playerAccount, int howManyField)
        {
            int currentIndex = _allFieldsContainer.AllFields.IndexOf(playerAccount.FieldOn);

            if (currentIndex == 0)
                PayoffSalaryAndRefresh(playerAccount, 200);

            IndexGenerator indexGenerator = new IndexGenerator(currentIndex, _allFieldsContainer.AllFields.Count - 1);

            _allFieldsContainer.AllFields.ElementAt(currentIndex).RemoveVisitor(playerAccount);

            int tempIndex = 0;

            for (int i=0; i<howManyField-1; i++)
            {
                tempIndex = indexGenerator.GetNextIndex();
                
                _allFieldsContainer.AllFields.ElementAt(tempIndex).SimulateStepOnOff(500);

                if (tempIndex == 0)
                    PayoffSalaryAndRefresh(playerAccount, 200);
            }

            _allFieldsContainer.AllFields.ElementAt(indexGenerator.GetNextIndex()).AddVisitor(playerAccount);
        }

        public void LockInPrison(PlayerAccount playerAccount)
        {
            int currentIndex = _allFieldsContainer.AllFields.IndexOf(playerAccount.FieldOn);

            _allFieldsContainer.AllFields.ElementAt(currentIndex).RemoveVisitor(playerAccount);

            _allFieldsContainer.Prison.LockInPrison(playerAccount);
        }

    }
}

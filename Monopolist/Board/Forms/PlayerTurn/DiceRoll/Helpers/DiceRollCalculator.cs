﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monopolist.Board.Forms.PlayerTurn.DiceRoll.Helpers.Types;

namespace Monopolist.Board.Forms.PlayerTurn.DiceRoll.Helpers
{
    public class DiceRollCalculator
    {
        private Random _random;

        public DiceRollCalculator()
        {
            _random = new Random();
        }

        private int RollDice(int minimum, int maximum)
        {
            return _random.Next(minimum, maximum + 1);
        }

        public DiceResult RollDices(int minimum, int maximum)
        {
            return new DiceResult(RollDice(minimum, maximum), RollDice(minimum, maximum));
        }

    }
}

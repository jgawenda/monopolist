﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopolist.Board.Forms.PlayerTurn.DiceRoll.Helpers.Moving
{
    public class IndexGenerator
    {
        private int _startingIndex;
        private int _maxIndex;

        private int _currentIndex;

        public IndexGenerator(int startingIndex, int maxIndex)
        {
            _startingIndex = startingIndex;
            _maxIndex = maxIndex;
            _currentIndex = startingIndex;
        }

        public int GetNextIndex()
        {
            if (_currentIndex < _maxIndex)
                _currentIndex++;
            else
                _currentIndex = 0;

            return _currentIndex;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monopolist.Core.Types.Players;

namespace Monopolist.Board.Forms.PlayerTurn.DiceRoll.Helpers.Moving
{
    public static class SalaryPayoffOperator
    {
        public static void PayoffSalary(PlayerAccount playerAccount, int salaryAmount)
        {
            playerAccount.CashAccount.AddCash(salaryAmount);
        }

    }
}

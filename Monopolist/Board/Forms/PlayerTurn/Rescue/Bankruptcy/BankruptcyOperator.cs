﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;
using Monopolist.Board.Types.Fields;

namespace Monopolist.Board.Forms.PlayerTurn.Rescue.Bankruptcy
{
    public class BankruptcyOperator
    {
        private List<BuyableField> _allBuyableFields;
        private PlayerAccount _bankAccount;

        public BankruptcyOperator(List<BuyableField> allBuyableFields, PlayerAccount bankAccount)
        {
            _allBuyableFields = allBuyableFields;
            _bankAccount = bankAccount;
        }
        
        private void RemoveBuyablesOwnerAndBuildings(PlayerAccount playerAccount)
        {
            foreach (BuyableField field in _allBuyableFields)
            {
                if (field.Owner == playerAccount)
                {
                    field.ChangeOwner(_bankAccount);

                    if (field is FieldStreet fieldStreet)
                        fieldStreet.Buildings.RemoveAllBuildings();
                }
            }
        }

        private void RemovePlayerFromBoard(PlayerAccount playerAccount)
        {
            playerAccount.FieldOn.RemoveVisitor(playerAccount);
        }

        private void ShowSummary(PlayerAccount playerAccount)
        {
            string message = String.Format("The game is over for {0}.\nYour final balance is: ${1}.\n\nGG and thank you for playing!", playerAccount.Name, playerAccount.CashAccount.Cash);
            MessageBox.Show(message, "Game over", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void DeclareBankruptcy(PlayerAccount playerAccount)
        {
            RemoveBuyablesOwnerAndBuildings(playerAccount);
            RemovePlayerFromBoard(playerAccount);
            playerAccount.ChangeBankruptStatus(true);
            ShowSummary(playerAccount);
        }

    }
}

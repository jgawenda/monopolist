﻿namespace Monopolist.Board.Forms.PlayerTurn.Rescue
{
    partial class RescueForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RescueForm));
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelPossibilities = new System.Windows.Forms.Label();
            this.labelBankruptcy = new System.Windows.Forms.Label();
            this.labelContinueCondition = new System.Windows.Forms.Label();
            this.buttonSellBuildings = new System.Windows.Forms.Button();
            this.buttonMakeMortgage = new System.Windows.Forms.Button();
            this.buttonTrade = new System.Windows.Forms.Button();
            this.buttonBankruptcy = new System.Windows.Forms.Button();
            this.labelCashBalanceTitle = new System.Windows.Forms.Label();
            this.buttonContinueGame = new System.Windows.Forms.Button();
            this.labelCash = new System.Windows.Forms.Label();
            this.labelCashDollarSign = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTitle.Location = new System.Drawing.Point(62, 15);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(367, 16);
            this.labelTitle.TabIndex = 0;
            this.labelTitle.Text = "You don\'t have enough money to proceed the game.";
            // 
            // labelPossibilities
            // 
            this.labelPossibilities.AutoSize = true;
            this.labelPossibilities.Location = new System.Drawing.Point(12, 53);
            this.labelPossibilities.Name = "labelPossibilities";
            this.labelPossibilities.Size = new System.Drawing.Size(298, 65);
            this.labelPossibilities.TabIndex = 1;
            this.labelPossibilities.Text = "There are some actions you can now perform to make money:\r\n- sell buildings,\r\n- m" +
    "ortgage properties,\r\n- sell properties,\r\n- trade with other Player.";
            // 
            // labelBankruptcy
            // 
            this.labelBankruptcy.AutoSize = true;
            this.labelBankruptcy.Location = new System.Drawing.Point(12, 131);
            this.labelBankruptcy.Name = "labelBankruptcy";
            this.labelBankruptcy.Size = new System.Drawing.Size(379, 13);
            this.labelBankruptcy.TabIndex = 2;
            this.labelBankruptcy.Text = "You can also give up and declare bankruptcy (you will be removed from game).";
            // 
            // labelContinueCondition
            // 
            this.labelContinueCondition.AutoSize = true;
            this.labelContinueCondition.ForeColor = System.Drawing.Color.Firebrick;
            this.labelContinueCondition.Location = new System.Drawing.Point(59, 288);
            this.labelContinueCondition.Name = "labelContinueCondition";
            this.labelContinueCondition.Size = new System.Drawing.Size(372, 13);
            this.labelContinueCondition.TabIndex = 3;
            this.labelContinueCondition.Text = "The game can be continued only if your cash balance is greater or equal to 0.";
            // 
            // buttonSellBuildings
            // 
            this.buttonSellBuildings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSellBuildings.Location = new System.Drawing.Point(15, 173);
            this.buttonSellBuildings.Name = "buttonSellBuildings";
            this.buttonSellBuildings.Size = new System.Drawing.Size(110, 23);
            this.buttonSellBuildings.TabIndex = 4;
            this.buttonSellBuildings.Text = "Sell buildings";
            this.buttonSellBuildings.UseVisualStyleBackColor = true;
            this.buttonSellBuildings.Click += new System.EventHandler(this.ButtonSellBuildings_Click);
            // 
            // buttonMakeMortgage
            // 
            this.buttonMakeMortgage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMakeMortgage.Location = new System.Drawing.Point(131, 173);
            this.buttonMakeMortgage.Name = "buttonMakeMortgage";
            this.buttonMakeMortgage.Size = new System.Drawing.Size(110, 23);
            this.buttonMakeMortgage.TabIndex = 5;
            this.buttonMakeMortgage.Text = "Make mortgage";
            this.buttonMakeMortgage.UseVisualStyleBackColor = true;
            this.buttonMakeMortgage.Click += new System.EventHandler(this.ButtonMakeMortgage_Click);
            // 
            // buttonTrade
            // 
            this.buttonTrade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTrade.Location = new System.Drawing.Point(247, 173);
            this.buttonTrade.Name = "buttonTrade";
            this.buttonTrade.Size = new System.Drawing.Size(110, 23);
            this.buttonTrade.TabIndex = 6;
            this.buttonTrade.Text = "Trade with...";
            this.buttonTrade.UseVisualStyleBackColor = true;
            this.buttonTrade.Click += new System.EventHandler(this.ButtonTrade_Click);
            // 
            // buttonBankruptcy
            // 
            this.buttonBankruptcy.BackColor = System.Drawing.Color.Coral;
            this.buttonBankruptcy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBankruptcy.Location = new System.Drawing.Point(363, 173);
            this.buttonBankruptcy.Name = "buttonBankruptcy";
            this.buttonBankruptcy.Size = new System.Drawing.Size(110, 23);
            this.buttonBankruptcy.TabIndex = 7;
            this.buttonBankruptcy.Text = "Declare bankruptcy";
            this.buttonBankruptcy.UseVisualStyleBackColor = false;
            this.buttonBankruptcy.Click += new System.EventHandler(this.ButtonBankruptcy_Click);
            // 
            // labelCashBalanceTitle
            // 
            this.labelCashBalanceTitle.AutoSize = true;
            this.labelCashBalanceTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCashBalanceTitle.Location = new System.Drawing.Point(173, 217);
            this.labelCashBalanceTitle.Name = "labelCashBalanceTitle";
            this.labelCashBalanceTitle.Size = new System.Drawing.Size(145, 20);
            this.labelCashBalanceTitle.TabIndex = 8;
            this.labelCashBalanceTitle.Text = "Your cash balance:";
            // 
            // buttonContinueGame
            // 
            this.buttonContinueGame.BackColor = System.Drawing.Color.ForestGreen;
            this.buttonContinueGame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonContinueGame.Location = new System.Drawing.Point(190, 283);
            this.buttonContinueGame.Name = "buttonContinueGame";
            this.buttonContinueGame.Size = new System.Drawing.Size(110, 23);
            this.buttonContinueGame.TabIndex = 9;
            this.buttonContinueGame.Text = "Continue game";
            this.buttonContinueGame.UseVisualStyleBackColor = false;
            this.buttonContinueGame.Visible = false;
            this.buttonContinueGame.Click += new System.EventHandler(this.ButtonContinueGame_Click);
            // 
            // labelCash
            // 
            this.labelCash.AutoSize = true;
            this.labelCash.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCash.ForeColor = System.Drawing.Color.Firebrick;
            this.labelCash.Location = new System.Drawing.Point(221, 239);
            this.labelCash.Name = "labelCash";
            this.labelCash.Size = new System.Drawing.Size(67, 25);
            this.labelCash.TabIndex = 10;
            this.labelCash.Text = "-2500";
            // 
            // labelCashDollarSign
            // 
            this.labelCashDollarSign.AutoSize = true;
            this.labelCashDollarSign.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCashDollarSign.Location = new System.Drawing.Point(204, 239);
            this.labelCashDollarSign.Name = "labelCashDollarSign";
            this.labelCashDollarSign.Size = new System.Drawing.Size(24, 25);
            this.labelCashDollarSign.TabIndex = 11;
            this.labelCashDollarSign.Text = "$";
            // 
            // RescueForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 328);
            this.ControlBox = false;
            this.Controls.Add(this.labelCash);
            this.Controls.Add(this.labelCashDollarSign);
            this.Controls.Add(this.buttonContinueGame);
            this.Controls.Add(this.labelCashBalanceTitle);
            this.Controls.Add(this.buttonBankruptcy);
            this.Controls.Add(this.buttonTrade);
            this.Controls.Add(this.buttonMakeMortgage);
            this.Controls.Add(this.buttonSellBuildings);
            this.Controls.Add(this.labelContinueCondition);
            this.Controls.Add(this.labelBankruptcy);
            this.Controls.Add(this.labelPossibilities);
            this.Controls.Add(this.labelTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RescueForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Label labelPossibilities;
        private System.Windows.Forms.Label labelBankruptcy;
        private System.Windows.Forms.Label labelContinueCondition;
        private System.Windows.Forms.Button buttonSellBuildings;
        private System.Windows.Forms.Button buttonMakeMortgage;
        private System.Windows.Forms.Button buttonTrade;
        private System.Windows.Forms.Button buttonBankruptcy;
        private System.Windows.Forms.Label labelCashBalanceTitle;
        private System.Windows.Forms.Button buttonContinueGame;
        private System.Windows.Forms.Label labelCash;
        private System.Windows.Forms.Label labelCashDollarSign;
    }
}
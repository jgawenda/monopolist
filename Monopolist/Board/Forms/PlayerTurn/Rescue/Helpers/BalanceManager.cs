﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using Monopolist.Core.Types.Players;

namespace Monopolist.Board.Forms.PlayerTurn.Rescue.Helpers
{
    public class BalanceManager
    {
        private Label _labelCash;
        private PlayerAccount _activePlayer;

        public BalanceManager(Label labelCash, PlayerAccount playerAccount)
        {
            _labelCash = labelCash;
            _activePlayer = playerAccount;
        }

        private void ChangeColor(bool isPositive)
        {
            if (isPositive)
                _labelCash.ForeColor = Color.ForestGreen;
            else
                _labelCash.ForeColor = Color.Firebrick;
        }

        public void UpdateLabel()
        {
            _labelCash.Text = _activePlayer.CashAccount.Cash.ToString();

            ChangeColor(CheckIfPositive());
        }
        
        public bool CheckIfPositive()
        {
            bool result = false;

            if (_activePlayer.CashAccount.Cash >= 0)
                result = true;

            return result;
        }

    }
}

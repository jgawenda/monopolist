﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;
using Monopolist.Core.Actions.GameProcess.Helpers;
using Monopolist.Board.Types.Fields.Containers;
using Monopolist.Core.Types.Players.Containers;
using Monopolist.Board.Forms.PlayerTurn.HousesOperations;
using Monopolist.Board.Forms.PlayerTurn.Mortgage;
using Monopolist.Board.Forms.PlayerTurn.Trading;
using Monopolist.Board.Forms.PlayerTurn.Rescue.Helpers;
using Monopolist.Board.Forms.PlayerTurn.Rescue.Bankruptcy;

namespace Monopolist.Board.Forms.PlayerTurn.Rescue
{
    public partial class RescueForm : Form
    {
        private PlayerAccount _playerAccount;
        private AllFieldsContainer _allFieldsContainer;
        private AllPlayersContainer _allPlayersContainer;
        private PlayerFormsUpdater _playerFormsUpdater;

        private BalanceManager _balanceManager;

        public RescueForm(PlayerAccount playerAccount, AllFieldsContainer allFieldsContainer, AllPlayersContainer allPlayersContainer, PlayerFormsUpdater playerFormsUpdater)
        {
            InitializeComponent();

            _playerAccount = playerAccount;
            _allFieldsContainer = allFieldsContainer;
            _allPlayersContainer = allPlayersContainer;
            _playerFormsUpdater = playerFormsUpdater;

            _balanceManager = new BalanceManager(labelCash, _playerAccount);
            _balanceManager.UpdateLabel();
        }

        private void ButtonSellBuildings_Click(object sender, EventArgs e)
        {
            HousesForm housesForm = new HousesForm(_playerAccount, _allFieldsContainer, _playerFormsUpdater, false);
            this.Opacity = 0.5;
            housesForm.ShowDialog(this);
            this.Opacity = 1;

            UpdateAndCheckCash();
        }

        private void ButtonMakeMortgage_Click(object sender, EventArgs e)
        {
            MortgageForm mortgageForm = new MortgageForm(_playerAccount, _allFieldsContainer, _playerFormsUpdater, false);
            this.Opacity = 0.5;
            mortgageForm.ShowDialog(this);
            this.Opacity = 1;

            UpdateAndCheckCash();
        }

        private void ButtonTrade_Click(object sender, EventArgs e)
        {
            TradingForm tradingForm = new TradingForm(_playerAccount, _allPlayersContainer.AllPlayers, _allFieldsContainer, _playerFormsUpdater);
            this.Opacity = 0.5;
            tradingForm.ShowDialog(this);
            this.Opacity = 1;

            UpdateAndCheckCash();
        }

        private void ButtonBankruptcy_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Are you sure you want to declare bankruptcy (give up)?\nThe game will be over for you.", "Declaring bankruptcy", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            
            if (result == DialogResult.Yes)
            {
                BankruptcyOperator bankruptcyOperator = new BankruptcyOperator(_allFieldsContainer.AllBuyableFields, _allPlayersContainer.BankAccount);
                bankruptcyOperator.DeclareBankruptcy(_playerAccount);
                this.Close();
            }
        }

        private void UpdateAndCheckCash()
        {
            _balanceManager.UpdateLabel();

            if (_balanceManager.CheckIfPositive())
            {
                labelContinueCondition.Visible = false;
                buttonContinueGame.Visible = true;
            }
            else
            {
                labelContinueCondition.Visible = true;
                buttonContinueGame.Visible = false;
            }
        }

        private void ButtonContinueGame_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

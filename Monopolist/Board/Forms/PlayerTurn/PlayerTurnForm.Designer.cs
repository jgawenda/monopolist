﻿namespace Monopolist.Board.Forms.PlayerTurn
{
    partial class PlayerTurnForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlayerTurnForm));
            this.labelPlayerSymbol = new System.Windows.Forms.Label();
            this.labelPlayerName = new System.Windows.Forms.Label();
            this.buttonRollDice = new System.Windows.Forms.Button();
            this.buttonBuySellHouse = new System.Windows.Forms.Button();
            this.buttonManageMortgages = new System.Windows.Forms.Button();
            this.buttonDeal = new System.Windows.Forms.Button();
            this.checkBoxHighlightProperties = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // labelPlayerSymbol
            // 
            this.labelPlayerSymbol.AutoSize = true;
            this.labelPlayerSymbol.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPlayerSymbol.Location = new System.Drawing.Point(103, 41);
            this.labelPlayerSymbol.Name = "labelPlayerSymbol";
            this.labelPlayerSymbol.Size = new System.Drawing.Size(39, 42);
            this.labelPlayerSymbol.TabIndex = 0;
            this.labelPlayerSymbol.Text = "#";
            // 
            // labelPlayerName
            // 
            this.labelPlayerName.AutoSize = true;
            this.labelPlayerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPlayerName.Location = new System.Drawing.Point(277, 9);
            this.labelPlayerName.Name = "labelPlayerName";
            this.labelPlayerName.Size = new System.Drawing.Size(52, 20);
            this.labelPlayerName.TabIndex = 1;
            this.labelPlayerName.Text = "Player";
            // 
            // buttonRollDice
            // 
            this.buttonRollDice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRollDice.Location = new System.Drawing.Point(256, 60);
            this.buttonRollDice.Name = "buttonRollDice";
            this.buttonRollDice.Size = new System.Drawing.Size(98, 23);
            this.buttonRollDice.TabIndex = 2;
            this.buttonRollDice.Text = "Roll the dice";
            this.buttonRollDice.UseVisualStyleBackColor = true;
            this.buttonRollDice.Click += new System.EventHandler(this.ButtonRollDice_Click);
            // 
            // buttonBuySellHouse
            // 
            this.buttonBuySellHouse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBuySellHouse.Location = new System.Drawing.Point(78, 194);
            this.buttonBuySellHouse.Name = "buttonBuySellHouse";
            this.buttonBuySellHouse.Size = new System.Drawing.Size(98, 23);
            this.buttonBuySellHouse.TabIndex = 3;
            this.buttonBuySellHouse.Text = "Buy or sell house";
            this.buttonBuySellHouse.UseVisualStyleBackColor = true;
            this.buttonBuySellHouse.Click += new System.EventHandler(this.ButtonBuySellHouse_Click);
            // 
            // buttonManageMortgages
            // 
            this.buttonManageMortgages.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonManageMortgages.Location = new System.Drawing.Point(256, 194);
            this.buttonManageMortgages.Name = "buttonManageMortgages";
            this.buttonManageMortgages.Size = new System.Drawing.Size(98, 46);
            this.buttonManageMortgages.TabIndex = 5;
            this.buttonManageMortgages.Text = "Manage mortgages";
            this.buttonManageMortgages.UseVisualStyleBackColor = true;
            this.buttonManageMortgages.Click += new System.EventHandler(this.ButtonManageMortgages_Click);
            // 
            // buttonDeal
            // 
            this.buttonDeal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDeal.Location = new System.Drawing.Point(429, 194);
            this.buttonDeal.Name = "buttonDeal";
            this.buttonDeal.Size = new System.Drawing.Size(98, 23);
            this.buttonDeal.TabIndex = 6;
            this.buttonDeal.Text = "Deal with...";
            this.buttonDeal.UseVisualStyleBackColor = true;
            this.buttonDeal.Click += new System.EventHandler(this.ButtonDeal_Click);
            // 
            // checkBoxHighlightProperties
            // 
            this.checkBoxHighlightProperties.AutoSize = true;
            this.checkBoxHighlightProperties.Location = new System.Drawing.Point(59, 103);
            this.checkBoxHighlightProperties.Name = "checkBoxHighlightProperties";
            this.checkBoxHighlightProperties.Size = new System.Drawing.Size(132, 17);
            this.checkBoxHighlightProperties.TabIndex = 9;
            this.checkBoxHighlightProperties.Text = "Highlight my properties";
            this.checkBoxHighlightProperties.UseVisualStyleBackColor = true;
            this.checkBoxHighlightProperties.CheckedChanged += new System.EventHandler(this.CheckBoxHighlightProperties_CheckedChanged);
            // 
            // PlayerTurnForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(598, 299);
            this.ControlBox = false;
            this.Controls.Add(this.checkBoxHighlightProperties);
            this.Controls.Add(this.buttonDeal);
            this.Controls.Add(this.buttonManageMortgages);
            this.Controls.Add(this.buttonBuySellHouse);
            this.Controls.Add(this.buttonRollDice);
            this.Controls.Add(this.labelPlayerName);
            this.Controls.Add(this.labelPlayerSymbol);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PlayerTurnForm";
            this.ShowInTaskbar = false;
            this.TopMost = true;
            this.VisibleChanged += new System.EventHandler(this.PlayerTurnForm_VisibleChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelPlayerSymbol;
        private System.Windows.Forms.Label labelPlayerName;
        private System.Windows.Forms.Button buttonRollDice;
        private System.Windows.Forms.Button buttonBuySellHouse;
        private System.Windows.Forms.Button buttonManageMortgages;
        private System.Windows.Forms.Button buttonDeal;
        private System.Windows.Forms.CheckBox checkBoxHighlightProperties;
    }
}
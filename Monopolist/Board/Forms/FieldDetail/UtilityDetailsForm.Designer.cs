﻿namespace Monopolist.Board.Forms.FieldDetail
{
    partial class UtilityDetailsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UtilityDetailsForm));
            this.labelName = new System.Windows.Forms.Label();
            this.labelDesc1 = new System.Windows.Forms.Label();
            this.labelDesc2 = new System.Windows.Forms.Label();
            this.labelMortage = new System.Windows.Forms.Label();
            this.labelMortgagePrice = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelName.Location = new System.Drawing.Point(82, 37);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(128, 24);
            this.labelName.TabIndex = 0;
            this.labelName.Text = "Water Works";
            this.labelName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelDesc1
            // 
            this.labelDesc1.AutoSize = true;
            this.labelDesc1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDesc1.Location = new System.Drawing.Point(35, 101);
            this.labelDesc1.Name = "labelDesc1";
            this.labelDesc1.Size = new System.Drawing.Size(226, 32);
            this.labelDesc1.TabIndex = 1;
            this.labelDesc1.Text = "If one Utility is owned,\r\nrent is 4 times amount shown on dice.";
            // 
            // labelDesc2
            // 
            this.labelDesc2.AutoSize = true;
            this.labelDesc2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDesc2.Location = new System.Drawing.Point(35, 162);
            this.labelDesc2.Name = "labelDesc2";
            this.labelDesc2.Size = new System.Drawing.Size(233, 32);
            this.labelDesc2.TabIndex = 2;
            this.labelDesc2.Text = "If both Utilities are owned,\r\nrent is 10 times amount shown on dice.";
            // 
            // labelMortage
            // 
            this.labelMortage.AutoSize = true;
            this.labelMortage.Location = new System.Drawing.Point(108, 237);
            this.labelMortage.Name = "labelMortage";
            this.labelMortage.Size = new System.Drawing.Size(82, 13);
            this.labelMortage.TabIndex = 3;
            this.labelMortage.Text = "Mortgage Value";
            // 
            // labelMortgagePrice
            // 
            this.labelMortgagePrice.AutoSize = true;
            this.labelMortgagePrice.Location = new System.Drawing.Point(138, 260);
            this.labelMortgagePrice.Name = "labelMortgagePrice";
            this.labelMortgagePrice.Size = new System.Drawing.Size(25, 13);
            this.labelMortgagePrice.TabIndex = 4;
            this.labelMortgagePrice.Text = "$75";
            // 
            // UtilityDetailsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(299, 297);
            this.ControlBox = false;
            this.Controls.Add(this.labelMortgagePrice);
            this.Controls.Add(this.labelMortage);
            this.Controls.Add(this.labelDesc2);
            this.Controls.Add(this.labelDesc1);
            this.Controls.Add(this.labelName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UtilityDetailsForm";
            this.ShowInTaskbar = false;
            this.TopMost = true;
            this.VisibleChanged += new System.EventHandler(this.UtilityDetailsForm_VisibleChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelDesc1;
        private System.Windows.Forms.Label labelDesc2;
        private System.Windows.Forms.Label labelMortage;
        private System.Windows.Forms.Label labelMortgagePrice;
    }
}
﻿namespace Monopolist.Board.Forms.FieldDetail
{
    partial class StationDetailsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StationDetailsForm));
            this.labelName = new System.Windows.Forms.Label();
            this.labelRent = new System.Windows.Forms.Label();
            this.labelRentPrice1 = new System.Windows.Forms.Label();
            this.labelRent2 = new System.Windows.Forms.Label();
            this.labelRent3 = new System.Windows.Forms.Label();
            this.labelRent4 = new System.Windows.Forms.Label();
            this.labelRentPrice2 = new System.Windows.Forms.Label();
            this.labelRentPrice3 = new System.Windows.Forms.Label();
            this.labelRentPrice4 = new System.Windows.Forms.Label();
            this.labelMortgage = new System.Windows.Forms.Label();
            this.labelMortgagePrice = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelName.Location = new System.Drawing.Point(56, 32);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(185, 24);
            this.labelName.TabIndex = 0;
            this.labelName.Text = "Dworzec Centralny";
            this.labelName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelRent
            // 
            this.labelRent.AutoSize = true;
            this.labelRent.Location = new System.Drawing.Point(42, 105);
            this.labelRent.Name = "labelRent";
            this.labelRent.Size = new System.Drawing.Size(30, 13);
            this.labelRent.TabIndex = 1;
            this.labelRent.Text = "Rent";
            // 
            // labelRentPrice1
            // 
            this.labelRentPrice1.AutoSize = true;
            this.labelRentPrice1.Location = new System.Drawing.Point(229, 105);
            this.labelRentPrice1.Name = "labelRentPrice1";
            this.labelRentPrice1.Size = new System.Drawing.Size(25, 13);
            this.labelRentPrice1.TabIndex = 2;
            this.labelRentPrice1.Text = "$25";
            this.labelRentPrice1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelRent2
            // 
            this.labelRent2.AutoSize = true;
            this.labelRent2.Location = new System.Drawing.Point(42, 133);
            this.labelRent2.Name = "labelRent2";
            this.labelRent2.Size = new System.Drawing.Size(87, 13);
            this.labelRent2.TabIndex = 3;
            this.labelRent2.Text = "2 stations owned";
            // 
            // labelRent3
            // 
            this.labelRent3.AutoSize = true;
            this.labelRent3.Location = new System.Drawing.Point(42, 161);
            this.labelRent3.Name = "labelRent3";
            this.labelRent3.Size = new System.Drawing.Size(87, 13);
            this.labelRent3.TabIndex = 4;
            this.labelRent3.Text = "3 stations owned";
            // 
            // labelRent4
            // 
            this.labelRent4.AutoSize = true;
            this.labelRent4.Location = new System.Drawing.Point(42, 189);
            this.labelRent4.Name = "labelRent4";
            this.labelRent4.Size = new System.Drawing.Size(87, 13);
            this.labelRent4.TabIndex = 5;
            this.labelRent4.Text = "4 stations owned";
            // 
            // labelRentPrice2
            // 
            this.labelRentPrice2.AutoSize = true;
            this.labelRentPrice2.Location = new System.Drawing.Point(229, 133);
            this.labelRentPrice2.Name = "labelRentPrice2";
            this.labelRentPrice2.Size = new System.Drawing.Size(25, 13);
            this.labelRentPrice2.TabIndex = 6;
            this.labelRentPrice2.Text = "$50";
            this.labelRentPrice2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelRentPrice3
            // 
            this.labelRentPrice3.AutoSize = true;
            this.labelRentPrice3.Location = new System.Drawing.Point(223, 161);
            this.labelRentPrice3.Name = "labelRentPrice3";
            this.labelRentPrice3.Size = new System.Drawing.Size(31, 13);
            this.labelRentPrice3.TabIndex = 7;
            this.labelRentPrice3.Text = "$100";
            this.labelRentPrice3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelRentPrice4
            // 
            this.labelRentPrice4.AutoSize = true;
            this.labelRentPrice4.Location = new System.Drawing.Point(223, 189);
            this.labelRentPrice4.Name = "labelRentPrice4";
            this.labelRentPrice4.Size = new System.Drawing.Size(31, 13);
            this.labelRentPrice4.TabIndex = 8;
            this.labelRentPrice4.Text = "$200";
            this.labelRentPrice4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelMortgage
            // 
            this.labelMortgage.AutoSize = true;
            this.labelMortgage.Location = new System.Drawing.Point(108, 239);
            this.labelMortgage.Name = "labelMortgage";
            this.labelMortgage.Size = new System.Drawing.Size(82, 13);
            this.labelMortgage.TabIndex = 9;
            this.labelMortgage.Text = "Mortgage Value";
            // 
            // labelMortgagePrice
            // 
            this.labelMortgagePrice.AutoSize = true;
            this.labelMortgagePrice.Location = new System.Drawing.Point(133, 266);
            this.labelMortgagePrice.Name = "labelMortgagePrice";
            this.labelMortgagePrice.Size = new System.Drawing.Size(31, 13);
            this.labelMortgagePrice.TabIndex = 10;
            this.labelMortgagePrice.Text = "$100";
            // 
            // StationDetailsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(299, 297);
            this.ControlBox = false;
            this.Controls.Add(this.labelMortgagePrice);
            this.Controls.Add(this.labelMortgage);
            this.Controls.Add(this.labelRentPrice4);
            this.Controls.Add(this.labelRentPrice3);
            this.Controls.Add(this.labelRentPrice2);
            this.Controls.Add(this.labelRent4);
            this.Controls.Add(this.labelRent3);
            this.Controls.Add(this.labelRent2);
            this.Controls.Add(this.labelRentPrice1);
            this.Controls.Add(this.labelRent);
            this.Controls.Add(this.labelName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StationDetailsForm";
            this.ShowInTaskbar = false;
            this.TopMost = true;
            this.VisibleChanged += new System.EventHandler(this.StationDetailsForm_VisibleChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelRent;
        private System.Windows.Forms.Label labelRentPrice1;
        private System.Windows.Forms.Label labelRent2;
        private System.Windows.Forms.Label labelRent3;
        private System.Windows.Forms.Label labelRent4;
        private System.Windows.Forms.Label labelRentPrice2;
        private System.Windows.Forms.Label labelRentPrice3;
        private System.Windows.Forms.Label labelRentPrice4;
        private System.Windows.Forms.Label labelMortgage;
        private System.Windows.Forms.Label labelMortgagePrice;
    }
}
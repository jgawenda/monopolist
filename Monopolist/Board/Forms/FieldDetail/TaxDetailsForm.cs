﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Forms.FieldDetail.Helpers;
using Monopolist.Board.Forms.FieldDetail.Tax.Helpers;
using Monopolist.Board.Types.Fields.Tax;

namespace Monopolist.Board.Forms.FieldDetail
{
    public partial class TaxDetailsForm : DetailsForm
    {
        private TaxFormFiller _formFiller;

        public TaxDetailsForm(Form parentForm)
        {
            InitializeComponent();
            PrepareFiller();
            _positionAdjuster = new CardDetailsPosAdjuster(parentForm, this);
        }

        private void PrepareFiller()
        {
            _formFiller = new TaxFormFiller(labelName, labelPrice);
        }
        
        public void UpdateInfo(TaxCardInfo taxCardInfo)
        {
            _formFiller.FillForm(taxCardInfo);
        }

        private void TaxDetailsForm_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
                _positionAdjuster.AdjustPosition();
        }

    }
}

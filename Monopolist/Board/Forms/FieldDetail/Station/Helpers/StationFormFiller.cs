﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Types.Fields;

namespace Monopolist.Board.Forms.FieldDetail.Station.Helpers
{
    /// <summary>
    /// Class can be used with forms that are having: Name, Mortgage Value.
    /// </summary>
    public class StationFormFiller
    {
        private Label _labelName;
        private Label _labelMortgageValue;

        public StationFormFiller(Label labelName, Label labelMortgageValue)
        {
            _labelName = labelName;
            _labelMortgageValue = labelMortgageValue;
        }

        public void FillForm(BuyableCardInfo stationCardInfo)
        {
            _labelName.Text = stationCardInfo.Name;
            _labelMortgageValue.Text = stationCardInfo.MortgageValue.ToString();
        }
        
    }
}

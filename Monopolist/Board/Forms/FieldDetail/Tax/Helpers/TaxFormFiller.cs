﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Types.Fields.Tax;

namespace Monopolist.Board.Forms.FieldDetail.Tax.Helpers
{
    public class TaxFormFiller
    {
        private Label _labelName;
        private Label _labelValue;

        public TaxFormFiller(Label labelName, Label labelValue)
        {
            _labelName = labelName;
            _labelValue = labelValue;
        }

        public void FillForm(TaxCardInfo taxCardInfo)
        {
            _labelName.Text = taxCardInfo.Name;
            _labelValue.Text = taxCardInfo.TaxValue.ToString();
        }

    }
}

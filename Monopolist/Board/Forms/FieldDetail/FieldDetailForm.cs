﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Forms.FieldDetail.Field.Helpers;
using Monopolist.Board.Forms.FieldDetail.Helpers;
using Monopolist.Board.Types.Fields.Normal;

namespace Monopolist.Board.Forms.FieldDetail
{
    public partial class FieldDetailForm : DetailsForm
    {
        private FieldFormFiller _formFiller;

        public FieldDetailForm(Form parentForm)
        {
            InitializeComponent();
            InitializeFormFiller();
            _positionAdjuster = new CardDetailsPosAdjuster(parentForm, this);
        }
        
        private void InitializeFormFiller()
        {
            _formFiller = new FieldFormFiller(textBoxName, richTextBoxDescription);
        }
        
        public void UpdateFieldInfo(ExtendedCardInfo extendedCardInfo)
        {
            _formFiller.FillForm(extendedCardInfo);
        }

        private void FieldDetailForm_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
                _positionAdjuster.AdjustPosition();

        }

    }
}

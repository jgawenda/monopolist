﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Types.Fields.Street;
using Monopolist.Board.Helpers.FieldDetail;

namespace Monopolist.Board.Forms.FieldDetail.Street.Helpers
{
    public class StreetFormFiller
    {
        private TextBox _streetName;
        private List<Label> _rentPriceLabels;
        private Label _rentHotelPrice;
        private Label _mortgagePrice;
        private Label _housePrice;

        public StreetFormFiller(TextBox streetNameBox, List<Label> rentPriceLabels, Label hotelPriceLabel, Label mortgagePriceLabel, Label housePriceLabel)
        {
            _streetName = streetNameBox;
            _rentPriceLabels = rentPriceLabels;
            _rentHotelPrice = hotelPriceLabel;
            _mortgagePrice = mortgagePriceLabel;
            _housePrice = housePriceLabel;
        }

        private List<int> ExtractPrices(StreetCardInfo cardInfo)
        {
            List<int> rentPrices = new List<int>() { cardInfo.RentPrice0, cardInfo.RentPrice1, cardInfo.RentPrice2, cardInfo.RentPrice3, cardInfo.RentPrice4 };

            return rentPrices;
        }

        private void FillPrices(List<int> pricesInStrings)
        {
            for (int i = 0; i < pricesInStrings.Count; i++)
            {
                _rentPriceLabels.ElementAt(i).Text = TextFormatter.GetPrice(pricesInStrings.ElementAt(i));
            }
        }
        
        public void FillForm(StreetCardInfo cardInfo)
        {
            _streetName.BackColor = cardInfo.StreetColor;
            _streetName.Text = cardInfo.Name;
            FillPrices(ExtractPrices(cardInfo));
            _rentHotelPrice.Text = TextFormatter.GetPrice(cardInfo.RentPriceHotel);
            _mortgagePrice.Text = TextFormatter.GetPrice(cardInfo.MortgageValue);
            _housePrice.Text = TextFormatter.GetPrice(cardInfo.OneHousePrice);
        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Forms.FieldDetail.Helpers;
using Monopolist.Board.Forms.FieldDetail.Field.Helpers;
using Monopolist.Board.Types.Fields;

namespace Monopolist.Board.Forms.FieldDetail
{
    public partial class ChanceDetailsForm : DetailsForm
    {
        private FieldFormFiller _formFiller;

        public ChanceDetailsForm(Form parentForm)
        {
            InitializeComponent();
            PrepareFiller();
            _positionAdjuster = new CardDetailsPosAdjuster(parentForm, this);
        }

        private void PrepareFiller()
        {
            _formFiller = new FieldFormFiller(textBoxName, richTextBoxDescription);
        }
        
        public void UpdateInfo(CardInfo cardInfo)
        {
            _formFiller.FillForm(cardInfo);
        }

        private void ChanceDetailsForm_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
                _positionAdjuster.AdjustPosition();
        }

    }
}

﻿namespace Monopolist.Board.Forms.FieldDetail
{
    partial class StreetDetailsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StreetDetailsForm));
            this.textBoxStreetName = new System.Windows.Forms.TextBox();
            this.groupBoxRent = new System.Windows.Forms.GroupBox();
            this.labelRentHotelPrice = new System.Windows.Forms.Label();
            this.labelRent4Price = new System.Windows.Forms.Label();
            this.labelRent3Price = new System.Windows.Forms.Label();
            this.labelRent2Price = new System.Windows.Forms.Label();
            this.labelRent1Price = new System.Windows.Forms.Label();
            this.labelRent0Price = new System.Windows.Forms.Label();
            this.labelRentHotel = new System.Windows.Forms.Label();
            this.labelRent4 = new System.Windows.Forms.Label();
            this.labelRent3 = new System.Windows.Forms.Label();
            this.labelRent2 = new System.Windows.Forms.Label();
            this.labelRent1 = new System.Windows.Forms.Label();
            this.labelRent0 = new System.Windows.Forms.Label();
            this.labelMortgage = new System.Windows.Forms.Label();
            this.labelHouse = new System.Windows.Forms.Label();
            this.labelMortgagePrice = new System.Windows.Forms.Label();
            this.labelHousePrice = new System.Windows.Forms.Label();
            this.groupBoxRent.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxStreetName
            // 
            this.textBoxStreetName.BackColor = System.Drawing.Color.MediumTurquoise;
            this.textBoxStreetName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxStreetName.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBoxStreetName.Enabled = false;
            this.textBoxStreetName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxStreetName.Location = new System.Drawing.Point(0, 0);
            this.textBoxStreetName.Name = "textBoxStreetName";
            this.textBoxStreetName.Size = new System.Drawing.Size(299, 29);
            this.textBoxStreetName.TabIndex = 0;
            this.textBoxStreetName.Text = "Ulica Radzymińska";
            this.textBoxStreetName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBoxRent
            // 
            this.groupBoxRent.Controls.Add(this.labelRentHotelPrice);
            this.groupBoxRent.Controls.Add(this.labelRent4Price);
            this.groupBoxRent.Controls.Add(this.labelRent3Price);
            this.groupBoxRent.Controls.Add(this.labelRent2Price);
            this.groupBoxRent.Controls.Add(this.labelRent1Price);
            this.groupBoxRent.Controls.Add(this.labelRent0Price);
            this.groupBoxRent.Controls.Add(this.labelRentHotel);
            this.groupBoxRent.Controls.Add(this.labelRent4);
            this.groupBoxRent.Controls.Add(this.labelRent3);
            this.groupBoxRent.Controls.Add(this.labelRent2);
            this.groupBoxRent.Controls.Add(this.labelRent1);
            this.groupBoxRent.Controls.Add(this.labelRent0);
            this.groupBoxRent.Location = new System.Drawing.Point(15, 46);
            this.groupBoxRent.Name = "groupBoxRent";
            this.groupBoxRent.Size = new System.Drawing.Size(268, 167);
            this.groupBoxRent.TabIndex = 1;
            this.groupBoxRent.TabStop = false;
            this.groupBoxRent.Text = "Rent";
            // 
            // labelRentHotelPrice
            // 
            this.labelRentHotelPrice.AutoSize = true;
            this.labelRentHotelPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelRentHotelPrice.Location = new System.Drawing.Point(224, 139);
            this.labelRentHotelPrice.Name = "labelRentHotelPrice";
            this.labelRentHotelPrice.Size = new System.Drawing.Size(35, 13);
            this.labelRentHotelPrice.TabIndex = 15;
            this.labelRentHotelPrice.Text = "$500";
            this.labelRentHotelPrice.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelRent4Price
            // 
            this.labelRent4Price.AutoSize = true;
            this.labelRent4Price.Location = new System.Drawing.Point(224, 117);
            this.labelRent4Price.Name = "labelRent4Price";
            this.labelRent4Price.Size = new System.Drawing.Size(31, 13);
            this.labelRent4Price.TabIndex = 14;
            this.labelRent4Price.Text = "$500";
            this.labelRent4Price.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelRent3Price
            // 
            this.labelRent3Price.AutoSize = true;
            this.labelRent3Price.Location = new System.Drawing.Point(224, 95);
            this.labelRent3Price.Name = "labelRent3Price";
            this.labelRent3Price.Size = new System.Drawing.Size(31, 13);
            this.labelRent3Price.TabIndex = 13;
            this.labelRent3Price.Text = "$500";
            this.labelRent3Price.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelRent2Price
            // 
            this.labelRent2Price.AutoSize = true;
            this.labelRent2Price.Location = new System.Drawing.Point(224, 73);
            this.labelRent2Price.Name = "labelRent2Price";
            this.labelRent2Price.Size = new System.Drawing.Size(31, 13);
            this.labelRent2Price.TabIndex = 12;
            this.labelRent2Price.Text = "$500";
            this.labelRent2Price.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelRent1Price
            // 
            this.labelRent1Price.AutoSize = true;
            this.labelRent1Price.Location = new System.Drawing.Point(224, 51);
            this.labelRent1Price.Name = "labelRent1Price";
            this.labelRent1Price.Size = new System.Drawing.Size(31, 13);
            this.labelRent1Price.TabIndex = 11;
            this.labelRent1Price.Text = "$500";
            this.labelRent1Price.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelRent0Price
            // 
            this.labelRent0Price.AutoSize = true;
            this.labelRent0Price.Location = new System.Drawing.Point(224, 29);
            this.labelRent0Price.Name = "labelRent0Price";
            this.labelRent0Price.Size = new System.Drawing.Size(31, 13);
            this.labelRent0Price.TabIndex = 10;
            this.labelRent0Price.Text = "$500";
            this.labelRent0Price.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelRentHotel
            // 
            this.labelRentHotel.AutoSize = true;
            this.labelRentHotel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelRentHotel.Location = new System.Drawing.Point(15, 139);
            this.labelRentHotel.Name = "labelRentHotel";
            this.labelRentHotel.Size = new System.Drawing.Size(82, 13);
            this.labelRentHotel.TabIndex = 5;
            this.labelRentHotel.Text = "With HOTEL:";
            // 
            // labelRent4
            // 
            this.labelRent4.AutoSize = true;
            this.labelRent4.Location = new System.Drawing.Point(15, 117);
            this.labelRent4.Name = "labelRent4";
            this.labelRent4.Size = new System.Drawing.Size(78, 13);
            this.labelRent4.TabIndex = 4;
            this.labelRent4.Text = "With 4 houses:";
            // 
            // labelRent3
            // 
            this.labelRent3.AutoSize = true;
            this.labelRent3.Location = new System.Drawing.Point(15, 95);
            this.labelRent3.Name = "labelRent3";
            this.labelRent3.Size = new System.Drawing.Size(78, 13);
            this.labelRent3.TabIndex = 3;
            this.labelRent3.Text = "With 3 houses:";
            // 
            // labelRent2
            // 
            this.labelRent2.AutoSize = true;
            this.labelRent2.Location = new System.Drawing.Point(15, 73);
            this.labelRent2.Name = "labelRent2";
            this.labelRent2.Size = new System.Drawing.Size(78, 13);
            this.labelRent2.TabIndex = 2;
            this.labelRent2.Text = "With 2 houses:";
            // 
            // labelRent1
            // 
            this.labelRent1.AutoSize = true;
            this.labelRent1.Location = new System.Drawing.Point(15, 51);
            this.labelRent1.Name = "labelRent1";
            this.labelRent1.Size = new System.Drawing.Size(75, 13);
            this.labelRent1.TabIndex = 1;
            this.labelRent1.Text = "With 1 House:";
            // 
            // labelRent0
            // 
            this.labelRent0.AutoSize = true;
            this.labelRent0.Location = new System.Drawing.Point(15, 29);
            this.labelRent0.Name = "labelRent0";
            this.labelRent0.Size = new System.Drawing.Size(57, 13);
            this.labelRent0.TabIndex = 0;
            this.labelRent0.Text = "Basic rent:";
            // 
            // labelMortgage
            // 
            this.labelMortgage.AutoSize = true;
            this.labelMortgage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMortgage.Location = new System.Drawing.Point(45, 237);
            this.labelMortgage.Name = "labelMortgage";
            this.labelMortgage.Size = new System.Drawing.Size(96, 13);
            this.labelMortgage.TabIndex = 6;
            this.labelMortgage.Text = "Mortgage Value";
            // 
            // labelHouse
            // 
            this.labelHouse.AutoSize = true;
            this.labelHouse.Location = new System.Drawing.Point(194, 237);
            this.labelHouse.Name = "labelHouse";
            this.labelHouse.Size = new System.Drawing.Size(62, 13);
            this.labelHouse.TabIndex = 7;
            this.labelHouse.Text = "House Cost";
            // 
            // labelMortgagePrice
            // 
            this.labelMortgagePrice.AutoSize = true;
            this.labelMortgagePrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMortgagePrice.Location = new System.Drawing.Point(45, 258);
            this.labelMortgagePrice.Name = "labelMortgagePrice";
            this.labelMortgagePrice.Size = new System.Drawing.Size(35, 13);
            this.labelMortgagePrice.TabIndex = 8;
            this.labelMortgagePrice.Text = "$500";
            // 
            // labelHousePrice
            // 
            this.labelHousePrice.AutoSize = true;
            this.labelHousePrice.Location = new System.Drawing.Point(194, 258);
            this.labelHousePrice.Name = "labelHousePrice";
            this.labelHousePrice.Size = new System.Drawing.Size(31, 13);
            this.labelHousePrice.TabIndex = 9;
            this.labelHousePrice.Text = "$150";
            // 
            // StreetDetailsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(299, 297);
            this.ControlBox = false;
            this.Controls.Add(this.labelHousePrice);
            this.Controls.Add(this.labelMortgagePrice);
            this.Controls.Add(this.labelHouse);
            this.Controls.Add(this.labelMortgage);
            this.Controls.Add(this.groupBoxRent);
            this.Controls.Add(this.textBoxStreetName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StreetDetailsForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.TopMost = true;
            this.VisibleChanged += new System.EventHandler(this.StreetDetailsForm_VisibleChanged);
            this.groupBoxRent.ResumeLayout(false);
            this.groupBoxRent.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxStreetName;
        private System.Windows.Forms.GroupBox groupBoxRent;
        private System.Windows.Forms.Label labelRentHotelPrice;
        private System.Windows.Forms.Label labelRent4Price;
        private System.Windows.Forms.Label labelRent3Price;
        private System.Windows.Forms.Label labelRent2Price;
        private System.Windows.Forms.Label labelRent1Price;
        private System.Windows.Forms.Label labelRent0Price;
        private System.Windows.Forms.Label labelRentHotel;
        private System.Windows.Forms.Label labelRent4;
        private System.Windows.Forms.Label labelRent3;
        private System.Windows.Forms.Label labelRent2;
        private System.Windows.Forms.Label labelRent1;
        private System.Windows.Forms.Label labelRent0;
        private System.Windows.Forms.Label labelMortgage;
        private System.Windows.Forms.Label labelHouse;
        private System.Windows.Forms.Label labelMortgagePrice;
        private System.Windows.Forms.Label labelHousePrice;
    }
}
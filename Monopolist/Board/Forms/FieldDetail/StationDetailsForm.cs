﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Forms.FieldDetail.Helpers;
using Monopolist.Board.Forms.FieldDetail.Station.Helpers;
using Monopolist.Board.Types.Fields;

namespace Monopolist.Board.Forms.FieldDetail
{
    public partial class StationDetailsForm : DetailsForm
    {
        private StationFormFiller _formFiller;
        
        public StationDetailsForm(Form parentForm)
        {
            InitializeComponent();
            PrepareFiller();
            _positionAdjuster = new CardDetailsPosAdjuster(parentForm, this);
        }

        private void PrepareFiller()
        {
            _formFiller = new StationFormFiller(labelName, labelMortgagePrice);
        }
        
        public void UpdateInfo(BuyableCardInfo stationCardInfo)
        {
            _formFiller.FillForm(stationCardInfo);
        }

        private void StationDetailsForm_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
                _positionAdjuster.AdjustPosition();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Types.Fields.Normal;
using Monopolist.Board.Types.Fields;

namespace Monopolist.Board.Forms.FieldDetail.Field.Helpers
{
    /// <summary>
    /// Class can be used with forms that are having: Name, Description.
    /// </summary>
    public class FieldFormFiller
    {
        private TextBox _textBoxName;
        private RichTextBox _richTextBoxDescription;

        public FieldFormFiller(TextBox textBoxName, RichTextBox richTextBoxDescription)
        {
            _textBoxName = textBoxName;
            _richTextBoxDescription = richTextBoxDescription;
        }

        public void FillForm(ExtendedCardInfo extendedCardInfo)
        {
            _textBoxName.Text = extendedCardInfo.Name;
            _richTextBoxDescription.Text = extendedCardInfo.Description;
        }

        public void FillForm(CardInfo cardInfo)
        {
            _textBoxName.Text = cardInfo.Name;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Forms.FieldDetail.Helpers;
using Monopolist.Board.Forms.FieldDetail.Street.Helpers;
using Monopolist.Board.Types.Fields.Street;

namespace Monopolist.Board.Forms.FieldDetail
{
    public partial class StreetDetailsForm : DetailsForm
    {
        private StreetFormFiller _formFiller;

        public StreetDetailsForm(Form parentForm)
        {
            InitializeComponent();
            InitializeFormFiller();
            _positionAdjuster = new CardDetailsPosAdjuster(parentForm, this);
        }

        private void InitializeFormFiller()
        {
            _formFiller = new StreetFormFiller(textBoxStreetName, new List<Label>() { labelRent0Price, labelRent1Price, labelRent2Price, labelRent3Price, labelRent4Price }, labelRentHotelPrice, labelMortgagePrice, labelHousePrice);
        }
        
        public void UpdateStreetInfo(StreetCardInfo cardInfo)
        {
            _formFiller.FillForm(cardInfo);
        }

        private void StreetDetailsForm_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
                _positionAdjuster.AdjustPosition();
        }

    }
}

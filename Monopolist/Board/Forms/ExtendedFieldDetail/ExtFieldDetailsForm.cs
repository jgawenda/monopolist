﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Forms.ExtendedFieldDetail.Helpers;
using Monopolist.Board.Forms.ExtendedFieldDetail.Types;
using Monopolist.Board.Types.Fields;

namespace Monopolist.Board.Forms.ExtendedFieldDetail
{
    public partial class ExtFieldDetailsForm : Form
    {
        private CardDetailsPosAdjuster _positionAdjuster;
        private ExtDetailsFormFiller _formFiller;

        public ExtFieldDetailsForm(Form parentForm)
        {
            InitializeComponent();

            _positionAdjuster = new CardDetailsPosAdjuster(parentForm, this);

            _formFiller = new ExtDetailsFormFiller(
                new OwnerLabels(labelOwner, labelOwnerSymbol, labelOwnerName),
                new BuildingsLabels(labelBuildings, labelBuildingsDetails),
                new RentCostLabels(labelRentCost, labelRentCostDetails),
                new VisitorsLabels(labelVisitors, labelVisitorsDetails),
                new MortgageLabels(labelMortgaged, labelMortgagedPrice),
                this
                );
        }

        private void ExtFieldDetailsForm_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
                _positionAdjuster.AdjustPosition();
        }

        public void UpdateInfo(Field field)
        {
            _formFiller.FillForm(field);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Helpers.UI;

namespace Monopolist.Board.Forms.ExtendedFieldDetail.Helpers
{
    public class CardDetailsPosAdjuster : FormPositonAdjuster
    {
        public CardDetailsPosAdjuster(Form parentForm, Form childForm) : base(parentForm, childForm)
        {

        }

        public void AdjustPosition()
        {
            var coords = _parentCoordsExtractor.GetCoords(ParentCoordsExtractor.Location.Same);
            coords.X += 401;
            coords.Y += 100;

            _childForm.Location = coords;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monopolist.Board.Forms.ExtendedFieldDetail.Types;
using Monopolist.Board.Forms.ExtendedFieldDetail.Helpers.Layout;
using Monopolist.Board.Types.Fields;
using Monopolist.Board.Forms.ExtendedFieldDetail.Helpers.FillingHelpers;
using Monopolist.Board.Helpers.FieldDetail;

namespace Monopolist.Board.Forms.ExtendedFieldDetail.Helpers
{
    public class ExtDetailsFormFiller
    {
        private OwnerLabels _ownerLabels;
        private BuildingsLabels _buildingsLabels;
        private RentCostLabels _rentCostLabels;
        private VisitorsLabels _visitorsLabels;
        private MortgageLabels _mortgageLabels;

        private ExtStreetFormFiller _streetFormFiller;
        private LayoutAdjuster _layoutAdjuster;

        public ExtDetailsFormFiller(OwnerLabels ownerLabels, BuildingsLabels buildingsLabels, RentCostLabels rentCostLabels, VisitorsLabels visitorsLabels, MortgageLabels mortgageLabels, ExtFieldDetailsForm parentDetailsForm)
        {
            _ownerLabels = ownerLabels;
            _buildingsLabels = buildingsLabels;
            _rentCostLabels = rentCostLabels;
            _visitorsLabels = visitorsLabels;
            _mortgageLabels = mortgageLabels;

            _streetFormFiller = new ExtStreetFormFiller(buildingsLabels, rentCostLabels);
            _layoutAdjuster = new LayoutAdjuster(_ownerLabels, _buildingsLabels, _rentCostLabels, mortgageLabels, parentDetailsForm);
        }
        
        private void FillOwner(char symbol, string name)
        {
            _ownerLabels.LabelOwnerSymbol.Text = symbol.ToString();
            _ownerLabels.LabelOwnerName.Text = name;
        }

        private void FillVisitors(Field field)
        {
            _visitorsLabels.LabelVisitorsDetails.Text = "";

            foreach (var visitor in field.Visitors)
                _visitorsLabels.LabelVisitorsDetails.Text += visitor.Symbol;

        }

        private void FillMortgage(BuyableCardInfo buyableCardInfo)
        {
            _layoutAdjuster.SwitchLayout(LayoutAdjuster.Layout.Mortgaged);
            _mortgageLabels.LabelMortgagePrice.Text = TextFormatter.GetPrice(buyableCardInfo.MortgageBuyoutFullPrice);
        }

        private void ProceedBuyable(BuyableField buyableField)
        {
            FillOwner(buyableField.Owner.Symbol, buyableField.Owner.Name);

            if (buyableField.Mortgage == Field.MortgageStatus.Normal)
            {
                if (buyableField is FieldStreet)
                    FillStreet((FieldStreet)buyableField);
                else if (buyableField is FieldStation)
                    FillStation((FieldStation)buyableField);
                else if (buyableField is FieldUtility)
                    FillUtility((FieldUtility)buyableField);
            }
            else
                FillMortgage(buyableField.CardInfo);
        }

        private void FillStreet(FieldStreet fieldStreet)
        {
            _layoutAdjuster.SwitchLayout(LayoutAdjuster.Layout.Street);
            
            _streetFormFiller.FillBuildingsAndRent(fieldStreet);
        }

        private void FillStation(FieldStation fieldStation)
        {
            _layoutAdjuster.SwitchLayout(LayoutAdjuster.Layout.Station);
            
            _rentCostLabels.LabelRentCostDetails.Text = TextFormatter.GetPrice(fieldStation.RentPriceLive);
        }

        private void FillUtility(FieldUtility fieldUtility)
        {
            _layoutAdjuster.SwitchLayout(LayoutAdjuster.Layout.Utility);
        }
        
        private void FillDefault()
        {
            _layoutAdjuster.SwitchLayout(LayoutAdjuster.Layout.Default);
        }
        
        /// <summary>
        /// This method is used to fill Form with new data, and to adjust the layout.
        /// </summary>
        public void FillForm(Field field)
        {
            if (field is BuyableField buyableField)
            {
                ProceedBuyable(buyableField);
            }
            else
                FillDefault();
            
            FillVisitors(field);
        }

    }
}

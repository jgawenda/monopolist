﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monopolist.Board.Forms.ExtendedFieldDetail.Types;
using Monopolist.Board.Forms.ExtendedFieldDetail.Helpers.Layout;
using Monopolist.Board.Types.Fields;
using Monopolist.Board.Helpers.FieldDetail;

namespace Monopolist.Board.Forms.ExtendedFieldDetail.Helpers.FillingHelpers
{
    /// <summary>
    /// This Class is used to fill the ExtFieldDetailsForm with special type (FieldStreet).
    /// </summary>
    public class ExtStreetFormFiller
    {
        private BuildingsLabels _buildingsLabels;
        private RentCostLabels _rentCostLabels;

        public ExtStreetFormFiller(BuildingsLabels buildingsLabels, RentCostLabels rentCostLabels)
        {
            _buildingsLabels = buildingsLabels;
            _rentCostLabels = rentCostLabels;
        }
        
        private void FillStreetBuildings(FieldStreet fieldStreet)
        {
            _buildingsLabels.LabelBuildingsDetails.Text = fieldStreet.Buildings.GetGraphicRepresentation();
        }

        private void FillStreetRentPrice(FieldStreet fieldStreet)
        {
            _rentCostLabels.LabelRentCostDetails.Text = TextFormatter.GetPrice(fieldStreet.RentPriceLive);
        }

        public void FillBuildingsAndRent(FieldStreet fieldStreet)
        {
            FillStreetBuildings(fieldStreet);
            FillStreetRentPrice(fieldStreet);
        }

    }
}

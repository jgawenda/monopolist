﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Monopolist.Board.Forms.ExtendedFieldDetail.Types;

namespace Monopolist.Board.Forms.ExtendedFieldDetail.Helpers.Layout
{
    public class LayoutAdjuster
    {
        public enum Layout
        {
            Street,
            Station,
            Utility,
            Default,
            Mortgaged
        }

        private ExtFieldDetailsForm _parentDetailsForm;

        private OwnerLabels _ownerLabels;
        private BuildingsLabels _buildingsLabels;
        private RentCostLabels _rentCostLabels;
        private MortgageLabels _mortgageLabels;

        public LayoutAdjuster(OwnerLabels ownerLabels, BuildingsLabels buildingsLabels, RentCostLabels rentCostLabels, MortgageLabels mortgageLabels, ExtFieldDetailsForm parentDetailsForm)
        {
            _parentDetailsForm = parentDetailsForm;
            _ownerLabels = ownerLabels;
            _buildingsLabels = buildingsLabels;
            _rentCostLabels = rentCostLabels;
            _mortgageLabels = mortgageLabels;
        }
        
        private void SwitchToStreet()
        {
            ClearMortgage();
            _ownerLabels.ShowAll();
            _buildingsLabels.ShowAll();
            _rentCostLabels.ResetAlignment();
            _rentCostLabels.ShowAll();
        }

        private void SwitchToStation()
        {
            ClearMortgage();
            _ownerLabels.ShowAll();
            _buildingsLabels.HideAll();
            _rentCostLabels.AlignCenter();
            _rentCostLabels.ShowAll();
        }

        private void SwitchToUtility()
        {
            ClearMortgage();
            _ownerLabels.ShowAll();
            _buildingsLabels.HideAll();
            _rentCostLabels.HideAll();
        }

        private void SwitchToDefault()
        {
            ClearMortgage();
            _ownerLabels.HideAll();
            _buildingsLabels.HideAll();
            _rentCostLabels.HideAll();
        }

        private void SwitchToMortgaged()
        {
            _parentDetailsForm.BackColor = Color.IndianRed;
            _mortgageLabels.ShowAll();

            _ownerLabels.ShowAll();
            _buildingsLabels.HideAll();
            _rentCostLabels.HideAll();
        }

        private void ClearMortgage()
        {
            _parentDetailsForm.BackColor = SystemColors.Control;
            _mortgageLabels.HideAll();
        }

        public void SwitchLayout(Layout layoutType)
        {
            switch (layoutType)
            {
                case Layout.Street:
                    SwitchToStreet();
                    break;
                case Layout.Station:
                    SwitchToStation();
                    break;
                case Layout.Utility:
                    SwitchToUtility();
                    break;
                case Layout.Default:
                    SwitchToDefault();
                    break;
                case Layout.Mortgaged:
                    SwitchToMortgaged();
                    break;
            }
        }

    }
}

﻿namespace Monopolist.Board.Forms.ExtendedFieldDetail
{
    partial class ExtFieldDetailsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExtFieldDetailsForm));
            this.labelOwnerSymbol = new System.Windows.Forms.Label();
            this.labelVisitorsDetails = new System.Windows.Forms.Label();
            this.labelRentCostDetails = new System.Windows.Forms.Label();
            this.labelBuildingsDetails = new System.Windows.Forms.Label();
            this.labelOwnerName = new System.Windows.Forms.Label();
            this.labelVisitors = new System.Windows.Forms.Label();
            this.labelRentCost = new System.Windows.Forms.Label();
            this.labelBuildings = new System.Windows.Forms.Label();
            this.labelOwner = new System.Windows.Forms.Label();
            this.labelMortgaged = new System.Windows.Forms.Label();
            this.labelMortgagedPrice = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelOwnerSymbol
            // 
            this.labelOwnerSymbol.AutoSize = true;
            this.labelOwnerSymbol.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelOwnerSymbol.Location = new System.Drawing.Point(81, 44);
            this.labelOwnerSymbol.Name = "labelOwnerSymbol";
            this.labelOwnerSymbol.Size = new System.Drawing.Size(41, 31);
            this.labelOwnerSymbol.TabIndex = 17;
            this.labelOwnerSymbol.Text = "❀";
            // 
            // labelVisitorsDetails
            // 
            this.labelVisitorsDetails.AutoSize = true;
            this.labelVisitorsDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelVisitorsDetails.Location = new System.Drawing.Point(49, 237);
            this.labelVisitorsDetails.Name = "labelVisitorsDetails";
            this.labelVisitorsDetails.Size = new System.Drawing.Size(209, 37);
            this.labelVisitorsDetails.TabIndex = 16;
            this.labelVisitorsDetails.Text = "♔♕♘❦❄❀";
            // 
            // labelRentCostDetails
            // 
            this.labelRentCostDetails.AutoSize = true;
            this.labelRentCostDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelRentCostDetails.Location = new System.Drawing.Point(190, 138);
            this.labelRentCostDetails.Name = "labelRentCostDetails";
            this.labelRentCostDetails.Size = new System.Drawing.Size(60, 24);
            this.labelRentCostDetails.TabIndex = 15;
            this.labelRentCostDetails.Text = "$1550";
            // 
            // labelBuildingsDetails
            // 
            this.labelBuildingsDetails.AutoSize = true;
            this.labelBuildingsDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelBuildingsDetails.Location = new System.Drawing.Point(59, 138);
            this.labelBuildingsDetails.Name = "labelBuildingsDetails";
            this.labelBuildingsDetails.Size = new System.Drawing.Size(62, 24);
            this.labelBuildingsDetails.TabIndex = 14;
            this.labelBuildingsDetails.Text = "⌂⌂⌂⌂";
            // 
            // labelOwnerName
            // 
            this.labelOwnerName.AutoSize = true;
            this.labelOwnerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelOwnerName.Location = new System.Drawing.Point(122, 49);
            this.labelOwnerName.Name = "labelOwnerName";
            this.labelOwnerName.Size = new System.Drawing.Size(49, 18);
            this.labelOwnerName.TabIndex = 13;
            this.labelOwnerName.Text = "Player";
            // 
            // labelVisitors
            // 
            this.labelVisitors.AutoSize = true;
            this.labelVisitors.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelVisitors.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.labelVisitors.Location = new System.Drawing.Point(128, 208);
            this.labelVisitors.Name = "labelVisitors";
            this.labelVisitors.Size = new System.Drawing.Size(46, 15);
            this.labelVisitors.TabIndex = 12;
            this.labelVisitors.Text = "Visitors";
            // 
            // labelRentCost
            // 
            this.labelRentCost.AutoSize = true;
            this.labelRentCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelRentCost.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.labelRentCost.Location = new System.Drawing.Point(189, 111);
            this.labelRentCost.Name = "labelRentCost";
            this.labelRentCost.Size = new System.Drawing.Size(58, 15);
            this.labelRentCost.TabIndex = 11;
            this.labelRentCost.Text = "Rent cost";
            // 
            // labelBuildings
            // 
            this.labelBuildings.AutoSize = true;
            this.labelBuildings.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelBuildings.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.labelBuildings.Location = new System.Drawing.Point(58, 111);
            this.labelBuildings.Name = "labelBuildings";
            this.labelBuildings.Size = new System.Drawing.Size(58, 15);
            this.labelBuildings.TabIndex = 10;
            this.labelBuildings.Text = "Buildings";
            // 
            // labelOwner
            // 
            this.labelOwner.AutoSize = true;
            this.labelOwner.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelOwner.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.labelOwner.Location = new System.Drawing.Point(128, 27);
            this.labelOwner.Name = "labelOwner";
            this.labelOwner.Size = new System.Drawing.Size(43, 15);
            this.labelOwner.TabIndex = 9;
            this.labelOwner.Text = "Owner";
            // 
            // labelMortgaged
            // 
            this.labelMortgaged.AutoSize = true;
            this.labelMortgaged.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMortgaged.Location = new System.Drawing.Point(60, 104);
            this.labelMortgaged.Name = "labelMortgaged";
            this.labelMortgaged.Size = new System.Drawing.Size(184, 36);
            this.labelMortgaged.TabIndex = 18;
            this.labelMortgaged.Text = "This field is MORTGAGED\r\nYou can buyout for:";
            this.labelMortgaged.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labelMortgaged.Visible = false;
            // 
            // labelMortgagedPrice
            // 
            this.labelMortgagedPrice.AutoSize = true;
            this.labelMortgagedPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMortgagedPrice.Location = new System.Drawing.Point(128, 152);
            this.labelMortgagedPrice.Name = "labelMortgagedPrice";
            this.labelMortgagedPrice.Size = new System.Drawing.Size(50, 24);
            this.labelMortgagedPrice.TabIndex = 19;
            this.labelMortgagedPrice.Text = "$400";
            this.labelMortgagedPrice.Visible = false;
            // 
            // ExtFieldDetailsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(297, 297);
            this.ControlBox = false;
            this.Controls.Add(this.labelMortgagedPrice);
            this.Controls.Add(this.labelOwnerSymbol);
            this.Controls.Add(this.labelVisitorsDetails);
            this.Controls.Add(this.labelRentCostDetails);
            this.Controls.Add(this.labelBuildingsDetails);
            this.Controls.Add(this.labelOwnerName);
            this.Controls.Add(this.labelVisitors);
            this.Controls.Add(this.labelRentCost);
            this.Controls.Add(this.labelBuildings);
            this.Controls.Add(this.labelOwner);
            this.Controls.Add(this.labelMortgaged);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ExtFieldDetailsForm";
            this.ShowInTaskbar = false;
            this.TopMost = true;
            this.VisibleChanged += new System.EventHandler(this.ExtFieldDetailsForm_VisibleChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelOwnerSymbol;
        private System.Windows.Forms.Label labelVisitorsDetails;
        private System.Windows.Forms.Label labelRentCostDetails;
        private System.Windows.Forms.Label labelBuildingsDetails;
        private System.Windows.Forms.Label labelOwnerName;
        private System.Windows.Forms.Label labelVisitors;
        private System.Windows.Forms.Label labelRentCost;
        private System.Windows.Forms.Label labelBuildings;
        private System.Windows.Forms.Label labelOwner;
        private System.Windows.Forms.Label labelMortgaged;
        private System.Windows.Forms.Label labelMortgagedPrice;
    }
}
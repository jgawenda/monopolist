﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monopolist.Board.Forms.ExtendedFieldDetail.Types
{
    public class BuildingsLabels
    {
        public Label LabelBuildings { get; }
        public Label LabelBuildingsDetails { get; }

        public BuildingsLabels(Label labelBuildings, Label labelBuildingsDetails)
        {
            LabelBuildings = labelBuildings;
            LabelBuildingsDetails = labelBuildingsDetails;
        }

        public void HideAll()
        {
            LabelBuildings.Visible = false;
            LabelBuildingsDetails.Visible = false;
        }

        public void ShowAll()
        {
            LabelBuildings.Visible = true;
            LabelBuildingsDetails.Visible = true;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monopolist.Board.Forms.ExtendedFieldDetail.Types
{
    public class MortgageLabels
    {
        public Label LabelMortgage { get; }
        public Label LabelMortgagePrice { get; }

        public MortgageLabels(Label labelMortgage, Label labelMortgagePrice)
        {
            LabelMortgage = labelMortgage;
            LabelMortgagePrice = labelMortgagePrice;
        }

        public void HideAll()
        {
            LabelMortgage.Visible = false;
            LabelMortgagePrice.Visible = false;
        }

        public void ShowAll()
        {
            LabelMortgage.Visible = true;
            LabelMortgagePrice.Visible = true;
        }

    }
}

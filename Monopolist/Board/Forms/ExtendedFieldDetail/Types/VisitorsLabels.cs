﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monopolist.Board.Forms.ExtendedFieldDetail.Types
{
    public class VisitorsLabels
    {
        public Label LabelVisitors { get; }
        public Label LabelVisitorsDetails { get; }

        public VisitorsLabels(Label labelVisitors, Label labelVisitorsDetails)
        {
            LabelVisitors = labelVisitors;
            LabelVisitorsDetails = labelVisitorsDetails;
        }

        public void HideAll()
        {
            LabelVisitors.Visible = false;
            LabelVisitorsDetails.Visible = false;
        }

        public void ShowAll()
        {
            LabelVisitors.Visible = true;
            LabelVisitorsDetails.Visible = true;
        }

    }
}

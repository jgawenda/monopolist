﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monopolist.Board.Forms.ExtendedFieldDetail.Types
{
    public class OwnerLabels
    {
        public Label LabelOwner { get; }
        public Label LabelOwnerSymbol { get; }
        public Label LabelOwnerName { get; }

        public OwnerLabels(Label labelOwner, Label labelOwnerSymbol, Label labelOwnerName)
        {
            LabelOwner = labelOwner;
            LabelOwnerSymbol = labelOwnerSymbol;
            LabelOwnerName = labelOwnerName;
        }

        public void HideAll()
        {
            LabelOwner.Visible = false;
            LabelOwnerSymbol.Visible = false;
            LabelOwnerName.Visible = false;
        }

        public void ShowAll()
        {
            LabelOwner.Visible = true;
            LabelOwnerSymbol.Visible = true;
            LabelOwnerName.Visible = true;
        }

    }
}

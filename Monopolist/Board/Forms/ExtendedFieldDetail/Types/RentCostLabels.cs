﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monopolist.Board.Forms.ExtendedFieldDetail.Types
{
    public class RentCostLabels
    {
        public Label LabelRentCost { get; }
        public Label LabelRentCostDetails { get; }

        public RentCostLabels(Label labelRentCost, Label labelRentCostDetails)
        {
            LabelRentCost = labelRentCost;
            LabelRentCostDetails = labelRentCostDetails;
        }

        public void HideAll()
        {
            LabelRentCost.Visible = false;
            LabelRentCostDetails.Visible = false;
        }

        public void ShowAll()
        {
            LabelRentCost.Visible = true;
            LabelRentCostDetails.Visible = true;
        }

        public void AlignCenter()
        {
            LabelRentCost.Location = new System.Drawing.Point(117, 111);
            LabelRentCostDetails.Location = new System.Drawing.Point(117, 138);
        }

        public void ResetAlignment()
        {
            LabelRentCost.Location = new System.Drawing.Point(184, 111);
            LabelRentCostDetails.Location = new System.Drawing.Point(184, 138);
        }

    }
}

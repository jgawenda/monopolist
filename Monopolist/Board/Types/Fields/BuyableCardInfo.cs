﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopolist.Board.Types.Fields
{
    public class BuyableCardInfo : CardInfo
    {
        public int Price { get; }
        public int MortgageValue { get; }
        public double MortgageValuePercent { get; }

        /// <summary>
        /// How much Player have to pay to lift mortgage
        /// </summary>
        public int MortgageBuyoutFullPrice { get; }

        public BuyableCardInfo(string name, int price) : base(name)
        {
            Price = price;
            MortgageValue = Price / 2;
            MortgageValuePercent = MortgageValue * 0.1;
            MortgageBuyoutFullPrice = MortgageValue + Convert.ToInt32(MortgageValuePercent);
        }

    }
}

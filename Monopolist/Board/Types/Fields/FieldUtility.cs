﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;
using Monopolist.Board.Types.Fields.Station;
using Monopolist.Board.Types.Fields.Utility;
using Monopolist.Board.Types.Fields.Mortgage;

namespace Monopolist.Board.Types.Fields
{
    public class FieldUtility : BuyableField
    {
        // Class used to help with calculating live rent price
        private UtilityRentPriceCalculator _rentPriceCalculator;

        private MortgageOperator _mortgageOperator;

        public FieldUtility(PlayerAccount owner, BuyableCardInfo cardInfo, List<FieldUtility> allUtilities, Button assignedButton) : base(owner, cardInfo, assignedButton)
        {
            _rentPriceCalculator = new UtilityRentPriceCalculator(allUtilities);
            _mortgageOperator = new MortgageOperator(this, CardInfo);
        }

        public int GetRentPriceLive(int diceNumber)
        {
            return _rentPriceCalculator.GetRentPrice(this, diceNumber);
        }

        public override void MakeMortgage()
        {
            _mortgageOperator.MakeMortgage();
        }

        public override void BuyoutMortgage()
        {
            _mortgageOperator.BuyoutMortgage();
        }

    }
}

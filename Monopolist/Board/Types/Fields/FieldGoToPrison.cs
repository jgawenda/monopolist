﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;
using Monopolist.Board.Types.Fields.Normal;

namespace Monopolist.Board.Types.Fields
{
    public class FieldGoToPrison : NormalField
    {
        private FieldPrison _prison;

        public FieldGoToPrison(FieldPrison prison, Button assignedButton) : base(new ExtendedCardInfo("Idź do więzienia", "Gracz, który stanie na tym polu, ma obowiązek natychmiast udać się do więzienia."), assignedButton)
        {
            _prison = prison;
        }

        public override void AddVisitor(PlayerAccount playerAccount)
        {
            MessageBox.Show("You go to prison!", "Go to prison", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            _prison.LockInPrison(playerAccount);
        }

    }
}

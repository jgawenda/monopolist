﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monopolist.Board.Types.Fields.Containers
{
    /// <summary>
    /// All lists inside this Class are sorted in order of appearance on Game Board
    /// </summary>
    public class AllFieldsContainer
    {
        public List<Field> AllFields { get; }

        public List<FieldStreet> AllStreets { get; private set; }

        public List<BuyableField> AllBuyableFields { get; private set; }

        /// <summary>
        /// Start, Prison, Free Parking, Go to Prison
        /// </summary>
        public List<NormalField> NormalFields { get; private set; }

        public List<FieldChance> ChestFields { get; private set; }

        /// <summary>
        /// ChanceFields and ChestFields mixed
        /// </summary>
        public List<FieldChance> CardFields { get; private set; }

        public List<FieldChance> ChanceFields { get; private set; }

        public List<FieldTax> TaxFields { get; private set; }

        public List<FieldStation> StationFields { get; private set; }

        public List<FieldUtility> UtilityFields { get; private set; }
        
        public FieldStart Start { get; }
        public FieldStreet StreetBrown1 { get; }
        public FieldChance ChestCard1 { get; }
        public FieldStreet StreetBrown2 { get; }
        public FieldTax Tax1 { get; }
        public FieldStation Station1 { get; }
        public FieldStreet StreetBlue1 { get; }
        public FieldChance Chance1 { get; }
        public FieldStreet StreetBlue2 { get; }
        public FieldStreet StreetBlue3 { get; }
        public FieldPrison Prison { get; }
        public FieldStreet StreetViolet1 { get; }
        public FieldUtility Utility1 { get; }
        public FieldStreet StreetViolet2 { get; }
        public FieldStreet StreetViolet3 { get; }
        public FieldStation Station2 { get; }
        public FieldStreet StreetOrange1 { get; }
        public FieldChance ChestCard2 { get; }
        public FieldStreet StreetOrange2 { get; }
        public FieldStreet StreetOrange3 { get; }
        public FieldParking Parking { get; }
        public FieldStreet StreetRed1 { get; }
        public FieldChance Chance2 { get; }
        public FieldStreet StreetRed2 { get; }
        public FieldStreet StreetRed3 { get; }
        public FieldStation Station3 { get; }
        public FieldStreet StreetYellow1 { get; }
        public FieldStreet StreetYellow2 { get; }
        public FieldUtility Utility2 { get; }
        public FieldStreet StreetYellow3 { get; }
        public FieldGoToPrison GoToPrison { get; }
        public FieldStreet StreetGreen1 { get; }
        public FieldStreet StreetGreen2 { get; }
        public FieldChance ChestCard3 { get; }
        public FieldStreet StreetGreen3 { get; }
        public FieldStation Station4 { get; }
        public FieldChance Chance3 { get; }
        public FieldStreet StreetDark1 { get; }
        public FieldTax Tax2 { get; }
        public FieldStreet StreetDark2 { get; }

        /// <summary>
        /// Make sure to provide List containing all fields in exact order.
        /// </summary>
        /// <param name="allFields"></param>
        public AllFieldsContainer(List<Field> allFields)
        {
            Start = (FieldStart)allFields[0];
            StreetBrown1 = (FieldStreet)allFields[1];
            ChestCard1 = (FieldChance)allFields[2];
            StreetBrown2 = (FieldStreet)allFields[3];
            Tax1 = (FieldTax)allFields[4];
            Station1 = (FieldStation)allFields[5];
            StreetBlue1 = (FieldStreet)allFields[6];
            Chance1 = (FieldChance)allFields[7];
            StreetBlue2 = (FieldStreet)allFields[8];
            StreetBlue3 = (FieldStreet)allFields[9];
            Prison = (FieldPrison)allFields[10];
            StreetViolet1 = (FieldStreet)allFields[11];
            Utility1 = (FieldUtility)allFields[12];
            StreetViolet2 = (FieldStreet)allFields[13];
            StreetViolet3 = (FieldStreet)allFields[14];
            Station2 = (FieldStation)allFields[15];
            StreetOrange1 = (FieldStreet)allFields[16];
            ChestCard2 = (FieldChance)allFields[17];
            StreetOrange2 = (FieldStreet)allFields[18];
            StreetOrange3 = (FieldStreet)allFields[19];
            Parking = (FieldParking)allFields[20];
            StreetRed1 = (FieldStreet)allFields[21];
            Chance2 = (FieldChance)allFields[22];
            StreetRed2 = (FieldStreet)allFields[23];
            StreetRed3 = (FieldStreet)allFields[24];
            Station3 = (FieldStation)allFields[25];
            StreetYellow1 = (FieldStreet)allFields[26];
            StreetYellow2 = (FieldStreet)allFields[27];
            Utility2 = (FieldUtility)allFields[28];
            StreetYellow3 = (FieldStreet)allFields[29];
            GoToPrison = (FieldGoToPrison)allFields[30];
            StreetGreen1 = (FieldStreet)allFields[31];
            StreetGreen2 = (FieldStreet)allFields[32];
            ChestCard3 = (FieldChance)allFields[33];
            StreetGreen3 = (FieldStreet)allFields[34];
            Station4 = (FieldStation)allFields[35];
            Chance3 = (FieldChance)allFields[36];
            StreetDark1 = (FieldStreet)allFields[37];
            Tax2 = (FieldTax)allFields[38];
            StreetDark2 = (FieldStreet)allFields[39];

            AllFields = allFields;
            GenerateStreetsList();
            GenerateBuyableList();
            GenerateNormalFieldsList();
            GenerateChestFieldsList();
            GenerateChanceFieldsList();
            GenerateCardFieldsList();
            GenerateTaxFieldsList();
            GenerateStationFieldsList();
            GenerateUtilityFieldsList();
        }

        private void GenerateStreetsList()
        {
            AllStreets = new List<FieldStreet>()
            {
                StreetBrown1,
                StreetBrown2,
                StreetBlue1,
                StreetBlue2,
                StreetBlue3,
                StreetViolet1,
                StreetViolet2,
                StreetViolet3,
                StreetOrange1,
                StreetOrange2,
                StreetOrange3,
                StreetRed1,
                StreetRed2,
                StreetRed3,
                StreetYellow1,
                StreetYellow2,
                StreetYellow3,
                StreetGreen1,
                StreetGreen2,
                StreetGreen3,
                StreetDark1,
                StreetDark2
            };
        }

        private void GenerateBuyableList()
        {
            AllBuyableFields = new List<BuyableField>()
            {
                StreetBrown1,
                StreetBrown2,
                Station1,
                StreetBlue1,
                StreetBlue2,
                StreetBlue3,
                StreetViolet1,
                Utility1,
                StreetViolet2,
                StreetViolet3,
                Station2,
                StreetOrange1,
                StreetOrange2,
                StreetOrange3,
                StreetRed1,
                StreetRed2,
                StreetRed3,
                Station3,
                StreetYellow1,
                StreetYellow2,
                Utility2,
                StreetYellow3,
                StreetGreen1,
                StreetGreen2,
                StreetGreen3,
                Station4,
                StreetDark1,
                StreetDark2
            };
        }

        private void GenerateNormalFieldsList()
        {
            NormalFields = new List<NormalField>()
            {
                Start,
                Prison,
                Parking,
                GoToPrison
            };
        }

        private void GenerateChestFieldsList()
        {
            ChestFields = new List<FieldChance>()
            {
                ChestCard1,
                ChestCard2,
                ChestCard3
            };
        }

        private void GenerateChanceFieldsList()
        {
            ChanceFields = new List<FieldChance>()
            {
                Chance1,
                Chance2,
                Chance3
            };
        }

        private void GenerateCardFieldsList()
        {
            CardFields = new List<FieldChance>()
            {
                ChestCard1,
                Chance1,
                ChestCard2,
                Chance2,
                ChestCard3,
                Chance3
            };
        }

        private void GenerateTaxFieldsList()
        {
            TaxFields = new List<FieldTax>()
            {
                Tax1,
                Tax2
            };
        }

        private void GenerateStationFieldsList()
        {
            StationFields = new List<FieldStation>()
            {
                Station1,
                Station2,
                Station3,
                Station4
            };
        }

        private void GenerateUtilityFieldsList()
        {
            UtilityFields = new List<FieldUtility>()
            {
                Utility1,
                Utility2
            };
        }

    }
}

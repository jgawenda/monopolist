﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopolist.Board.Types.Fields.Station
{
    public class StationRentPriceCalculator
    {
        private List<FieldStation> _allStations;

        public StationRentPriceCalculator(List<FieldStation> allStations)
        {
            _allStations = allStations;
        }

        private int CountHowManyOwned(FieldStation parentFieldStation)
        {
            int counter = 0;

            foreach (var station in _allStations)
            {
                if (station.Owner == parentFieldStation.Owner)
                    counter++;
            }

            return counter;
        }

        public int GetRentPrice(FieldStation parentFieldStation)
        {
            int rentPrice = 0;

            if ((parentFieldStation.Owner.Type != Core.Types.Players.PlayerAccount.PlayerType.Bank) && (parentFieldStation.Mortgage == Field.MortgageStatus.Normal))
            {
                rentPrice = 25;

                int howManyOwned = CountHowManyOwned(parentFieldStation);

                for (int i = 1; i < howManyOwned; i++)
                    rentPrice *= 2;
                
            }

            return rentPrice;
        }

    }
}

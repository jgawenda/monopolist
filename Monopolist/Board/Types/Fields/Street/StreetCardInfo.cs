﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Monopolist.Board.Types.Fields.Street
{
    public class StreetCardInfo : BuyableCardInfo
    {
        public Color StreetColor { get; }
        public int RentPrice0 { get; }
        public int RentPrice1 { get; }
        public int RentPrice2 { get; }
        public int RentPrice3 { get; }
        public int RentPrice4 { get; }
        public int RentPriceHotel { get; }
        
        public int OneHousePrice { get; }

        public StreetCardInfo(Color color, string name, int price, int rentPrice0, int rentPrice1, int rentPrice2, int rentPrice3, int rentPrice4, int rentPriceHotel, int oneHousePrice) : base(name, price)
        {
            StreetColor = color;
            RentPrice0 = rentPrice0;
            RentPrice1 = rentPrice1;
            RentPrice2 = rentPrice2;
            RentPrice3 = rentPrice3;
            RentPrice4 = rentPrice4;
            RentPriceHotel = rentPriceHotel;
            OneHousePrice = oneHousePrice;
        }

    }
}

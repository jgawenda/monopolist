﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monopolist.Core.Types.Players;

namespace Monopolist.Board.Types.Fields.Street
{
    /* 
     * This Class can be implemented and stored inside the FieldStreet (in private field)
     * It can provide anytime, anywhere access to List that contains
     * all Streets of the same color.
     * The list would be generated dynamically, and could be accesed by (for example)
     * "streetBlue1.GetAllStreetsOfSameColor()".
     * In case we use this Class, consider removing other Class:
     * Monopolist.Board.Forms.PlayerTurn.Helpers.Tools.SameColorStreetsExtractor
     */

    public class StreetInfoExtractor
    {
        private FieldStreet _parentFieldStreet;
        private List<FieldStreet> _allStreets;

        public StreetInfoExtractor(List<FieldStreet> allStreets, FieldStreet parentField)
        {
            _parentFieldStreet = parentField;
            _allStreets = allStreets;
        }

        public List<FieldStreet> GetAllStreetsOfTheSameColor()
        {
            return _allStreets.Where(p => (p.StreetCardInfo.StreetColor == _parentFieldStreet.StreetCardInfo.StreetColor)).ToList();
        }

        public bool OwnsEveryStreetOfSameColor(PlayerAccount playerAccount)
        {
            bool result = true;

            List<FieldStreet> sameColorStreets = GetAllStreetsOfTheSameColor();

            foreach (FieldStreet street in sameColorStreets)
            {
                if (street.Owner != playerAccount)
                {
                    result = false;
                    break;
                }
            }

            return result;
        }

    }
}

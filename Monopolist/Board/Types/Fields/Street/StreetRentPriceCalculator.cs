﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monopolist.Core.Types.Players;

namespace Monopolist.Board.Types.Fields.Street
{
    public class StreetRentPriceCalculator
    {
        private FieldStreet _parentFieldStreet;
        private List<FieldStreet> _allStreets;

        public StreetRentPriceCalculator(FieldStreet parentFieldStreet, List<FieldStreet> allStreets)
        {
            _parentFieldStreet = parentFieldStreet;
            _allStreets = allStreets;
        }

        public bool OwnsEveryStreetOfSameColor(PlayerAccount playerAccount)
        {
            bool result = true;

            List<FieldStreet> sameColorStreets = _allStreets.Where(p => p.StreetCardInfo.StreetColor == _parentFieldStreet.StreetCardInfo.StreetColor).ToList();
            
            foreach (FieldStreet street in sameColorStreets)
            {
                if (street.Owner != playerAccount)
                {
                    result = false;
                    break;
                }
            }

            return result;
        }

        public int GetRentPrice()
        {
            int rentPrice = 0;

            if ((_parentFieldStreet.Owner.Type != PlayerAccount.PlayerType.Bank) && (_parentFieldStreet.Mortgage == Field.MortgageStatus.Normal))
            {
                if (_parentFieldStreet.Buildings.Hotels > 0)
                    rentPrice = _parentFieldStreet.StreetCardInfo.RentPriceHotel;
                else
                {
                    switch (_parentFieldStreet.Buildings.Houses)
                    {
                        case 0:
                            rentPrice = _parentFieldStreet.StreetCardInfo.RentPrice0;
                            if (OwnsEveryStreetOfSameColor(_parentFieldStreet.Owner))
                                rentPrice *= 2;
                            break;
                        case 1:
                            rentPrice = _parentFieldStreet.StreetCardInfo.RentPrice1;
                            break;
                        case 2:
                            rentPrice = _parentFieldStreet.StreetCardInfo.RentPrice2;
                            break;
                        case 3:
                            rentPrice = _parentFieldStreet.StreetCardInfo.RentPrice3;
                            break;
                        case 4:
                            rentPrice = _parentFieldStreet.StreetCardInfo.RentPrice4;
                            break;
                    }
                }
            }
            
            return rentPrice;
        }

    }
}

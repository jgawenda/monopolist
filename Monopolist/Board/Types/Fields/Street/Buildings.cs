﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopolist.Board.Types.Fields.Street
{
    public class Buildings
    {
        public int Houses { get; private set; }
        public int Hotels { get; private set; }

        public Buildings()
        {
            RemoveAllBuildings();
        }

        private void RemoveHouses()
        {
            Houses = 0;
        }

        private void MakeFullHouses()
        {
            Houses = 4;
        }

        public void RemoveAllBuildings()
        {
            Houses = 0;
            Hotels = 0;
        }

        public bool AddBuilding()
        {
            if (Hotels < 1)
            {
                if (Houses < 4)
                    Houses++;
                else
                {
                    RemoveHouses();
                    Hotels++;
                }

                return true;
            }

            return false;
        }

        public bool AddBuilding(int howMany)
        {
            for (int i=0; i<howMany; i++)
            {
                if (!AddBuilding())
                    return false;
            }

            return true;
        }

        public bool RemoveBuilding()
        {
            if (Hotels > 0)
            {
                Hotels--;
                MakeFullHouses();
            }
            else
            {
                if (Houses > 0)
                    Houses--;
                else
                    return false;
            }

            return true;
        }

        public bool RemoveBuilding(int howMany)
        {
            for (int i=0; i<howMany; i++)
            {
                if (!RemoveBuilding())
                    return false;
            }

            return true;
        }

        public string GetGraphicRepresentation()
        {
            string result = "";

            if (Hotels > 0)
                result = "H";
            else
            {
                for (int i = 0; i < Houses; i++)
                    result += "⌂";
            }

            return result;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Types.Fields.Mortgage;

namespace Monopolist.Board.Types.Fields.Street
{
    public class StreetMortgageOperator : MortgageOperator
    {
        private Buildings _buildings;

        public StreetMortgageOperator(FieldStreet parentField) : base(parentField, parentField.StreetCardInfo)
        {
            _buildings = parentField.Buildings;
        }

        public void BuyBuilding()
        {
            if (IsMortgageNormal())
            {
                StreetCardInfo cardInfo = (StreetCardInfo)_cardInfo;

                if (_parentField.Owner.CashAccount.Cash >= cardInfo.OneHousePrice)
                {
                    if (_buildings.AddBuilding())
                    {
                        _parentField.Owner.CashAccount.RemoveCash(cardInfo.OneHousePrice);
                    }
                    else
                        MessageBox.Show("You can't buy another building. You've reached maximum.");
                }
                else
                    MessageBox.Show("You don't have enough money to buy this building.", "Not enough money", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
                MessageBox.Show("You cant buy buildings when street is mortgaged.", "Street is mortgaged", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            
        }

        public void SellBuilding()
        {
            StreetCardInfo cardInfo = (StreetCardInfo)_cardInfo;

            if (_buildings.RemoveBuilding())
                _parentField.Owner.CashAccount.AddCash(cardInfo.OneHousePrice / 2);
            else
                MessageBox.Show("You don't have any buildings to sell.");
            
        }
        
        public override void MakeMortgage()
        {
            if (IsMortgageNormal())
            {
                if ((_buildings.Hotels == 0) && (_buildings.Houses == 0))
                {
                    _parentField.Mortgage = Field.MortgageStatus.Mortgaged;
                    _parentField.Owner.CashAccount.AddCash(_cardInfo.MortgageValue);
                }
                else
                    MessageBox.Show("You cannot mortgage this street. Try selling the buildings first.");
            }
            else
                MessageBox.Show("This street is already mortgaged.");
            
        }
        
    }
}

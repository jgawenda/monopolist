﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;
using Monopolist.Board.Types.Fields.Normal;

namespace Monopolist.Board.Types.Fields
{
    public class FieldStart : NormalField
    {
        public FieldStart(Button assignedButton) : base(new ExtendedCardInfo("START", "W chwili przejścia przez start, pobierz $200"), assignedButton)
        {

        }
        
    }
}

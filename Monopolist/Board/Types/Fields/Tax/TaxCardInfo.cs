﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopolist.Board.Types.Fields.Tax
{
    public class TaxCardInfo : CardInfo
    {
        public int TaxValue { get; }

        public TaxCardInfo(string name, int taxValue) : base(name)
        {
            TaxValue = taxValue;
        }
    }
}

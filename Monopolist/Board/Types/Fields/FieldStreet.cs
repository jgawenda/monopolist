﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Types.Fields.Street;
using Monopolist.Core.Types.Players;

namespace Monopolist.Board.Types.Fields
{
    public class FieldStreet : BuyableField
    {
        public Buildings Buildings { get; }
        
        public StreetCardInfo StreetCardInfo { get; }

        private StreetRentPriceCalculator _rentPriceCalculator;
        
        public int RentPriceLive
        {
            get
            {
                return _rentPriceCalculator.GetRentPrice();
            }
        }

        private StreetMortgageOperator _mortgageOperator;

        public FieldStreet(PlayerAccount streetOwner, StreetCardInfo cardInfo, List<FieldStreet> allStreets, Button assignedButton) : base(streetOwner, cardInfo, assignedButton)
        {
            StreetCardInfo = cardInfo;
            Buildings = new Buildings();

            _rentPriceCalculator = new StreetRentPriceCalculator(this, allStreets);

            _mortgageOperator = new StreetMortgageOperator(this);
        }

        /// <summary>
        /// This method is used to check if user owns every Streets of the same color.
        /// </summary>
        public bool CheckSameColor(PlayerAccount playerAccount)
        {
            return _rentPriceCalculator.OwnsEveryStreetOfSameColor(playerAccount);
        }

        public override void MakeMortgage()
        {
            _mortgageOperator.MakeMortgage();
        }

        public override void BuyoutMortgage()
        {
            _mortgageOperator.BuyoutMortgage();
        }

        public void BuyBuilding()
        {
            _mortgageOperator.BuyBuilding();
        }

        public void SellBuilding()
        {
            _mortgageOperator.SellBuilding();
        }

        public override string ToString()
        {
            return String.Format("{0} ({1}, ${2} each)", base.ToString(), Buildings.GetGraphicRepresentation(), StreetCardInfo.OneHousePrice);
        }

    }
}

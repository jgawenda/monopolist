﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;
using Monopolist.Board.Types.Fields.Station;
using Monopolist.Board.Types.Fields.Mortgage;

namespace Monopolist.Board.Types.Fields
{
    public class FieldStation : BuyableField
    {
        public int RentPriceLive
        {
            get
            {
                return _rentPriceCalculator.GetRentPrice(this);
            }
        }

        // Class used to help with calculating live rent price
        private StationRentPriceCalculator _rentPriceCalculator;

        private MortgageOperator _mortgageOperator;

        public FieldStation(PlayerAccount owner, BuyableCardInfo cardInfo, List<FieldStation> allStations, Button assignedButton) : base(owner, cardInfo, assignedButton)
        {
            _rentPriceCalculator = new StationRentPriceCalculator(allStations);
            _mortgageOperator = new MortgageOperator(this, CardInfo);
        }

        public override void MakeMortgage()
        {
            _mortgageOperator.MakeMortgage();
        }

        public override void BuyoutMortgage()
        {
            _mortgageOperator.BuyoutMortgage();
        }

    }
}

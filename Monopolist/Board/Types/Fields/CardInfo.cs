﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopolist.Board.Types.Fields
{
    public class CardInfo
    {
        public string Name { get; }

        public CardInfo(string name)
        {
            Name = name;
        }
        
    }
}

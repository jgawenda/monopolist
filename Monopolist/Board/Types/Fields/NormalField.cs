﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Types.Fields.Normal;

namespace Monopolist.Board.Types.Fields
{
    public abstract class NormalField : Field
    {
        public ExtendedCardInfo CardInfo { get; }

        public override string Name { get { return CardInfo.Name; } }

        public NormalField(ExtendedCardInfo extendedCardInfo, Button assignedButton) : base(assignedButton)
        {
            CardInfo = extendedCardInfo;
        }

    }
}

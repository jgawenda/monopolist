﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;
using Monopolist.Board.Types.Fields.Actions.Buyable.Helpers;

namespace Monopolist.Board.Types.Fields.Actions.Buyable
{
    public class BuyableFieldAction
    {
        private BuyableField _buyableField;
        
        public BuyableFieldAction(BuyableField buyableField)
        {
            _buyableField = buyableField;
        }

        private void Buying(PlayerAccount playerAccount)
        {
            var dialogResult = MessageBox.Show(String.Format("Do you want to buy {0} for ${1}?", _buyableField.Name, _buyableField.CardInfo.Price), "Buying property", MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
                FieldBuyingOperator.BuyField(_buyableField, playerAccount);
        }

        private void ProceedPayingOrNot(PlayerAccount playerAccount)
        {
            if (_buyableField.Owner == playerAccount)
                MessageBox.Show("This is your property. You can sit back and relax :)");
            else
                RentPayingOperator.PayRent(_buyableField, playerAccount);
        }
        
        public void PerformAction(PlayerAccount playerAccount)
        {
            switch (_buyableField.Owner.Type)
            {
                case PlayerAccount.PlayerType.Bank:
                    Buying(playerAccount);
                    break;
                default:
                    ProceedPayingOrNot(playerAccount);
                    break;
            }
        }

    }
}

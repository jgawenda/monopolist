﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;

namespace Monopolist.Board.Types.Fields.Actions.Buyable.Helpers
{
    public static class FieldBuyingOperator
    {
        public static void BuyField(BuyableField fieldToBuy, PlayerAccount playerAccount)
        {
            if (playerAccount.CashAccount.Cash >= fieldToBuy.CardInfo.Price)
            {
                playerAccount.CashAccount.RemoveCash(fieldToBuy.CardInfo.Price);
                fieldToBuy.ChangeOwner(playerAccount);
                MessageBox.Show(String.Format("You bought a property {0}.", fieldToBuy.Name));
            }
            else
                MessageBox.Show("You don't have enough money to buy this property.");

        }

    }
}

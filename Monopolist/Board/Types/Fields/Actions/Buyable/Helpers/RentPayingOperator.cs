﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;

namespace Monopolist.Board.Types.Fields.Actions.Buyable.Helpers
{
    public static class RentPayingOperator
    {
        private static int GetLiveRentPrice(FieldStreet fieldStreet)
        {
            return fieldStreet.RentPriceLive;
        }

        private static int GetLiveRentPrice(FieldStation fieldStation)
        {
            return fieldStation.RentPriceLive;
        }

        private static int GetLiveRentPrice(FieldUtility fieldUtility, int diceSum)
        {
            return fieldUtility.GetRentPriceLive(diceSum);
        }

        private static void ProceedWithPaying(BuyableField buyableField, PlayerAccount playerAccount)
        {
            int rentPrice = 0;

            if (buyableField is FieldStreet fieldStreet)
            {
                rentPrice = GetLiveRentPrice(fieldStreet);
            }
            else if (buyableField is FieldStation fieldStation)
            {
                rentPrice = GetLiveRentPrice(fieldStation);
            }
            else if (buyableField is FieldUtility fieldUtility)
            {
                rentPrice = GetLiveRentPrice(fieldUtility, playerAccount.LastDiceResult);
            }

            playerAccount.CashAccount.RemoveCash(rentPrice);
            buyableField.Owner.CashAccount.AddCash(rentPrice);
            MessageBox.Show(String.Format("You paid the rent ${0} to {1}{2}.", rentPrice, buyableField.Owner.Symbol, buyableField.Owner.Name));
        }

        public static void PayRent(BuyableField buyableField, PlayerAccount playerAccount)
        {
            if ((buyableField.Mortgage == Field.MortgageStatus.Mortgaged) || (buyableField.Owner.PrisonStatus.IsInPrison))
                MessageBox.Show("This is your lucky day, you don't have to pay Rent.\nOwner is in prison or Property is mortgaged.");
            else
                ProceedWithPaying(buyableField, playerAccount);
            
        }

    }
}

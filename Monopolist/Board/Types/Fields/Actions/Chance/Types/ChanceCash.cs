﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monopolist.Core.Types.Players;

namespace Monopolist.Board.Types.Fields.Actions.Chance.Types
{
    public class ChanceCash : ChanceCard
    {
        private int _howManyCash;

        /// <summary>
        /// HowManyCash can be positive (add money) or negative (remove money)
        /// </summary>
        /// <param name="howManyCash"></param>
        public ChanceCash(string description, int howManyCash) : base(description)
        {
            _howManyCash = howManyCash;
        }
        
        protected override void Proceed(PlayerAccount player)
        {
            player.CashAccount.AddCash(_howManyCash);
        }

    }
}

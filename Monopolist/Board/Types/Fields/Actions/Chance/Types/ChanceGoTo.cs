﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monopolist.Core.Types.Players;
using Monopolist.Board.Types.Fields;

namespace Monopolist.Board.Types.Fields.Actions.Chance.Types
{
    public class ChanceGoTo : ChanceCard
    {
        private Field _fieldToGoTo;

        public ChanceGoTo(string description, Field fieldToGoTo) : base(description)
        {
            _fieldToGoTo = fieldToGoTo;
        }
        
        protected override void Proceed(PlayerAccount player)
        {
            player.FieldOn.RemoveVisitor(player);
            _fieldToGoTo.AddVisitor(player);
        }

    }
}

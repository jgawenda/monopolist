﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;

namespace Monopolist.Board.Types.Fields.Actions.Chance.Types
{
    public abstract class ChanceCard
    {
        public string Description { get; }

        public ChanceCard(string description)
        {
            Description = description;
        }

        public void Execute(PlayerAccount player)
        {
            MessageBox.Show(Description, "Card", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Proceed(player);
        }

        protected abstract void Proceed(PlayerAccount player);

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monopolist.Board.Types.Fields.Actions.Chance.Types;
using Monopolist.Core.Types.Players;

namespace Monopolist.Board.Types.Fields.Actions.Chance
{
    public class ChanceFieldAction
    {
        private FieldChance _fieldChance;

        public ChanceFieldAction(FieldChance fieldChance)
        {
            _fieldChance = fieldChance;
        }

        public void PerformAction(PlayerAccount playerAccount)
        {
            ChanceCard chanceCard = _fieldChance.ChanceCards.GetNextCard();

            chanceCard.Execute(playerAccount);
        }

    }
}

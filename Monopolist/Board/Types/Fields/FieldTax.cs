﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;
using Monopolist.Board.Types.Fields.Tax;

namespace Monopolist.Board.Types.Fields
{
    public class FieldTax : Field
    {
        public TaxCardInfo CardInfo { get; }

        public override string Name { get { return CardInfo.Name; } }

        public FieldTax(string name, int taxAmount, Button assignedButton) : base(assignedButton)
        {
            CardInfo = new TaxCardInfo(name, taxAmount);
        }

        public override void AddVisitor(PlayerAccount playerAccount)
        {
            base.AddVisitor(playerAccount);
            MessageBox.Show(String.Format("You have to pay tax: ${0}", CardInfo.TaxValue), "Tax", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            playerAccount.CashAccount.RemoveCash(CardInfo.TaxValue);
        }

    }
}

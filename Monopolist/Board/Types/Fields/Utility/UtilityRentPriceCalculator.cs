﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopolist.Board.Types.Fields.Utility
{
    public class UtilityRentPriceCalculator
    {
        private List<FieldUtility> _allUtilities;

        public UtilityRentPriceCalculator(List<FieldUtility> allUtilities)
        {
            _allUtilities = allUtilities;
        }

        private int CountHowManyOwned(FieldUtility parentFieldUtility)
        {
            int counter = 0;

            foreach (var utility in _allUtilities)
            {
                if (utility.Owner == parentFieldUtility.Owner)
                    counter++;
            }

            return counter;
        }

        public int GetRentPrice(FieldUtility parentFieldUtility, int diceNumber)
        {
            int rentPrice = 0;

            if ((parentFieldUtility.Owner.Type != Core.Types.Players.PlayerAccount.PlayerType.Bank) && (parentFieldUtility.Mortgage == Field.MortgageStatus.Normal))
            {
                switch (CountHowManyOwned(parentFieldUtility))
                {
                    case 1:
                        rentPrice = diceNumber * 4;
                        break;
                    case 2:
                        rentPrice = diceNumber * 10;
                        break;
                }
            }

            return rentPrice;
        }

    }
}

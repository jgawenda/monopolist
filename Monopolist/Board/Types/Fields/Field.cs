﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;
using Monopolist.Board.Types.Fields.FieldOperators;

namespace Monopolist.Board.Types.Fields
{
    public abstract class Field
    {
        public enum MortgageStatus
        {
            Normal,
            Mortgaged
        }

        public abstract string Name { get; }

        public List<PlayerAccount> Visitors { get; }

        public Button AssignedButton { get; }

        private ButtonAdjuster _buttonAdjuster;

        public Field(Button assignedButton)
        {
            Visitors = new List<PlayerAccount>();
            AssignedButton = assignedButton;
            _buttonAdjuster = new ButtonAdjuster(assignedButton);
        }

        public virtual void AddVisitor(PlayerAccount playerAccount)
        {
            Visitors.Add(playerAccount);
            playerAccount.ChangeFieldOn(this);
            _buttonAdjuster.HighlightButton();
        }
        
        public virtual void RemoveVisitor(PlayerAccount playerAccount)
        {
            Visitors.Remove(playerAccount);
            _buttonAdjuster.UnhighlightButton();
        }

        /// <summary>
        /// Simulates the Step On, Step Off visual effect.
        /// </summary>
        public void SimulateStepOnOff(int miliseconds)
        {
            _buttonAdjuster.SimulateStepOnOff(miliseconds);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;
using Monopolist.Board.Types.Fields.Actions.Buyable;

namespace Monopolist.Board.Types.Fields
{
    public abstract class BuyableField : Field
    {
        public MortgageStatus Mortgage { get; set; }
        public PlayerAccount Owner { get; private set; }
        public BuyableCardInfo CardInfo { get; }

        public override string Name { get { return CardInfo.Name; } }

        private BuyableFieldAction _buyableFieldAction;

        public BuyableField(PlayerAccount owner, BuyableCardInfo cardInfo, Button assignedButton) : base(assignedButton)
        {
            Owner = owner;
            Mortgage = MortgageStatus.Normal;
            CardInfo = cardInfo;

            _buyableFieldAction = new BuyableFieldAction(this);
        }

        public override void AddVisitor(PlayerAccount playerAccount)
        {
            base.AddVisitor(playerAccount);
            _buyableFieldAction.PerformAction(playerAccount);
        }

        public void ChangeOwner(PlayerAccount newOwner)
        {
            Owner = newOwner;
        }

        public override string ToString()
        {
            string signature = CardInfo.Name;

            if (Mortgage == MortgageStatus.Mortgaged)
                signature += " (mortgaged!)";

            return signature;
        }

        public abstract void MakeMortgage();

        public abstract void BuyoutMortgage();
        
    }
}

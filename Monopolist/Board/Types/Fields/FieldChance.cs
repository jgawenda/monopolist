﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Types.Fields.Chance;
using Monopolist.Core.Types.Players;
using Monopolist.Board.Types.Fields.Actions.Chance;

namespace Monopolist.Board.Types.Fields
{
    public class FieldChance : Field
    {
        public CardInfo CardInfo { get; }
        public ChanceCardsPile ChanceCards { get; private set; }

        public override string Name { get { return CardInfo.Name; } }

        private ChanceFieldAction _chanceFieldAction;

        /// <summary>
        /// Remember to put here the same Pile object for every FieldChance.
        /// </summary>
        /// <param name="chanceCardsContainer"></param>
        public FieldChance(CardInfo cardInfo, ChanceCardsPile chanceCardsContainer, Button assignedButton) : base(assignedButton)
        {
            CardInfo = cardInfo;
            ChanceCards = chanceCardsContainer;
            _chanceFieldAction = new ChanceFieldAction(this);
        }

        public override void AddVisitor(PlayerAccount playerAccount)
        {
            base.AddVisitor(playerAccount);
            _chanceFieldAction.PerformAction(playerAccount);
        }

        /// <summary>
        /// Remember to update all objects of FieldChance present on Game Board.
        /// </summary>
        /// <param name="chanceCardsPile"></param>
        public void UpdateCardPile(ChanceCardsPile chanceCardsPile)
        {
            ChanceCards = chanceCardsPile;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopolist.Board.Types.Fields.Normal
{
    public class ExtendedCardInfo : CardInfo
    {
        public string Description { get; }

        public ExtendedCardInfo(string name, string description) : base(name)
        {
            Description = description;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monopolist.Board.Types.Fields.Mortgage
{
    /// <summary>
    /// Class used to manage the mortgage operations.
    /// </summary>
    public class MortgageOperator
    {
        protected BuyableField _parentField;
        protected BuyableCardInfo _cardInfo;

        public MortgageOperator(BuyableField parentField, BuyableCardInfo cardInfo)
        {
            _parentField = parentField;
            _cardInfo = cardInfo;
        }

        protected bool IsMortgageNormal()
        {
            bool result = false;

            if (_parentField.Mortgage == Field.MortgageStatus.Normal)
                result = true;

            return result;
        }

        public virtual void MakeMortgage()
        {
            if (IsMortgageNormal())
            {
                _parentField.Mortgage = Field.MortgageStatus.Mortgaged;
                _parentField.Owner.CashAccount.AddCash(_cardInfo.MortgageValue);
            }
            else
                MessageBox.Show("This street is already mortgaged.");
            
        }

        public virtual void BuyoutMortgage()
        {
            if (!IsMortgageNormal())
            {
                if (_parentField.Owner.CashAccount.Cash >= _cardInfo.MortgageBuyoutFullPrice)
                {
                    _parentField.Mortgage = Field.MortgageStatus.Normal;
                    _parentField.Owner.CashAccount.RemoveCash(_cardInfo.MortgageBuyoutFullPrice);
                }
                else
                    MessageBox.Show("You don't have enough money to lift this mortgage.", "Not enough money", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
                MessageBox.Show("You can't lift mortgage - it's already lifted.");
            
        }

    }
}

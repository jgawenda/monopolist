﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;

namespace Monopolist.Board.Types.Fields.FieldOperators
{
    public class ButtonAdjuster
    {
        // Button assigned to specific Field
        private Button _assignedButton;

        private int _howManyPlayers;

        public ButtonAdjuster(Button button)
        {
            _assignedButton = button;
            _howManyPlayers = 0;
        }

        private void DarkenButtonColor()
        {
            _assignedButton.BackColor = Color.FromArgb(_assignedButton.BackColor.R - 30, _assignedButton.BackColor.G - 30, _assignedButton.BackColor.B - 30);
        }

        private void LightenButtonColor()
        {
            _assignedButton.BackColor = Color.FromArgb(_assignedButton.BackColor.R + 30, _assignedButton.BackColor.G + 30, _assignedButton.BackColor.B + 30);
        }

        public void HighlightButton()
        {
            switch (_howManyPlayers)
            {
                case 0:
                    _assignedButton.BackColor = Color.FromArgb(195, 195, 195);
                    break;
                default:
                    DarkenButtonColor();
                    break;
            }

            _howManyPlayers++;
            _assignedButton.Refresh();
        }
        
        public void UnhighlightButton()
        {
            _howManyPlayers--;

            switch (_howManyPlayers)
            {
                case 0:
                    _assignedButton.BackColor = SystemColors.Control;
                    break;
                default:
                    LightenButtonColor();
                    break;
            }

            _assignedButton.Refresh();
        }
        
        public void SimulateStepOnOff(int miliseconds)
        {
            HighlightButton();
            Thread.Sleep(miliseconds);
            UnhighlightButton();
        }

    }
}

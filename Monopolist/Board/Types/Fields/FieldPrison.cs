﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;
using Monopolist.Board.Types.Fields.Normal;

namespace Monopolist.Board.Types.Fields
{
    public class FieldPrison : NormalField
    {
        public List<PlayerAccount> Prisoners { get; }

        public FieldPrison(Button assignedButton) : base(new ExtendedCardInfo("Więzienie", "To miejsce w którym jedni gracze odsiadują swoją karę, a inni tylko ich odwiedzają."), assignedButton)
        {
            Prisoners = new List<PlayerAccount>();
        }

        public void LockInPrison(PlayerAccount player)
        {
            Prisoners.Add(player);
            player.ChangeFieldOn(this);
            player.PrisonStatus.LockInPrison();
        }

        public void FreeFromPrison(PlayerAccount player)
        {
            if (Prisoners.Remove(player))
            {
                player.PrisonStatus.FreeFromPrison();
                base.AddVisitor(player);
            }
        }

    }
}

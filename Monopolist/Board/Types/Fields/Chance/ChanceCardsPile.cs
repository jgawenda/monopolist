﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Types.Fields.Actions.Chance.Types;

namespace Monopolist.Board.Types.Fields.Chance
{
    public class ChanceCardsPile
    {
        private List<ChanceCard> _chanceCards;
        private int _counter;

        public ChanceCardsPile(List<ChanceCard> chanceCards)
        {
            _chanceCards = chanceCards;
            _counter = -1;
        }

        public ChanceCard GetNextCard()
        {
            if (_counter >= _chanceCards.Count - 1)
                _counter = -1;

            _counter++;

            return _chanceCards.ElementAt(_counter);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Board.Types.Fields.Normal;

namespace Monopolist.Board.Types.Fields
{
    public class FieldParking : NormalField
    {
        public FieldParking(Button assignedButton) : base(new ExtendedCardInfo("Bezpłatny parking", "Gracz odwiedzający to miejsce, korzysta z bezpłatnego parkingu."), assignedButton)
        {

        }

    }
}

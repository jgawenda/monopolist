﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopolist.Board.Types.Drawing
{
    public class LineCoordinatesContainer
    {
        public List<LineCoordinates> TopLinesCoordinates { get; }
        public List<LineCoordinates> BottomLinesCoordinates { get; }
        public List<LineCoordinates> LeftLinesCoordinates { get; }
        public List<LineCoordinates> RightLinesCoordinates { get; }

        public LineCoordinatesContainer(List<LineCoordinates> topLines, List<LineCoordinates> bottomLines, List<LineCoordinates> leftLines, List<LineCoordinates> rightLines)
        {
            TopLinesCoordinates = topLines;
            BottomLinesCoordinates = bottomLines;
            LeftLinesCoordinates = leftLines;
            RightLinesCoordinates = rightLines;
        }

    }
}

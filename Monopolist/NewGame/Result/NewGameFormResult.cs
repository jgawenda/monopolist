﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monopolist.Core.Types.Players;

namespace Monopolist.NewGame.Result
{
    /// <summary>
    /// Class for holding Players collected from NewGameForm
    /// </summary>
    public class NewGameFormResult
    {
        public int HowManyPlayers { get; private set; }

        public List<PlayerAccount> Players { get; private set; }
        public PlayerAccount BankAccount { get; private set; }

        public NewGameFormResult(List<PlayerAccount> players, PlayerAccount bankAccount)
        {
            Players = players;
            BankAccount = bankAccount;
            CountPlayers();
        }

        private void CountPlayers()
        {
            HowManyPlayers = Players.Count;
        }

    }
}

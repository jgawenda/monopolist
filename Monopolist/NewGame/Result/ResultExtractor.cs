﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monopolist.NewGame.Types;
using Monopolist.Core.Types.Players;
using Monopolist.Core.Helpers;

namespace Monopolist.NewGame.Result
{
    public class ResultExtractor
    {
        private NewGameFormPlayerContainer _formElementsContainer;

        public ResultExtractor(NewGameFormPlayerContainer newGameFormElements)
        {
            _formElementsContainer = newGameFormElements;
        }

        private List<PlayerAccount> ExtractPlayers()
        {
            List<PlayerAccount> resultList = new List<PlayerAccount>();

            foreach (var player in _formElementsContainer.NewGameFormPlayers)
            {
                if (player.CheckBox.Checked)
                {
                    resultList.Add(new PlayerAccount(PlayerAccount.PlayerType.Human, player.TextBox.Text, player.LabelSymbol.Text.First(), 1300));
                }
            }

            ShufflePlayers(resultList);

            return resultList;
        }

        private void ShufflePlayers(List<PlayerAccount> playerAccounts)
        {
            if (_formElementsContainer.CheckBoxRandomOrder.Checked)
                ListShuffler.Shuffle(playerAccounts);
        }

        public NewGameFormResult GetResult()
        {
            return new NewGameFormResult(ExtractPlayers(), new PlayerAccount());
        }

    }
}

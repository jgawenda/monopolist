﻿namespace Monopolist.NewGame
{
    partial class NewGameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewGameForm));
            this.textBoxNameP1 = new System.Windows.Forms.TextBox();
            this.labelSelectPlayers = new System.Windows.Forms.Label();
            this.checkBoxEnabledP1 = new System.Windows.Forms.CheckBox();
            this.labelSymbolP1 = new System.Windows.Forms.Label();
            this.groupBoxP1 = new System.Windows.Forms.GroupBox();
            this.groupBoxP2 = new System.Windows.Forms.GroupBox();
            this.textBoxNameP2 = new System.Windows.Forms.TextBox();
            this.labelSymbolP2 = new System.Windows.Forms.Label();
            this.checkBoxEnabledP2 = new System.Windows.Forms.CheckBox();
            this.groupBoxP3 = new System.Windows.Forms.GroupBox();
            this.textBoxNameP3 = new System.Windows.Forms.TextBox();
            this.labelSymbolP3 = new System.Windows.Forms.Label();
            this.checkBoxEnabledP3 = new System.Windows.Forms.CheckBox();
            this.groupBoxP4 = new System.Windows.Forms.GroupBox();
            this.textBoxNameP4 = new System.Windows.Forms.TextBox();
            this.labelSymbolP4 = new System.Windows.Forms.Label();
            this.checkBoxEnabledP4 = new System.Windows.Forms.CheckBox();
            this.groupBoxP5 = new System.Windows.Forms.GroupBox();
            this.textBoxNameP5 = new System.Windows.Forms.TextBox();
            this.labelSymbolP5 = new System.Windows.Forms.Label();
            this.checkBoxEnabledP5 = new System.Windows.Forms.CheckBox();
            this.groupBoxP6 = new System.Windows.Forms.GroupBox();
            this.textBoxNameP6 = new System.Windows.Forms.TextBox();
            this.labelSymbolP6 = new System.Windows.Forms.Label();
            this.checkBoxEnabledP6 = new System.Windows.Forms.CheckBox();
            this.buttonOk = new System.Windows.Forms.Button();
            this.checkBoxRandomOrder = new System.Windows.Forms.CheckBox();
            this.groupBoxP1.SuspendLayout();
            this.groupBoxP2.SuspendLayout();
            this.groupBoxP3.SuspendLayout();
            this.groupBoxP4.SuspendLayout();
            this.groupBoxP5.SuspendLayout();
            this.groupBoxP6.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxNameP1
            // 
            this.textBoxNameP1.Enabled = false;
            this.textBoxNameP1.Location = new System.Drawing.Point(16, 51);
            this.textBoxNameP1.MaxLength = 20;
            this.textBoxNameP1.Name = "textBoxNameP1";
            this.textBoxNameP1.Size = new System.Drawing.Size(100, 20);
            this.textBoxNameP1.TabIndex = 0;
            // 
            // labelSelectPlayers
            // 
            this.labelSelectPlayers.AutoSize = true;
            this.labelSelectPlayers.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSelectPlayers.Location = new System.Drawing.Point(208, 21);
            this.labelSelectPlayers.Name = "labelSelectPlayers";
            this.labelSelectPlayers.Size = new System.Drawing.Size(94, 16);
            this.labelSelectPlayers.TabIndex = 6;
            this.labelSelectPlayers.Text = "Select players";
            // 
            // checkBoxEnabledP1
            // 
            this.checkBoxEnabledP1.AutoSize = true;
            this.checkBoxEnabledP1.Location = new System.Drawing.Point(59, 31);
            this.checkBoxEnabledP1.Name = "checkBoxEnabledP1";
            this.checkBoxEnabledP1.Size = new System.Drawing.Size(15, 14);
            this.checkBoxEnabledP1.TabIndex = 7;
            this.checkBoxEnabledP1.UseVisualStyleBackColor = true;
            this.checkBoxEnabledP1.CheckedChanged += new System.EventHandler(this.CheckBoxEnabledP1_CheckedChanged);
            // 
            // labelSymbolP1
            // 
            this.labelSymbolP1.AutoSize = true;
            this.labelSymbolP1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSymbolP1.Location = new System.Drawing.Point(42, 83);
            this.labelSymbolP1.Name = "labelSymbolP1";
            this.labelSymbolP1.Size = new System.Drawing.Size(49, 37);
            this.labelSymbolP1.TabIndex = 8;
            this.labelSymbolP1.Text = "♔";
            // 
            // groupBoxP1
            // 
            this.groupBoxP1.Controls.Add(this.textBoxNameP1);
            this.groupBoxP1.Controls.Add(this.labelSymbolP1);
            this.groupBoxP1.Controls.Add(this.checkBoxEnabledP1);
            this.groupBoxP1.Location = new System.Drawing.Point(33, 61);
            this.groupBoxP1.Name = "groupBoxP1";
            this.groupBoxP1.Size = new System.Drawing.Size(132, 131);
            this.groupBoxP1.TabIndex = 9;
            this.groupBoxP1.TabStop = false;
            this.groupBoxP1.Text = "Player";
            // 
            // groupBoxP2
            // 
            this.groupBoxP2.Controls.Add(this.textBoxNameP2);
            this.groupBoxP2.Controls.Add(this.labelSymbolP2);
            this.groupBoxP2.Controls.Add(this.checkBoxEnabledP2);
            this.groupBoxP2.Location = new System.Drawing.Point(186, 61);
            this.groupBoxP2.Name = "groupBoxP2";
            this.groupBoxP2.Size = new System.Drawing.Size(132, 131);
            this.groupBoxP2.TabIndex = 10;
            this.groupBoxP2.TabStop = false;
            this.groupBoxP2.Text = "Player";
            // 
            // textBoxNameP2
            // 
            this.textBoxNameP2.Enabled = false;
            this.textBoxNameP2.Location = new System.Drawing.Point(16, 51);
            this.textBoxNameP2.MaxLength = 20;
            this.textBoxNameP2.Name = "textBoxNameP2";
            this.textBoxNameP2.Size = new System.Drawing.Size(100, 20);
            this.textBoxNameP2.TabIndex = 0;
            // 
            // labelSymbolP2
            // 
            this.labelSymbolP2.AutoSize = true;
            this.labelSymbolP2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSymbolP2.Location = new System.Drawing.Point(42, 83);
            this.labelSymbolP2.Name = "labelSymbolP2";
            this.labelSymbolP2.Size = new System.Drawing.Size(49, 37);
            this.labelSymbolP2.TabIndex = 8;
            this.labelSymbolP2.Text = "♕";
            // 
            // checkBoxEnabledP2
            // 
            this.checkBoxEnabledP2.AutoSize = true;
            this.checkBoxEnabledP2.Location = new System.Drawing.Point(59, 31);
            this.checkBoxEnabledP2.Name = "checkBoxEnabledP2";
            this.checkBoxEnabledP2.Size = new System.Drawing.Size(15, 14);
            this.checkBoxEnabledP2.TabIndex = 7;
            this.checkBoxEnabledP2.UseVisualStyleBackColor = true;
            this.checkBoxEnabledP2.CheckedChanged += new System.EventHandler(this.CheckBoxEnabledP2_CheckedChanged);
            // 
            // groupBoxP3
            // 
            this.groupBoxP3.Controls.Add(this.textBoxNameP3);
            this.groupBoxP3.Controls.Add(this.labelSymbolP3);
            this.groupBoxP3.Controls.Add(this.checkBoxEnabledP3);
            this.groupBoxP3.Location = new System.Drawing.Point(340, 61);
            this.groupBoxP3.Name = "groupBoxP3";
            this.groupBoxP3.Size = new System.Drawing.Size(132, 131);
            this.groupBoxP3.TabIndex = 10;
            this.groupBoxP3.TabStop = false;
            this.groupBoxP3.Text = "Player";
            // 
            // textBoxNameP3
            // 
            this.textBoxNameP3.Enabled = false;
            this.textBoxNameP3.Location = new System.Drawing.Point(16, 51);
            this.textBoxNameP3.MaxLength = 20;
            this.textBoxNameP3.Name = "textBoxNameP3";
            this.textBoxNameP3.Size = new System.Drawing.Size(100, 20);
            this.textBoxNameP3.TabIndex = 0;
            // 
            // labelSymbolP3
            // 
            this.labelSymbolP3.AutoSize = true;
            this.labelSymbolP3.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSymbolP3.Location = new System.Drawing.Point(42, 83);
            this.labelSymbolP3.Name = "labelSymbolP3";
            this.labelSymbolP3.Size = new System.Drawing.Size(49, 37);
            this.labelSymbolP3.TabIndex = 8;
            this.labelSymbolP3.Text = "♘";
            // 
            // checkBoxEnabledP3
            // 
            this.checkBoxEnabledP3.AutoSize = true;
            this.checkBoxEnabledP3.Location = new System.Drawing.Point(59, 31);
            this.checkBoxEnabledP3.Name = "checkBoxEnabledP3";
            this.checkBoxEnabledP3.Size = new System.Drawing.Size(15, 14);
            this.checkBoxEnabledP3.TabIndex = 7;
            this.checkBoxEnabledP3.UseVisualStyleBackColor = true;
            this.checkBoxEnabledP3.CheckedChanged += new System.EventHandler(this.CheckBoxEnabledP3_CheckedChanged);
            // 
            // groupBoxP4
            // 
            this.groupBoxP4.Controls.Add(this.textBoxNameP4);
            this.groupBoxP4.Controls.Add(this.labelSymbolP4);
            this.groupBoxP4.Controls.Add(this.checkBoxEnabledP4);
            this.groupBoxP4.Location = new System.Drawing.Point(33, 215);
            this.groupBoxP4.Name = "groupBoxP4";
            this.groupBoxP4.Size = new System.Drawing.Size(132, 131);
            this.groupBoxP4.TabIndex = 11;
            this.groupBoxP4.TabStop = false;
            this.groupBoxP4.Text = "Player";
            // 
            // textBoxNameP4
            // 
            this.textBoxNameP4.Enabled = false;
            this.textBoxNameP4.Location = new System.Drawing.Point(16, 51);
            this.textBoxNameP4.MaxLength = 20;
            this.textBoxNameP4.Name = "textBoxNameP4";
            this.textBoxNameP4.Size = new System.Drawing.Size(100, 20);
            this.textBoxNameP4.TabIndex = 0;
            // 
            // labelSymbolP4
            // 
            this.labelSymbolP4.AutoSize = true;
            this.labelSymbolP4.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSymbolP4.Location = new System.Drawing.Point(42, 83);
            this.labelSymbolP4.Name = "labelSymbolP4";
            this.labelSymbolP4.Size = new System.Drawing.Size(49, 37);
            this.labelSymbolP4.TabIndex = 8;
            this.labelSymbolP4.Text = "❦";
            // 
            // checkBoxEnabledP4
            // 
            this.checkBoxEnabledP4.AutoSize = true;
            this.checkBoxEnabledP4.Location = new System.Drawing.Point(59, 31);
            this.checkBoxEnabledP4.Name = "checkBoxEnabledP4";
            this.checkBoxEnabledP4.Size = new System.Drawing.Size(15, 14);
            this.checkBoxEnabledP4.TabIndex = 7;
            this.checkBoxEnabledP4.UseVisualStyleBackColor = true;
            this.checkBoxEnabledP4.CheckedChanged += new System.EventHandler(this.CheckBoxEnabledP4_CheckedChanged);
            // 
            // groupBoxP5
            // 
            this.groupBoxP5.Controls.Add(this.textBoxNameP5);
            this.groupBoxP5.Controls.Add(this.labelSymbolP5);
            this.groupBoxP5.Controls.Add(this.checkBoxEnabledP5);
            this.groupBoxP5.Location = new System.Drawing.Point(186, 215);
            this.groupBoxP5.Name = "groupBoxP5";
            this.groupBoxP5.Size = new System.Drawing.Size(132, 131);
            this.groupBoxP5.TabIndex = 10;
            this.groupBoxP5.TabStop = false;
            this.groupBoxP5.Text = "Player";
            // 
            // textBoxNameP5
            // 
            this.textBoxNameP5.Enabled = false;
            this.textBoxNameP5.Location = new System.Drawing.Point(16, 51);
            this.textBoxNameP5.MaxLength = 20;
            this.textBoxNameP5.Name = "textBoxNameP5";
            this.textBoxNameP5.Size = new System.Drawing.Size(100, 20);
            this.textBoxNameP5.TabIndex = 0;
            // 
            // labelSymbolP5
            // 
            this.labelSymbolP5.AutoSize = true;
            this.labelSymbolP5.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSymbolP5.Location = new System.Drawing.Point(42, 83);
            this.labelSymbolP5.Name = "labelSymbolP5";
            this.labelSymbolP5.Size = new System.Drawing.Size(49, 37);
            this.labelSymbolP5.TabIndex = 8;
            this.labelSymbolP5.Text = "❄";
            // 
            // checkBoxEnabledP5
            // 
            this.checkBoxEnabledP5.AutoSize = true;
            this.checkBoxEnabledP5.Location = new System.Drawing.Point(59, 31);
            this.checkBoxEnabledP5.Name = "checkBoxEnabledP5";
            this.checkBoxEnabledP5.Size = new System.Drawing.Size(15, 14);
            this.checkBoxEnabledP5.TabIndex = 7;
            this.checkBoxEnabledP5.UseVisualStyleBackColor = true;
            this.checkBoxEnabledP5.CheckedChanged += new System.EventHandler(this.CheckBoxEnabledP5_CheckedChanged);
            // 
            // groupBoxP6
            // 
            this.groupBoxP6.Controls.Add(this.textBoxNameP6);
            this.groupBoxP6.Controls.Add(this.labelSymbolP6);
            this.groupBoxP6.Controls.Add(this.checkBoxEnabledP6);
            this.groupBoxP6.Location = new System.Drawing.Point(340, 215);
            this.groupBoxP6.Name = "groupBoxP6";
            this.groupBoxP6.Size = new System.Drawing.Size(132, 131);
            this.groupBoxP6.TabIndex = 10;
            this.groupBoxP6.TabStop = false;
            this.groupBoxP6.Text = "Player";
            // 
            // textBoxNameP6
            // 
            this.textBoxNameP6.Enabled = false;
            this.textBoxNameP6.Location = new System.Drawing.Point(16, 51);
            this.textBoxNameP6.MaxLength = 20;
            this.textBoxNameP6.Name = "textBoxNameP6";
            this.textBoxNameP6.Size = new System.Drawing.Size(100, 20);
            this.textBoxNameP6.TabIndex = 0;
            // 
            // labelSymbolP6
            // 
            this.labelSymbolP6.AutoSize = true;
            this.labelSymbolP6.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSymbolP6.Location = new System.Drawing.Point(42, 83);
            this.labelSymbolP6.Name = "labelSymbolP6";
            this.labelSymbolP6.Size = new System.Drawing.Size(49, 37);
            this.labelSymbolP6.TabIndex = 8;
            this.labelSymbolP6.Text = "❀";
            // 
            // checkBoxEnabledP6
            // 
            this.checkBoxEnabledP6.AutoSize = true;
            this.checkBoxEnabledP6.Location = new System.Drawing.Point(59, 31);
            this.checkBoxEnabledP6.Name = "checkBoxEnabledP6";
            this.checkBoxEnabledP6.Size = new System.Drawing.Size(15, 14);
            this.checkBoxEnabledP6.TabIndex = 7;
            this.checkBoxEnabledP6.UseVisualStyleBackColor = true;
            this.checkBoxEnabledP6.CheckedChanged += new System.EventHandler(this.CheckBoxEnabledP6_CheckedChanged);
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(215, 365);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 1;
            this.buttonOk.Text = "OK";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.ButtonOk_Click);
            // 
            // checkBoxRandomOrder
            // 
            this.checkBoxRandomOrder.AutoSize = true;
            this.checkBoxRandomOrder.Location = new System.Drawing.Point(343, 369);
            this.checkBoxRandomOrder.Name = "checkBoxRandomOrder";
            this.checkBoxRandomOrder.Size = new System.Drawing.Size(129, 17);
            this.checkBoxRandomOrder.TabIndex = 2;
            this.checkBoxRandomOrder.Text = "Random players order";
            this.checkBoxRandomOrder.UseVisualStyleBackColor = true;
            // 
            // NewGameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(505, 400);
            this.Controls.Add(this.checkBoxRandomOrder);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.groupBoxP6);
            this.Controls.Add(this.groupBoxP5);
            this.Controls.Add(this.groupBoxP4);
            this.Controls.Add(this.groupBoxP3);
            this.Controls.Add(this.groupBoxP2);
            this.Controls.Add(this.groupBoxP1);
            this.Controls.Add(this.labelSelectPlayers);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewGameForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "New game";
            this.groupBoxP1.ResumeLayout(false);
            this.groupBoxP1.PerformLayout();
            this.groupBoxP2.ResumeLayout(false);
            this.groupBoxP2.PerformLayout();
            this.groupBoxP3.ResumeLayout(false);
            this.groupBoxP3.PerformLayout();
            this.groupBoxP4.ResumeLayout(false);
            this.groupBoxP4.PerformLayout();
            this.groupBoxP5.ResumeLayout(false);
            this.groupBoxP5.PerformLayout();
            this.groupBoxP6.ResumeLayout(false);
            this.groupBoxP6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxNameP1;
        private System.Windows.Forms.Label labelSelectPlayers;
        private System.Windows.Forms.CheckBox checkBoxEnabledP1;
        private System.Windows.Forms.Label labelSymbolP1;
        private System.Windows.Forms.GroupBox groupBoxP1;
        private System.Windows.Forms.GroupBox groupBoxP2;
        private System.Windows.Forms.TextBox textBoxNameP2;
        private System.Windows.Forms.Label labelSymbolP2;
        private System.Windows.Forms.CheckBox checkBoxEnabledP2;
        private System.Windows.Forms.GroupBox groupBoxP3;
        private System.Windows.Forms.TextBox textBoxNameP3;
        private System.Windows.Forms.Label labelSymbolP3;
        private System.Windows.Forms.CheckBox checkBoxEnabledP3;
        private System.Windows.Forms.GroupBox groupBoxP4;
        private System.Windows.Forms.TextBox textBoxNameP4;
        private System.Windows.Forms.Label labelSymbolP4;
        private System.Windows.Forms.CheckBox checkBoxEnabledP4;
        private System.Windows.Forms.GroupBox groupBoxP5;
        private System.Windows.Forms.TextBox textBoxNameP5;
        private System.Windows.Forms.Label labelSymbolP5;
        private System.Windows.Forms.CheckBox checkBoxEnabledP5;
        private System.Windows.Forms.GroupBox groupBoxP6;
        private System.Windows.Forms.TextBox textBoxNameP6;
        private System.Windows.Forms.Label labelSymbolP6;
        private System.Windows.Forms.CheckBox checkBoxEnabledP6;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.CheckBox checkBoxRandomOrder;
    }
}
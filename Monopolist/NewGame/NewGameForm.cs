﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.NewGame.Result;
using Monopolist.NewGame.Types;
using Monopolist.NewGame.Behavior;
using Monopolist.NewGame.Checkboxes;
using Monopolist.NewGame.Preparation;

namespace Monopolist.NewGame
{
    public partial class NewGameForm : Form
    {
        public NewGameFormResult NewGameFormResult { get; private set; }

        private NewGameFormBehaviorOperator _behaviorOperator;

        private CheckboxOperator _checkBoxOperator;

        public NewGameForm()
        {
            InitializeComponent();

            PrepareAllOperators();
        }

        private void PrepareAllOperators()
        {
            NewGameFormPlayerContainer formContainer = PrepareFormContainer();
            _behaviorOperator = NewGameFormGenerator.GenerateBehaviorOperator(formContainer, this);
            _checkBoxOperator = NewGameFormGenerator.GenerateCheckBoxOperator(formContainer);
        }
        
        private NewGameFormPlayerContainer PrepareFormContainer()
        {
            // prepare set of Form Controls (checkBoxes, textBoxes etc.) to use in ResultExtractor and Validator
            NewGameFormPlayerContainer formContainer = new NewGameFormPlayerContainer(
                new NewGameFormPlayer(checkBoxEnabledP1, textBoxNameP1, labelSymbolP1),
                new NewGameFormPlayer(checkBoxEnabledP2, textBoxNameP2, labelSymbolP2),
                new NewGameFormPlayer(checkBoxEnabledP3, textBoxNameP3, labelSymbolP3),
                new NewGameFormPlayer(checkBoxEnabledP4, textBoxNameP4, labelSymbolP4),
                new NewGameFormPlayer(checkBoxEnabledP5, textBoxNameP5, labelSymbolP5),
                new NewGameFormPlayer(checkBoxEnabledP6, textBoxNameP6, labelSymbolP6),
                checkBoxRandomOrder
                );

            return formContainer;
        }
        
        private void ButtonOk_Click(object sender, EventArgs e)
        {
            _behaviorOperator.ClickedOK();
        }

        public void SaveFormResult(NewGameFormResult formResult)
        {
            NewGameFormResult = formResult;
        }

        private void CheckBoxEnabledP1_CheckedChanged(object sender, EventArgs e)
        {
            _checkBoxOperator.Switch(checkBoxEnabledP1);
        }

        private void CheckBoxEnabledP2_CheckedChanged(object sender, EventArgs e)
        {
            _checkBoxOperator.Switch(checkBoxEnabledP2);
        }

        private void CheckBoxEnabledP3_CheckedChanged(object sender, EventArgs e)
        {
            _checkBoxOperator.Switch(checkBoxEnabledP3);
        }

        private void CheckBoxEnabledP4_CheckedChanged(object sender, EventArgs e)
        {
            _checkBoxOperator.Switch(checkBoxEnabledP4);
        }

        private void CheckBoxEnabledP5_CheckedChanged(object sender, EventArgs e)
        {
            _checkBoxOperator.Switch(checkBoxEnabledP5);
        }

        private void CheckBoxEnabledP6_CheckedChanged(object sender, EventArgs e)
        {
            _checkBoxOperator.Switch(checkBoxEnabledP6);
        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.NewGame.Types;

namespace Monopolist.NewGame.Checkboxes
{
    public class CheckboxOperator
    {
        private NewGameFormPlayerContainer _playersContainer;

        public CheckboxOperator(NewGameFormPlayerContainer formContainer)
        {
            _playersContainer = formContainer;
        }

        private void ChangeState(NewGameFormPlayer elementsToSwitch)
        {
            if (elementsToSwitch.CheckBox.Checked)
            {
                elementsToSwitch.TextBox.Enabled = true;
                elementsToSwitch.TextBox.Focus();
            }
            else
                elementsToSwitch.TextBox.Enabled = false;
        }

        public void Switch(CheckBox checkBox)
        {
            NewGameFormPlayer elementsToSwitch = _playersContainer.NewGameFormPlayers.Find(p => p.CheckBox.Equals(checkBox));
            ChangeState(elementsToSwitch);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monopolist.NewGame.Types
{
    public class NewGameFormPlayerContainer
    {
        public List<NewGameFormPlayer> NewGameFormPlayers { get; }
        public CheckBox CheckBoxRandomOrder { get; }

        public NewGameFormPlayerContainer(NewGameFormPlayer player1, NewGameFormPlayer player2, NewGameFormPlayer player3, NewGameFormPlayer player4, NewGameFormPlayer player5, NewGameFormPlayer player6, CheckBox randomOrder)
        {
            NewGameFormPlayers = new List<NewGameFormPlayer>() { player1, player2, player3, player4, player5, player6 };
            CheckBoxRandomOrder = randomOrder;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monopolist.NewGame.Types
{
    public class NewGameFormPlayer
    {
        public CheckBox CheckBox { get; }
        public TextBox TextBox { get; }
        public Label LabelSymbol { get; }

        public NewGameFormPlayer(CheckBox checkBox, TextBox textBox, Label labelSymbol)
        {
            CheckBox = checkBox;
            TextBox = textBox;
            LabelSymbol = labelSymbol;
        }

    }
}

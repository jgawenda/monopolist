﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.NewGame.Result;
using Monopolist.NewGame.Validation;

namespace Monopolist.NewGame.Behavior
{
    public class NewGameFormBehaviorOperator
    {
        private ResultExtractor _resultExtractor;
        private NewGameFormValidator _validator;

        private NewGameForm _parentForm;

        public NewGameFormBehaviorOperator(ResultExtractor resultExtractor, NewGameFormValidator validator, NewGameForm parentForm)
        {
            _resultExtractor = resultExtractor;
            _validator = validator;
            _parentForm = parentForm;
        }

        public void ClickedOK()
        {
            if (_validator.Validate())
            {
                //_parentForm.NewGameFormResult = _resultExtractor.GetResult();
                _parentForm.SaveFormResult(_resultExtractor.GetResult());
                _parentForm.DialogResult = DialogResult.OK;
                _parentForm.Close();
            }
            else
                MessageBox.Show("You have to select at least 2 players", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

    }
}

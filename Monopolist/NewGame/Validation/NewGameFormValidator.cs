﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monopolist.NewGame.Types;

namespace Monopolist.NewGame.Validation
{
    public class NewGameFormValidator
    {
        private int _minimumPlayers;
        private NewGameFormPlayerContainer _playerProfilesContainer;

        public NewGameFormValidator(NewGameFormPlayerContainer newGameFormPlayerContainer)
        {
            _minimumPlayers = 2;
            _playerProfilesContainer = newGameFormPlayerContainer;
        }

        public bool Validate()
        {
            int checkedCounter = 0;

            foreach (var player in _playerProfilesContainer.NewGameFormPlayers)
            {
                if (player.CheckBox.Checked)
                    checkedCounter++;
            }

            if (checkedCounter >= _minimumPlayers)
                return true;
            else
                return false;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.NewGame.Behavior;
using Monopolist.NewGame.Result;
using Monopolist.NewGame.Validation;
using Monopolist.NewGame.Types;
using Monopolist.NewGame.Checkboxes;

namespace Monopolist.NewGame.Preparation
{
    public static class NewGameFormGenerator
    {
        public static NewGameFormBehaviorOperator GenerateBehaviorOperator(NewGameFormPlayerContainer formPlayerContainer, Form parentForm)
        {
            return new NewGameFormBehaviorOperator(new ResultExtractor(formPlayerContainer), new NewGameFormValidator(formPlayerContainer), (NewGameForm)parentForm);
        }

        public static CheckboxOperator GenerateCheckBoxOperator(NewGameFormPlayerContainer formPlayerContainer)
        {
            return new CheckboxOperator(formPlayerContainer);
        }

    }
}

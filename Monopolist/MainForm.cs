﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.UI.FormContainers;
using Monopolist.NewGame;
using Monopolist.Core.Actions.GameStart;
using Monopolist.Helpers;
using Monopolist.About;

namespace Monopolist
{
    public partial class MainForm : Form
    {
        private GameOperator _gameOperator;

        private ControlsOperator _controlsOperator;

        public MainForm()
        {
            InitializeComponent();

            _controlsOperator = new ControlsOperator(newGameToolStripMenuItem, labelInstructions);
            CheckResolution();
        }

        private void CheckResolution()
        {
            ResolutionGuard resolutionGuard = new ResolutionGuard(1600, 900, _controlsOperator);
            resolutionGuard.CheckAndSecure();
        }
        
        private void NewGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewGameFormContainer newGameContainer = new NewGameFormContainer(new NewGameForm());
            
            switch (newGameContainer.SwitchToDialog(this))
            {
                case DialogResult.OK:
                    MainFormPosAdjuster.AdjustPosRightDownCorner(this);
                    _controlsOperator.ChangeGameInProgress();

                    _gameOperator = new GameOperator();
                    _gameOperator.PrepareGame(newGameContainer.GetFormResult());
                    _gameOperator.StartGame();
                    //this.Focus();
                    break;
                //case DialogResult.Cancel:
                    //MessageBox.Show("New game canceled.");
                    //break;
            }
            
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (_gameOperator != null)
            {
                switch (this.WindowState)
                {
                    case FormWindowState.Minimized:
                        _gameOperator.HideAllForms();
                        break;
                    case FormWindowState.Normal:
                        _gameOperator.UnhideAllForms();
                        break;
                }
            }
        }

        private void ToolStripButtonAbout_Click(object sender, EventArgs e)
        {
            AboutForm aboutForm = new AboutForm();
            aboutForm.ShowDialog(this);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_gameOperator != null)
            {
                var result = MessageBox.Show(new Form() { TopMost = true }, "Do you really want to close the game?\nProgress will not be saved.", "Closing game", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);

                if (result == DialogResult.Cancel)
                    e.Cancel = true;
            }
        }
    }
}

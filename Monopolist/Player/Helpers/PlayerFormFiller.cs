﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Monopolist.Core.Types.Players;
using Monopolist.Player.Types;
using Monopolist.Board.Types.Fields;

namespace Monopolist.Player.Helpers
{
    public class PlayerFormFiller
    {
        private PlayerAccount _playerAccount;
        private PlayerFormControls _formControls;

        public PlayerFormFiller(PlayerAccount playerAccount, PlayerFormControls playerFormControls)
        {
            _playerAccount = playerAccount;
            _formControls = playerFormControls;

            LoadInfo();
            //UpdateInfo();
        }

        private void LoadInfo()
        {
            _formControls.LabelSymbol.Text = _playerAccount.Symbol.ToString();
            _formControls.LabelName.Text = _playerAccount.Name;
        }
        
        private void ExtractFieldInfo()
        {
            if (_playerAccount.FieldOn is FieldStreet fieldStreet)
                _formControls.TextBoxCurrentLocation.BackColor = fieldStreet.StreetCardInfo.StreetColor;
            else
                _formControls.TextBoxCurrentLocation.BackColor = SystemColors.Control;

            _formControls.TextBoxCurrentLocation.Text = _playerAccount.FieldOn.Name;
        }

        private void AdjustStatus()
        {
            if (_playerAccount.PrisonStatus.IsInPrison)
                StatusLabelAdjuster.Set(StatusLabelAdjuster.Status.Prison, _formControls.LabelStatus);
            else if (_playerAccount.IsBankrupt)
                StatusLabelAdjuster.Set(StatusLabelAdjuster.Status.Bankrupt, _formControls.LabelStatus);
            else
                StatusLabelAdjuster.Set(StatusLabelAdjuster.Status.Normal, _formControls.LabelStatus);
        }
        
        public void UpdateInfo()
        {
            ExtractFieldInfo();
            _formControls.LabelCash.Text = _playerAccount.CashAccount.Cash.ToString();
            AdjustStatus();
        }

    }
}

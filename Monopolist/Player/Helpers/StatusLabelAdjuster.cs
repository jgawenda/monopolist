﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Monopolist.Player.Helpers
{
    public static class StatusLabelAdjuster
    {
        public enum Status
        {
            Normal,
            Prison,
            Bankrupt
        }

        private static void SetNormal(Label statusLabel)
        {
            statusLabel.ForeColor = SystemColors.ControlText;
            statusLabel.Text = "";
            statusLabel.Visible = false;
        }

        private static void SetPrison(Label statusLabel)
        {
            statusLabel.ForeColor = Color.Orange;
            statusLabel.Text = "In prison";
            statusLabel.Visible = true;
        }

        private static void SetBankrupt(Label statusLabel)
        {
            statusLabel.ForeColor = Color.DarkRed;
            statusLabel.Text = "Bankrupt";
            statusLabel.Visible = true;
        }

        public static void Set(Status status, Label statusLabel)
        {
            switch (status)
            {
                case Status.Normal:
                    SetNormal(statusLabel);
                    break;
                case Status.Prison:
                    SetPrison(statusLabel);
                    break;
                case Status.Bankrupt:
                    SetBankrupt(statusLabel);
                    break;
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monopolist.Player.Types
{
    public class PlayerFormControls
    {
        public TextBox TextBoxCurrentLocation { get; }
        public Label LabelSymbol { get; }
        public Label LabelName { get; }
        public Label LabelCash { get; }
        public Label LabelStatus { get; }

        public PlayerFormControls(TextBox currentLocation, Label labelSymbol, Label labelName, Label labelCash, Label labelStatus)
        {
            TextBoxCurrentLocation = currentLocation;
            LabelSymbol = labelSymbol;
            LabelName = labelName;
            LabelCash = labelCash;
            LabelStatus = labelStatus;
        }

    }
}

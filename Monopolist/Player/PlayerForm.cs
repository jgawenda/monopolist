﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monopolist.Core.Types.Players;
using Monopolist.Player.Helpers;
using Monopolist.Player.Types;
using Monopolist.Board.Helpers.View;
using Monopolist.Board.Types.Fields.Containers;

namespace Monopolist.Player
{
    public partial class PlayerForm : Form
    {
        public PlayerAccount PlayerAccount { get; }

        private PlayerFormFiller _formFiller;
        
        public PlayerForm(PlayerAccount playerAccount)
        {
            InitializeComponent();

            PlayerAccount = playerAccount;

            // only Symbol, Name labels are automatically updated for first time in constructor
            // we need to run UpdateInfo() method after placing the players on Start Field and
            // after each action that requires refreshing Player's Form
            _formFiller = new PlayerFormFiller(playerAccount, new PlayerFormControls(textBoxCurrentLocation, labelSymbol, labelName, labelCash, labelStatus));
        }
        
        /// <summary>
        /// This method is used to refresh User's location, User's cash balance and Status displayed in PlayerForm.
        /// </summary>
        public void UpdateInfo()
        {
            _formFiller.UpdateInfo();
        }
        
    }
}

﻿namespace Monopolist.Player
{
    partial class PlayerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlayerForm));
            this.labelName = new System.Windows.Forms.Label();
            this.labelSymbol = new System.Windows.Forms.Label();
            this.labelCash = new System.Windows.Forms.Label();
            this.labelDollarSign = new System.Windows.Forms.Label();
            this.textBoxCurrentLocation = new System.Windows.Forms.TextBox();
            this.labelCurrentLocation = new System.Windows.Forms.Label();
            this.labelStatus = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelName.Location = new System.Drawing.Point(59, 59);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(61, 24);
            this.labelName.TabIndex = 0;
            this.labelName.Text = "Name";
            // 
            // labelSymbol
            // 
            this.labelSymbol.AutoSize = true;
            this.labelSymbol.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSymbol.Location = new System.Drawing.Point(12, 53);
            this.labelSymbol.Name = "labelSymbol";
            this.labelSymbol.Size = new System.Drawing.Size(41, 31);
            this.labelSymbol.TabIndex = 1;
            this.labelSymbol.Text = "@";
            // 
            // labelCash
            // 
            this.labelCash.AutoSize = true;
            this.labelCash.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCash.Location = new System.Drawing.Point(61, 108);
            this.labelCash.Name = "labelCash";
            this.labelCash.Size = new System.Drawing.Size(24, 25);
            this.labelCash.TabIndex = 2;
            this.labelCash.Text = "0";
            // 
            // labelDollarSign
            // 
            this.labelDollarSign.AutoSize = true;
            this.labelDollarSign.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDollarSign.Location = new System.Drawing.Point(31, 108);
            this.labelDollarSign.Name = "labelDollarSign";
            this.labelDollarSign.Size = new System.Drawing.Size(24, 25);
            this.labelDollarSign.TabIndex = 3;
            this.labelDollarSign.Text = "$";
            // 
            // textBoxCurrentLocation
            // 
            this.textBoxCurrentLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxCurrentLocation.Enabled = false;
            this.textBoxCurrentLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxCurrentLocation.Location = new System.Drawing.Point(0, 24);
            this.textBoxCurrentLocation.Name = "textBoxCurrentLocation";
            this.textBoxCurrentLocation.Size = new System.Drawing.Size(195, 22);
            this.textBoxCurrentLocation.TabIndex = 4;
            this.textBoxCurrentLocation.Text = "Al. Ujazdowskie";
            this.textBoxCurrentLocation.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelCurrentLocation
            // 
            this.labelCurrentLocation.AutoSize = true;
            this.labelCurrentLocation.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.labelCurrentLocation.Location = new System.Drawing.Point(12, 7);
            this.labelCurrentLocation.Name = "labelCurrentLocation";
            this.labelCurrentLocation.Size = new System.Drawing.Size(84, 13);
            this.labelCurrentLocation.TabIndex = 5;
            this.labelCurrentLocation.Text = "Current location:";
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStatus.ForeColor = System.Drawing.Color.Orange;
            this.labelStatus.Location = new System.Drawing.Point(52, 151);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(91, 24);
            this.labelStatus.TabIndex = 6;
            this.labelStatus.Text = "In prison";
            this.labelStatus.Visible = false;
            // 
            // PlayerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(195, 184);
            this.ControlBox = false;
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.labelCurrentLocation);
            this.Controls.Add(this.textBoxCurrentLocation);
            this.Controls.Add(this.labelDollarSign);
            this.Controls.Add(this.labelCash);
            this.Controls.Add(this.labelSymbol);
            this.Controls.Add(this.labelName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PlayerForm";
            this.ShowInTaskbar = false;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelSymbol;
        private System.Windows.Forms.Label labelCash;
        private System.Windows.Forms.Label labelDollarSign;
        private System.Windows.Forms.TextBox textBoxCurrentLocation;
        private System.Windows.Forms.Label labelCurrentLocation;
        private System.Windows.Forms.Label labelStatus;
    }
}
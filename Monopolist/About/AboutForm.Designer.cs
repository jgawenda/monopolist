﻿namespace Monopolist.About
{
    partial class AboutForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutForm));
            this.labelGameName = new System.Windows.Forms.Label();
            this.labelGameVersion = new System.Windows.Forms.Label();
            this.labelCopyright = new System.Windows.Forms.Label();
            this.labelIconCredits = new System.Windows.Forms.Label();
            this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // labelGameName
            // 
            this.labelGameName.AutoSize = true;
            this.labelGameName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelGameName.Location = new System.Drawing.Point(95, 21);
            this.labelGameName.Name = "labelGameName";
            this.labelGameName.Size = new System.Drawing.Size(102, 24);
            this.labelGameName.TabIndex = 0;
            this.labelGameName.Text = "Monopolist";
            // 
            // labelGameVersion
            // 
            this.labelGameVersion.AutoSize = true;
            this.labelGameVersion.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.labelGameVersion.Location = new System.Drawing.Point(125, 53);
            this.labelGameVersion.Name = "labelGameVersion";
            this.labelGameVersion.Size = new System.Drawing.Size(40, 13);
            this.labelGameVersion.TabIndex = 1;
            this.labelGameVersion.Text = "ver 1.0";
            // 
            // labelCopyright
            // 
            this.labelCopyright.AutoSize = true;
            this.labelCopyright.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCopyright.Location = new System.Drawing.Point(71, 84);
            this.labelCopyright.Name = "labelCopyright";
            this.labelCopyright.Size = new System.Drawing.Size(148, 16);
            this.labelCopyright.TabIndex = 2;
            this.labelCopyright.Text = "© Jakub Gawenda 2019";
            // 
            // labelIconCredits
            // 
            this.labelIconCredits.AutoSize = true;
            this.labelIconCredits.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.labelIconCredits.Location = new System.Drawing.Point(11, 125);
            this.labelIconCredits.Name = "labelIconCredits";
            this.labelIconCredits.Size = new System.Drawing.Size(266, 13);
            this.labelIconCredits.TabIndex = 3;
            this.labelIconCredits.Text = "Icon made by Dimitry Miroliubov from www.flaticon.com";
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.Image = global::Monopolist.Properties.Resources.money_60x60;
            this.pictureBoxLogo.Location = new System.Drawing.Point(18, 14);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(60, 60);
            this.pictureBoxLogo.TabIndex = 4;
            this.pictureBoxLogo.TabStop = false;
            // 
            // AboutForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(286, 154);
            this.Controls.Add(this.pictureBoxLogo);
            this.Controls.Add(this.labelIconCredits);
            this.Controls.Add(this.labelCopyright);
            this.Controls.Add(this.labelGameVersion);
            this.Controls.Add(this.labelGameName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "About";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelGameName;
        private System.Windows.Forms.Label labelGameVersion;
        private System.Windows.Forms.Label labelCopyright;
        private System.Windows.Forms.Label labelIconCredits;
        private System.Windows.Forms.PictureBox pictureBoxLogo;
    }
}
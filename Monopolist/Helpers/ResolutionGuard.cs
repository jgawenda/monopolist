﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Monopolist.Helpers
{
    public class ResolutionGuard
    {
        private int _minResolutionWidth;
        private int _minResolutionHeight;

        private ControlsOperator _controlsOperator;
        
        public ResolutionGuard(int minWidth, int minHeight, ControlsOperator controlsOperator)
        {
            _minResolutionWidth = minWidth;
            _minResolutionHeight = minHeight;

            _controlsOperator = controlsOperator;
        }

        private bool CheckIfResolutionIsOk()
        {
            bool result = false;

            if ((Screen.PrimaryScreen.Bounds.Width >= _minResolutionWidth) && (Screen.PrimaryScreen.Bounds.Height >= _minResolutionHeight))
                result = true;

            return result;
        }
        
        public void CheckAndSecure()
        {
            if (!CheckIfResolutionIsOk())
            {
                string activeRes = String.Format("{0} x {1}", Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
                string minRes = String.Format("{0} x {1}", _minResolutionWidth, _minResolutionHeight);
                _controlsOperator.ChangeWrongResolution(activeRes, minRes);
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopolist.Helpers
{
    public static class MainFormPosAdjuster
    {
        public static void AdjustPosRightDownCorner(MainForm mainForm)
        {
            var activePosition = mainForm.Location;
            activePosition.X += 550;
            activePosition.Y += 320;

            mainForm.Location = activePosition;
        }

    }
}

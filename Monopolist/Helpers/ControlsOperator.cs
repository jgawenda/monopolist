﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Monopolist.Helpers
{
    /// <summary>
    /// This Class is used to change MainForm layout (instructions label, toolStrip elements).
    /// </summary>
    public class ControlsOperator
    {
        private ToolStripMenuItem _menuItemNewGame;
        private Label _labelInstructions;

        public ControlsOperator(ToolStripMenuItem menuItemNewGame, Label labelInstructions)
        {
            _menuItemNewGame = menuItemNewGame;
            _labelInstructions = labelInstructions;
        }

        public void ChangeGameInProgress()
        {
            _menuItemNewGame.Enabled = false;
            _labelInstructions.Text = "Minimize this window to minimize the\nwhole game.\n\nClose this window to close the game.";
        }

        public void ChangeGameEnded()
        {
            _menuItemNewGame.Enabled = false;
            _labelInstructions.Text = "Game has ended.\nThank you for playing!";
        }

        public void ChangeWrongResolution(string activeResolution, string minimumResolution)
        {
            _menuItemNewGame.Enabled = false;

            _labelInstructions.Text = String.Format("Your screen resolution\nis too low to play this game.\n\nYour resolution: {0}\nMinimum resolution: {1}", activeResolution, minimumResolution);
            _labelInstructions.ForeColor = Color.OrangeRed;
        }
    }
}
